<div id="tai-san-popup" class="footer-nav-popup tai-san-popup is-hidden">
	<div class="footer-nav-popup__bar d-flex space-between align-center">
		<span class="footer-nav-popup__title">
			<i class="fas fa-book"></i>Tài sản
		</span>
		<span class="footer-nav-popup__close">
			<i class="fas fa-angle-down"></i>
		</span>
	</div>
	<div class="footer-nav-popup__header d-flex space-between align-center">
		<strong>Tài khoản</strong>
		<div class="tai-san__tk footer-nav-popup__select has-submenu dropdown">
			<a class="dropdown-toggle" title="Danh mục"><span>4608121</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-tk="4608121" class="show-tk">4608121</a></li>
				<li><a href="#" data-tk="4608128" class="show-tk">4608128</a></li>
				<li><a href="#" data-tk="4608823" class="show-tk">4608823</a></li>
				<li><a href="#" data-tk="4608825" class="show-tk">4608825</a></li>
				<li><a href="#" data-tk="4608827" class="show-tk">4608827</a></li>
			</ul>
		</div>
	</div>
	<div class="footer-nav-popup__body custom-scrollbar">
		<h5 style="margin: 5px 0;">I. THÔNG TIN TÀI KHOẢN</h5>
		<div class="d-flex space-between">
			<span>Sức mua tài khoản</span>
			<strong>4.500.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Giá trị CK</span>
			<strong>4.500.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Số dư tiền</span>
			<strong>12.500.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Tiền bán còn được ứng</span>
			<strong>12.500.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Giá trị quyền chờ về</span>
			<strong>12.500.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Tiền được rút</span>
			<strong>12.500.000</strong>
		</div>

		<h5 style="margin: 10px 0 5px;">II. DỊCH VỤ TÀI CHÍNH</h5>
		<div class="d-flex space-between">
			<span>Giá trị DM tính QTRR</span>
			<strong>92.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>TSR tính QTRR</span>
			<strong>92.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Nợ</span>
			<strong>9.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Tỷ lệ K</span>
			<strong>5%</strong>
		</div>
		<div class="d-flex space-between">
			<span>Tỷ lệ Call</span>
			<strong>12%</strong>
		</div>
		<div class="d-flex space-between">
			<span>Tỷ lệ Force Sell</span>
			<strong>12%</strong>
		</div>
		<div class="d-flex space-between">
			<span>Hạn mức TK</span>
			<strong>14.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Hạn mức Fal</span>
			<strong>14.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Nợ Fal T0</span>
			<strong>14.000.000</strong>
		</div>
		<!-- <div class="d-flex space-between">
			<span>Nợ Fal T1</span>
			<strong>14.000.000</strong>
		</div>
		<div class="d-flex space-between">
			<span>Nợ Fal T2</span>
			<strong>14.000.000</strong>
		</div> -->
	</div>
</div>