<?php
$defaults = [ 'VNXALL-Index', 'VN-Index', 'VN30-Index', 'VN30F1M', 'HNX-Index' ]
?>

<div class="chart-area is-hidden">
	<?php foreach( $defaults as $default ) : ?>
		<div id="chi-so-<?= $default; ?>" class="chart__item">
			<div class="chart__image">
				<img src="images/chart.svg" alt="chart">
			</div>
			<div class="chart__info">
				<div>
					<span class="info__ten"><?php echo $default; ?></span>
					<span class="info__point info__point--down"><i class="fas fa-caret-down"></i> 912.32 (1.23  1.32%)</span>
				</div>
				<div>
					<span class="info__cp">312.235 <span class="txt-yellow">CP</span></span>
					<span class="info__ty">951.231 <span class="txt-yellow">Tỷ</span></span>
				</div>
				<div>
					<span class="txt-green"><i class="fas fa-caret-up"></i> 11 <span>(<span class="txt-pink">0</span>)</span></span>
					<span class="txt-yellow"><i class="fas fa-square"></i> 4</span>
					<span class="txt-red"><i class="fas fa-caret-down"></i> 15 <span>(<span class="txt-cyan">0</span>)</span></span>
					<span>Liên tục</span>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<div class="chart__item chart__item--table">
		<table class="chart__table">
			<tr>
				<th></th>
				<th>GD</th>
				<th>TT</th>
				<th>Tổng GT</th>
			</tr>
			<tr>
				<td>VN-Index</td>
				<td class="txt-green">12.2</td>
				<td class="txt-green">130</td>
				<td class="txt-green">52.1</td>
			</tr>
			<tr class="half-hidden">
				<td>VN30-Index</td>
				<td class="txt-red">12.2</td>
				<td class="txt-red">130</td>
				<td class="txt-red">52.1</td>
			</tr>
			<tr>
				<td>HNX-Index</td>
				<td class="txt-red">12.2</td>
				<td class="txt-red">130</td>
				<td class="txt-red">52.1</td>
			</tr>
			<tr class="half-hidden">
				<td>UPCOM-Index</td>
				<td class="txt-green">12.2</td>
				<td class="txt-green">130</td>
				<td class="txt-green">52.1</td>
			</tr>
		</table>
	</div>
</div>