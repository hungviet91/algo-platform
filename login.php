<?php include 'header-logged-in.php'; ?>
<div class="fixed-components">
	<?php include 'site-header-login.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header.php'; ?>
</div>
<?php include 'stock-tables/tables.php'; ?>
<?php include 'footer.php'; ?>
