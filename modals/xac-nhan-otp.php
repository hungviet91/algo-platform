<div class="modal modal--xac-nhan-otp" id="xac-nhan-otp">
	<div class="modal__background"></div>
		<div class="modal__body">
			<form class="xac-nhan-otp">
				<div class="xac-nhan-otp__header">
					<span>Xác nhận kích hoạt</span>
					<span class="modal__close">X</span>
				</div>
				<div class="xac-nhan-otp__body">
					<div class="xac-nhan-otp__ds-lenh">
						Danh sách lệnh đã chọn <span></span>
					</div>
					<label>
						<span>Mật khẩu đặt lệnh (OTP)</span>
						<input type="password" id="xac-nhan-otp">
					</label>
					<button class="btn btn--red" type="submit">Xác nhận</button>
				</div>
			</form>
		</div>
	</div>
</div>