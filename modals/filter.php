<div class="modal modal--filter" id="bo-loc-modal">
	<div class="modal__background"></div>
	<div class="modal__body">
		<div class="filter__header d-flex">
			<div class="filter__name filter--left">
				<input type="text" name="" value="" placeholder="Nhập tên bộ lọc">
			</div>
			<div class="filter__save filter--right">
				<div class="filter__total-stock"><span class="number">181</span> mã thỏa mãn</div>
				<div class="filter__buttons">
					<button class="btn btn--primary buttons__save" type="button">Lưu và xem kết quả</button>
					<button class="btn btn--no-bg buttons__reset" type="button">Làm lại</button>
				</div>
			</div>
		</div>
		<div class="filter__body">
			<div class="filter__inputs d-flex flex-wrap">
				<div class="input__search filter--left">
					<input type="text" placeholder="Tìm kiếm điều kiện lọc">
					<button type="submit"><i class="fas fa-search"></i></button>
				</div>
				<div class="input__selects filter--right d-flex flex-wrap">
					<div class="filter__san filter__dropdown">
						<div class="filter__label" data-none="Chưa chọn sàn" data-label="Sàn:">Tất cả các sàn</div>
						<i class="fas fa-angle-down"></i>
						<div class="filter__choices">
							<label><input type="checkbox" name="filter_san[]" checked value="Tất cả các sàn" class="all"> Tất cả các sàn</label>
							<label><input type="checkbox" name="filter_san[]" checked value="HOSE"> HOSE</label>
							<label><input type="checkbox" name="filter_san[]" checked value="HNX"> HNX</label>
							<label><input type="checkbox" name="filter_san[]" checked value="UPCOM"> UPCOM</label>
						</div>
					</div>
					<div class="filter__nganh filter__dropdown">
						<div class="filter__label" data-none="Chưa chọn ngành" data-label="Ngành:">Tất cả các ngành</div>
						<i class="fas fa-angle-down"></i>
						<div class="filter__choices">
							<label><input type="checkbox" name="filter_nganh[]" checked value="Tất cả các ngành" class="all"> Tất cả các ngành</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Dầu khí"> Dầu khí</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Hóa chất"> Hóa chất</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Tài nguyên cơ bản"> Tài nguyên cơ bản</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Xây dựng và vật liệu"> Xây dựng và vật liệu</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Hàng và Dịch vụ công nghiệp"> Hàng và Dịch vụ công nghiệp</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Ô tô và phụ tùng"> Ô tô và phụ tùng</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Thực phẩm và đồ uống"> Thực phẩm và đồ uống</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Hàng cá nhân và gia dụng"> Hàng cá nhân và gia dụng</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Thiết bị và dịch vụ y tế"> Thiết bị và dịch vụ y tế</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Dược phẩm"> Dược phẩm</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Bán lẻ"> Bán lẻ</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Truyền thông"> Truyền thông</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Du lịch và giải trí"> Du lịch và giải trí</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Viễn thông"> Viễn thông</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Điện"> Điện</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Nước và Khí đốt"> Nước và Khí đốt</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Ngân hàng"> Ngân hàng</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Bảo hiểm"> Bảo hiểm</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Bất động sản"> Bất động sản</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Dịch vụ tài chính"> Dịch vụ tài chính</label>
							<label><input type="checkbox" name="filter_nganh[]" checked value="Công nghệ thông tin"> Công nghệ thông tin</label>
						</div>
					</div>
				</div>
			</div>
			<div class="filter__conditions d-flex flex-wrap">
				<div class="filter--left d-flex flex-wrap">
					<div class="condition__tab-items custom-scrollbar">
						<a href="#tab-content-1" class="tab__item active">Quy mô <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-2" class="tab__item">Giá và khối lượng <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-3" class="tab__item">Khối ngoại <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-4" class="tab__item">Tự doanh <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-5" class="tab__item">Tăng trưởng <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-6" class="tab__item">Định giá <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-7" class="tab__item">Khả năng sinh lời <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-8" class="tab__item">Khả năng thanh toán ngắn hạn <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-9" class="tab__item">Khả năng thanh toán dài hạn <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-10" class="tab__item">Hiệu quả hoạt động <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-11" class="tab__item">Cổ tức <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-12" class="tab__item">Sức khỏe tài chính <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-13" class="tab__item">Tín hiệu kỹ thuật <i class="fas fa-angle-right"></i></a>
					</div>
					<?php include 'condition-tab-content.php' ?>
				</div>
				<div class="filter__edit-condition custom-scrollbar filter--right">

				</div>
			</div>
		</div>
		<button type="button" class="modal__close"><i class="fas fa-times"></i></button>
	</div>
</div>
