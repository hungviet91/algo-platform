/**
  stickybits - Stickybits is a lightweight alternative to `position: sticky` polyfills
  @version v3.6.6
  @link https://github.com/dollarshaveclub/stickybits#readme
  @author Jeff Wainwright <yowainwright@gmail.com> (https://jeffry.in)
  @license MIT
**/
!function(t,s){"object"==typeof exports&&"undefined"!=typeof module?module.exports=s():"function"==typeof define&&define.amd?define(s):(t=t||self).stickybits=s()}(this,function(){"use strict";var e=function(){function t(t,s){var e=void 0!==s?s:{};this.version="3.6.6",this.userAgent=window.navigator.userAgent||"no `userAgent` provided by the browser",this.props={customStickyChangeNumber:e.customStickyChangeNumber||null,noStyles:e.noStyles||!1,stickyBitStickyOffset:e.stickyBitStickyOffset||0,parentClass:e.parentClass||"js-stickybit-parent",scrollEl:"string"==typeof e.scrollEl?document.querySelector(e.scrollEl):e.scrollEl||window,stickyClass:e.stickyClass||"js-is-sticky",stuckClass:e.stuckClass||"js-is-stuck",stickyChangeClass:e.stickyChangeClass||"js-is-sticky--change",useStickyClasses:e.useStickyClasses||!1,useFixed:e.useFixed||!1,useGetBoundingClientRect:e.useGetBoundingClientRect||!1,verticalPosition:e.verticalPosition||"top"},this.props.positionVal=this.definePosition()||"fixed",this.instances=[];var i=this.props,n=i.positionVal,o=i.verticalPosition,r=i.noStyles,a=i.stickyBitStickyOffset,l="top"!==o||r?"":a+"px",c="fixed"!==n?n:"";this.els="string"==typeof t?document.querySelectorAll(t):t,"length"in this.els||(this.els=[this.els]);for(var f=0;f<this.els.length;f++){var u=this.els[f];u.style[o]=l,u.style.position=c,this.instances.push(this.addInstance(u,this.props))}}var s=t.prototype;return s.definePosition=function(){var t;if(this.props.useFixed)t="fixed";else{for(var s=["","-o-","-webkit-","-moz-","-ms-"],e=document.head.style,i=0;i<s.length;i+=1)e.position=s[i]+"sticky";t=e.position?e.position:"fixed",e.position=""}return t},s.addInstance=function(t,s){var e=this,i={el:t,parent:t.parentNode,props:s};if("fixed"===s.positionVal||s.useStickyClasses){this.isWin=this.props.scrollEl===window;var n=this.isWin?window:this.getClosestParent(i.el,i.props.scrollEl);this.computeScrollOffsets(i),i.parent.className+=" "+s.parentClass,i.state="default",i.stateContainer=function(){return e.manageState(i)},n.addEventListener("scroll",i.stateContainer)}return i},s.getClosestParent=function(t,s){var e=s,i=t;if(i.parentElement===e)return e;for(;i.parentElement!==e;)i=i.parentElement;return e},s.getTopPosition=function(t){if(this.props.useGetBoundingClientRect)return t.getBoundingClientRect().top+(this.props.scrollEl.pageYOffset||document.documentElement.scrollTop);for(var s=0;s=t.offsetTop+s,t=t.offsetParent;);return s},s.computeScrollOffsets=function(t){var s=t,e=s.props,i=s.el,n=s.parent,o=!this.isWin&&"fixed"===e.positionVal,r="bottom"!==e.verticalPosition,a=o?this.getTopPosition(e.scrollEl):0,l=o?this.getTopPosition(n)-a:this.getTopPosition(n),c=null!==e.customStickyChangeNumber?e.customStickyChangeNumber:i.offsetHeight,f=l+n.offsetHeight;s.offset=a+e.stickyBitStickyOffset,s.stickyStart=r?l-s.offset:0,s.stickyChange=s.stickyStart+c,s.stickyStop=r?f-(i.offsetHeight+s.offset):f-window.innerHeight},s.toggleClasses=function(t,s,e){var i=t,n=i.className.split(" ");e&&-1===n.indexOf(e)&&n.push(e);var o=n.indexOf(s);-1!==o&&n.splice(o,1),i.className=n.join(" ")},s.manageState=function(t){var s=t,e=s.el,i=s.props,n=s.state,o=s.stickyStart,r=s.stickyChange,a=s.stickyStop,l=e.style,c=i.noStyles,f=i.positionVal,u=i.scrollEl,p=i.stickyClass,h=i.stickyChangeClass,d=i.stuckClass,y=i.verticalPosition,k="bottom"!==y,m=function(t){t()},g=this.isWin&&(window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||window.msRequestAnimationFrame)||m,C=this.toggleClasses,v=this.isWin?window.scrollY||window.pageYOffset:u.scrollTop,S=k&&v<=o&&("sticky"===n||"stuck"===n),w=a<=v&&"sticky"===n;o<v&&v<a&&("default"===n||"stuck"===n)?(s.state="sticky",g(function(){C(e,d,p),l.position=f,c||(l.bottom="",l[y]=i.stickyBitStickyOffset+"px")})):S?(s.state="default",g(function(){C(e,p),C(e,d),"fixed"===f&&(l.position="")})):w&&(s.state="stuck",g(function(){C(e,p,d),"fixed"!==f||c||(l.top="",l.bottom="0",l.position="absolute")}));var b=r<=v&&v<=a;v<r/2||a<v?g(function(){C(e,h)}):b&&g(function(){C(e,"stub",h)})},s.update=function(t){void 0===t&&(t=null);for(var s=0;s<this.instances.length;s+=1){var e=this.instances[s];if(this.computeScrollOffsets(e),t)for(var i in t)e.props[i]=t[i]}return this},s.removeInstance=function(t){var s=t.el,e=t.props,i=this.toggleClasses;s.style.position="",s.style[e.verticalPosition]="",i(s,e.stickyClass),i(s,e.stuckClass),i(s.parentNode,e.parentClass)},s.cleanup=function(){for(var t=0;t<this.instances.length;t+=1){var s=this.instances[t];s.stateContainer&&s.props.scrollEl.removeEventListener("scroll",s.stateContainer),this.removeInstance(s)}this.manageState=!1,this.instances=[]},t}();return function(t,s){return new e(t,s)}});



( function( $ ) {
	'use strict';
	var $body            = $( 'body' );
	var $tables          = $( '.stock-table' );
	var $tabContent      = $( '.condition__tab-content' );
	var modals           = [ 'filter', 'chi-so', 'backtest','chitiet-backtest', 'xac-nhan-otp' ];
	var toggleClassItems = [ '.nav-tab', '.market__sub__link', '.view-mode' ];
	var $fixedComponents = $( '.fixed-components' );
	var $chartArea       = $( '.chart-area' );
	var $tablesWrapper   = $( '.stock-tables__table' );
	var $stockHeaderNav  = $( '.stock-header__navigation' );
	var $window          = $( window );
	var $viewMode        = $( '.stock-header__view-modes' );
	var chartSliderArgs  = {
		slidesToShow: 5,
		arrows: false,
		swipeToSlide: true,
		mobileFirst: false,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 5,
				}
			}
		]
	};

	// Default values
	cssVars({
		// Targets
		rootElement   : document,
		shadowDOM     : false,

		// Sources
		include       : 'link[rel=stylesheet],style',
		exclude       : '',
		variables     : {},

		// Options
		onlyLegacy    : true,
		preserveStatic: true,
		preserveVars  : false,
		silent        : false,
		updateDOM     : true,
		updateURLs    : true,
		watch         : false,
	});

	function isLoggedIn() {
		return $body.hasClass( 'logged-in' );
	}

	function randomID() {
		return '_' + Math.random().toString(36).substr(2, 9);
	};

	function chuaDangNhap( $text ) {
		if ( isLoggedIn() ) {
			return false;
		}
		alert( $text );
		return true;
	}

	/**
	 * Sticky Header
	 */
	var stickyHeader = function() {
		var $tableHeader = $( '.table--active .table__header ' ),
			fixedComponentsHeight = Math.floor( $fixedComponents.outerHeight() ),
			$financeHeader = $( '.code-details .tc__header' );

		$tableHeader.css( 'top', fixedComponentsHeight + 'px' );
		stickybits( $tableHeader, { stickyBitStickyOffset: fixedComponentsHeight } );
		$tablesWrapper.css( 'margin-top', fixedComponentsHeight + 'px' );
		$financeHeader.css( 'top', fixedComponentsHeight + 'px' );
	};

	function highlightTab() {
		$( '.nav-tab' ).removeClass( 'active' );
		$( this ).closest( '.dropdown' ).find( '.nav-tab' ).addClass( 'active' );
	}

	var filterTab = {
		init: function() {
			filterTab.modalFilter = $( '.modal--filter' );

			filterTab.initFilterModal();
			filterTab.handleClickOnFilterName();
			filterTab.editFilterItem();
			filterTab.deleteFilterItem();
		},

		getFilterData: function( filterID ) {
			var filterData  = JSON.parse( localStorage.getItem( filterID ) );
			return {
				name: filterData[0].name,
				items: filterData[1].items
			};
		},

		checkFilterNumber: function() {
			var $filters = $( '.dropdown-menu--loc' ).find( '.list__item' );
			if ( $filters.length >= 10 ) {
				alert( 'Chỉ được chọn tối đa 10 bộ lọc' );
				return false;
			}
			return true;
		},

		/**
		 * Edit list item
		 */
		editFilterItem: function() {
			$( '.dropdown-menu--loc' ).on( 'click', '.btn--edit', function( e ) {
				e.preventDefault();
				var $this         = $( this );
				var $listItem     = $this.closest( '.list__item' );
				var $listName     = $listItem.find( '.list__name' ).toggleClass( 'is-hidden' );
				var $input        = $listItem.find( 'input' ).toggleClass( 'is-hidden' );
				var $textWrapper  = $listName.find( 'span' );
				var $dropDownMenu = $listItem.closest( '.dropdown-menu' );

				var filterID      = $listName.attr( 'data-filter-id' );
				var filterData 	  = filterTab.getFilterData( filterID );

				$input.val( $textWrapper.text() );

				$input.on( 'change', function( e ) {
					$textWrapper.text( e.target.value );

					// Update filter name.
					var newfilterData = [ { name: e.target.value }, { items: filterData.items } ];
					localStorage.setItem( filterID, JSON.stringify( newfilterData ) );

					// Update ngay tên bộ lọc ở phía trên bảng hiển thị kq.
					$( '.filter-result__name[data-filter-id="' + filterID + '"]' ).text( e.target.value );
				} );

				$input.on( 'keypress', function( e ) {
					if ( e.which == '13' ) {
						$listName.removeClass( 'is-hidden' );
						$( this ).addClass( 'is-hidden' );
					}
				} )

			} );
		},

		/**
		 * Edit list item
		 */
		deleteFilterItem: function() {
			$( '.dropdown-menu--loc' ).on( 'click', '.btn--delete', function( e ) {
				e.preventDefault();
				e.stopPropagation(); // khi click sẽ không trigger click vào thẻ a phía trên.
				var $listItem     = $( this ).closest( '.list__item--filter' );
				var $prevItem     = $listItem.prev( '.list__item--filter' );
				var $nextItem     = $listItem.next( '.list__item--filter' );
				$listItem.remove();

				// Check xem item phía trước hoặc phía sau có không thì hiện ra
				// Nếu không còn thì để trống
				if ( $nextItem.length > 0 ) {
					$nextItem.find( '.list__name' ).trigger( 'click' );
				} else if ( $prevItem.length > 0 ) {
					$prevItem.find( '.list__name' ).trigger( 'click' );
				} else {
					$( '#ket-qua-loc' ).empty();
					$( '.visble-on-filter' ).addClass( 'is-hidden' );
				}
			} );
		},

		initFilterModal: function() {
			$( '.init-filter-modal' ).on( 'click', function( e ) {
				e.preventDefault();

				if ( chuaDangNhap( 'Quý khách vui lòng đăng nhập để tạo bộ lọc mới' ) ) {
					return;
				}

				var edit     = $( this ).attr( 'data-modal-edit' );
				var filterID = $( this ).attr( 'data-filter-id' );

				// Kiểm tra xem modal filter đang ở trạng thái edit hay tạo mới.
				filterTab.modalFilter.attr( 'data-modal-edit', edit ? 'enable' : 'disable' );

				// Check số lượng bộ lọc hiện tại nếu > 10 và nếu đang là tạo mới bộ lọc thì bỏ qua.
				if ( ! filterID && ! filterTab.checkFilterNumber() ) {
					$body.removeClass( 'modal-filter-enable' );
					return;
				}

				// Nếu chưa có ID thì coi như tạo mới filter và gán ID vào cho fitler.
				if ( ! filterID ) {
					filterTab.modalFilter.attr( 'data-filter-id', randomID() );
				} else {
					// Nếu không thì gán ID của đối tượng khởi tạo filter
					filterTab.modalFilter.attr( 'data-filter-id', filterID );
				}

				// Hàm này sẽ điền các tiêu chí lọc đã chọn ( nếu là edit ) vào modal filter
				// Nếu mới thì sẽ không có tiêu chí nào
				filterTab.setFilterItems( filterID );

				filterCondition.init( edit );
				renderCondition.handleTabContent();
			} );
		},
		saveFilter: function() {

			// Save tên bộ lọc.
			var filterName = $( '.filter__name input' ).val();
			if ( filterName == '' ) {
				alert( 'Xin hãy nhập tên bộ lọc' );
				return;
			}

			var filterID = filterTab.modalFilter.attr( 'data-filter-id' );

			// Lưu filter vào local storage.
			var filterItems = $( '.tab-content__item input:checked' ),
				filterData = [ { name: filterName } ],
				items = [];
			filterItems.each( function() {
				items.push( {
					'label': this.value,
					'tooltip': this.dataset.tooltip
				} );
			} );
			filterData.push( { items: items } );

			localStorage.setItem( filterID, JSON.stringify( filterData ) );

			filterTab.addNewFilter( filterID, filterName );

			// Ẩn modal
			handleHideModals();

			filterTab.addFilterData( filterID );
			filterTab.showFilterTable();

			// Nếu có item ở dropdown rồi thì update tên.
			if ( $( '.modal--filter' ).attr( 'data-modal-edit' ) === 'enable' ) {
				var dropDownItem = $( '.list__name[data-filter-id="' + filterID + '"]');
				dropDownItem.find( 'span' ).text( filterName );
			}

			renderCondition.selected = {};
		},
		addNewFilter: function( filterID, filterName ) {
			// Nếu đang edit filter thì không thêm vào dropdown.
			if ( $( '.modal--filter' ).attr( 'data-modal-edit' ) === 'enable' ) {
				return;
			}
			var newItem = '\
				<li class="list__item list__item--filter">\
					<span class="list__name txt-white" data-filter-id="' + filterID + '"><span>' + filterName + '</span></span>\
					<input class="is-hidden" type="text">\
					<span class="list__buttons">\
						<span class="txt-white btn--edit"><i class="fas fa-edit"></i></span>\
						<span class="txt-white btn--delete"><i class="fas fa-times"></i></aspan>\
					</span>\
				</li>\
			';
			$( '.dropdown-menu--loc' ).find( 'ul' ).append( newItem );
		},

		/**
		 * Thêm data từ filter ID vào bảng hiển thị kết quả lọc
		 */
		addFilterData: function( filterID ) {
			var filterData  = filterTab.getFilterData( filterID );
			var filterItems = filterData.items;

			var $container = $( '#ket-qua-loc' ),
				$body = $container.find( '.table__body tr' );

			// Điền tên bộ lọc ở phía trên bảng kết quả:
			$( '.filter-result__name' ).text( filterData.name );

			// Dữ liệu cho các mã trong bảng theo filter này, mỗi dòng cho 1 mã
			var data = [];

			// Đoạn này chèn dữ liệu sample.
			$body.each( function() {
				var itemData = [];
				filterItems.forEach( function() {
					itemData.push( Math.floor( Math.random() * Math.floor( 1000 ) ) );
				} );
				data.push( itemData );
			} );

			// Thêm dữ liệu vào bảng giá.
			var $header = $container.find( '.table__header tr:first-child' );

			// Xóa các cột của filter cũ.
			$container.find( '.filter-column' ).remove();

			// Thêm các cột của filter mới.
			var newCols = [];
			filterItems.forEach( function( filter ) {
				newCols.push( '<th rowspan="2" class="td-col-2 filter-column" sortable>' + filter.label + ' <i class="tooltip fas fa-info-circle" data-tippy-content="' + filter.tooltip + '"></i>' + '</th>' );
			} );
			$header.append( newCols.join( '' ) );

			data.forEach( function( value, index ) {
				value = value.map( function( singleValue ) {
					return '<td class="txt-white td-col-2 filter-column">' + singleValue + '</td>';
				} );
				$body.eq( index ).append( value.join( '' ) );
			} );

			// Gán dữ liệu vào nút sửa bộ lọc
			$( '.filter__edit' ).attr( 'data-filter-id', filterID );
			$( '.filter-result__name' ).attr( 'data-filter-id', filterID );

			runTooltip();
		},
		showFilterTable: function() {
			var $container = $( '#ket-qua-loc' );

			$tables.removeClass( 'table--active' );
			$container.addClass( 'table--active' );
			$( '.visble-on-filter' ).removeClass( 'is-hidden' );
			$viewMode.removeClass( 'is-active' );

			stickyHeader();
		},

		/**
		 * Khi click vào danh sách lọc ở menu xổ xuống thì hiện ra bảng kết quả lọc
		 */
		handleClickOnFilterName: function() {
			$( '.dropdown-menu--loc' ).on( 'click', '.list__name', function() {
				var filterID = $( this ).attr( 'data-filter-id' );

				filterTab.addFilterData( filterID );
				filterTab.showFilterTable();

				highlightTab.bind( this )();
			} );
		},

		setFilterItems: function( filterID ) {
			if ( ! filterID ) {
				return;
			}

			var filterData 	= filterTab.getFilterData( filterID );
			var filterItems = filterData.items;
			var filterName  = filterData.name;

			$( '.filter__name input' ).val( filterName );

			var $inputs = $( '.tab-content__item input' );

			// Reset filter trước khi lấy dữ liệu mới và add vào filter.
			filterTab.resetFilterItems( $inputs );

			var filterItemsLabel = filterItems.map( function( item ) {
				return item.label;
			} );
			var $checked = $inputs.filter( function() {
				return filterItemsLabel.indexOf( this.value ) !== -1;
			} );
			$checked.prop( 'checked', true ).trigger( 'change' );
		},

		resetFilterItems: function( $inputs ) {
			$inputs.prop( 'checked', false );
			$('.filter__edit-condition').empty();
		}
	}

	function runTooltip() {
		tippy( '.tooltip', {
			arrow: true,
			placement: 'top',
			size: 'large',
			theme: 'google',
		} );
		tippy( '.market__sub__link', {
			arrow: true,
			placement: 'bottom',
			size: 'large',
			theme: 'google',
		} );
	}

	function toggleTables() {
		$body.on( 'click', '.has-table', function( e ) {
			e.stopPropagation();
			e.preventDefault();
			var href = $( this ).attr( 'href' );
			if ( -1 !== ['#thoa-thuan', '#khuyen-nghi'].indexOf( href ) ) {
				/* $( '.stock-header__view-modes a' )
					.removeClass( 'active' )
					.removeAttr( 'href' )
					.css( 'pointer-events', 'none' ); */
				$( '.visble-on-filter' ).addClass( 'is-hidden' );
			}

			if ( href === '#thoa-thuan' ) {
				$( '.ty-gia' ).text( 'Giá x 1000 VND. Khối lượng x 1 cổ phiếu' );
			}

			$tables.removeClass( 'table--active' );
			$tables.filter( href ).addClass( 'table--active' );
			stickyHeader();

			// Ẩn cái header của kết quả lọc đi.
			$( '.visble-on-filter' ).addClass( 'is-hidden' );
			// Chạy lại stickyHeader vì khi hiện kq lọc thay đổi margin top.
			stickyHeader();
		} );
	}

	function toggleTab() {
		$body.on( 'click', '.tab__item', function( e ) {
			e.preventDefault();
			var $this = $( this );

			$this.siblings().removeClass( 'active' );
			$this.addClass( 'active' );
			$tabContent.removeClass( 'active' );
			$tabContent.filter( $this.attr( 'href' ) ).addClass( 'active' );
		} );
	}

	function toggleClassActive() {
		toggleClassItems.forEach( function( item ) {
			$body.on( 'click', item, function( e ) {
				e.preventDefault();
				var $this = $( this );
				$( item ).removeClass( 'active' );
				$this.addClass( 'active' );
			} );
		} );
	}

	/**
	 * Khởi tạo modals
	 */
	function initModals() {
		modals.forEach( function( modal ) {
			$( '.init-' + modal + '-modal' ).on( 'click', function( e ) {
				e.preventDefault();
				if ( 'chi-so' === modal ) {
					chiSo.reset();
				}
				$body.addClass( 'modal-' + modal + '-enable' );
			} );
		} );
	}

	function handleHideModals() {
		$body.removeClass( function( index, className ) {
			return (className.match(/\bmodal-\S+/g) || []).join(' ');
		} );
	}

	/**
	 * Ẩn modal
	 */
	function hideModals() {
		$( '.modal__background, .modal__close, .modal__close--btn' ).on( 'click', function() {
			handleHideModals();
		} );
	}

	 var renderCondition = {
        selected: {},
        /**
         * Xử lý phần lựa chọn condition
         */
        handleTabContent: function () {
             $body.on( 'change', '.tab-content__item input', function( e ) {
                var $this = $( this );
                var id    = $this.prop( 'id' );
                if ( ! $this.is( ':checked' ) ) {
                    delete renderCondition.selected[ id ];
                } else {
                    renderCondition.selected[ id ] = {
                        elem    : $this,
                        min     : $this.attr( 'data-min' ),
                        max     : $this.attr( 'data-max' ),
                        startMin: $this.attr( 'data-start-min' ),
                        startMax: $this.attr( 'data-start-max' ),
                    };
                }
                var selected = $body.find( '.tab-content__item input:checked' );
                if ( selected.length > 7 ) {
                    // Chỉ được đặt tối đa 7 item, nếu lớn hơn thì alert.
                    setTimeout(function () {
                        $this.prop('checked', false);
                    }, 0);
                    e.preventDefault();
                    e.stopPropagation();
                    alert('Quý khách hàng vui lòng chỉ chọn tối đa 7 tiêu chí lọc');
                    return false;
                }
                renderCondition.renderConditionItems(renderCondition.selected);
            });
        },

        /**
         * Render các condition trong phần edit khi lựa chọn xong.
         */
        renderConditionItems: function () {
            var selected = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            console.log("selected", selected);
            var $filterConditionWrap = $('.filter__edit-condition');
            var html = '';
            if (!selected) {
                $filterConditionWrap.html('');
                return;
            }
            Object.keys( selected ).forEach( function( id ) {
                var selectedItem = selected[id],
                    condition = selectedItem.elem.next( 'span' ).html();

                html += '<div class="condition__item d-flex flex-wrap">\
                            <div class="condition__edit">\
                                <label class="custom-checkbox custom-checkbox--1">\
                                    <input checked type="checkbox" name="" value=""><span>' + condition + '</span>\
                                </label>\
                                <span class="remove" data-input="' + id + '"><i class="fas fa-times"></i></span>\
                            </div>\
                            <div class="condition__slider-wrapper d-flex flex-wrap">\
                                <input type="number" name="low">\
                                <div class="condition__slider" data-input="' + id + '" data-min="' + selectedItem.min + '" data-max="' + selectedItem.max + '" data-start-min="' + selectedItem.startMin + '" data-start-max="' + selectedItem.startMax + '"></div>\
                                <input type="number" name="top">\
                            </div>\
                        </div>';
            } );
            $filterConditionWrap
                .html(html)
                .on('click', '.condition__item .remove', function () {
                    var id = $(this).data('input');
           			delete renderCondition.selected[ id ];
                    $('#' + id).prop('checked', false);
                    $(this).closest('.condition__item').remove();
                });
            renderCondition.initRangeSlider();
        },

        /**
         * Tạo range slider sử dụng plugin nouislider.
         */
        initRangeSlider: function () {
            var sliders = document.querySelectorAll('.condition__slider');
            // Không dùng forEach vì chậm và ie không hỗ trợ foreach.
            for (var i = 0, len = sliders.length; i < len; i++) {
                var slider = sliders[i];
                noUiSlider.create(slider, {
                    start: [parseFloat( slider.dataset.startMin ), parseFloat( slider.dataset.startMax )],
                    connect: true,
                    format: {
                        to: function (value) {
                            return +parseFloat(value).toFixed(2); // convert toFixed string to number
                        },
                        from: function (value) {
                            return +parseFloat(value).toFixed(2);
                        }
                    },
                    range: {
                        min: [parseFloat(slider.dataset.min)],
                        max: [parseFloat(slider.dataset.max)]
                    }
                });
                renderCondition.updateSliderByInput(slider);
            };
        },
        updateSliderByInput: function (slider) {
            var inputLow = slider.previousElementSibling;
            var inputTop = slider.nextElementSibling;

            // Thay đổi giá trị input bằng slider.
            slider.noUiSlider.on('update', function ( values, handle ) {
                var id = this.target.dataset.input;
                renderCondition.selected[id].startMin = values[ 0 ];
                renderCondition.selected[id].startMax = values[ 1 ];

                inputLow.value = values[ 0 ];
                inputTop.value = values[ 1 ];
            });

            inputLow.addEventListener('change', function () {
                slider.noUiSlider.set([this.value, null]);
            });
            inputTop.addEventListener('change', function () {
                slider.noUiSlider.set([null, this.value]);
            });
        }
    };

	var filterCondition = {
		$tabItems: $( '.tab__item' ),
		$tabContentItems: $( '.tab-content__item' ),
		init: function( edit ) {
			if ( edit !== 'enable' ) {
				filterCondition.handleResetByButton();
			}
			var timeout = null;
			$( '.input__search input' ).on( 'input', function() {
				var value = xoaDau( $( this ).val().toLowerCase() );
				if ( timeout ) {
					clearTimeout( timeout );
				}
				timeout = setTimeout( filterCondition.handle, 400, value );
			} );
			filterCondition.resetByButton();

			filterCondition.initDropdown();
			filterCondition.selectSanNganh();
		},

		initDropdown: function() {
			// unbind click event để tránh tích lũy click event.
			$( '.filter__dropdown' ).off().on( 'click', '.filter__label, i', function( e ) {
				e.preventDefault();
				$( this ).parent()
					.toggleClass( 'filter__dropdown--visible' )
					.find( 'i' ).toggleClass( 'fa-angle-up' );
			} );
		},

		selectSanNganh: function() {
			$( '.filter__choices' ).on( 'change', 'input', function() {
				var $this = $( this ),
					$container = $this.closest( '.filter__dropdown' ),
					$inputs = $container.find( 'input' ),
					$all = $inputs.filter( '.all' ),
					$normal = $inputs.not( $all ),
					$checked = $normal.filter( ':checked' );

				// Toggle all.
				if ( $this.hasClass( 'all' ) ) {
					var checked = $this.prop( 'checked' );
					$normal.prop( 'checked', checked );
					$checked = checked ? $normal : [];
				}

				// Set .all checked or unchecked.
				else {
					$all.prop( 'checked', $normal.length === $checked.length );
				}

				// Set label.
				var $label = $container.find( '.filter__label' );

				// Select none.
				if ( ! $checked.length ) {
					$label.text( $label.data( 'none' ) );
					return;
				}

				// Select all.
				if ( $all.prop( 'checked' ) ) {
					$label.text( $all.val() );
					return;
				}

				// Select some items.
				var label = [];
				$checked.each( function() {
					label.push( this.value );
				} );
				$label.text( $label.data( 'label' ) + ' ' + label.join( ', ' ) );
			} );
		},

		handle: function( value ) {
			if ( value === '' ) {
				filterCondition.resetByKeyboard();
				return;
			}
			filterCondition.$tabContentItems.children( 'span' ).each( function() {
				var $this       = $( this );
				var $tabContent = $this.closest( '.tab-content__item' );
				var text        = xoaDau( $this.text().toLowerCase() );
				if ( text.indexOf( value ) !== -1 ) {
					$tabContent.addClass( 'is-visible' ).removeClass( 'is-hidden' );
				} else {
					$tabContent.removeClass( 'is-visible' ).addClass( 'is-hidden' );
				}
			} );
			var visibleItems = filterCondition.getVisibleConditionTabItems();

			// trước khi filter cho hiện hết các tabItems.
			filterCondition.$tabItems.removeClass( 'is-hidden' ).each( function() {
				var href = $( this ).attr( 'href' ).replace( '#', '' );
				var index = visibleItems.indexOf( href );
				if ( index === 0 ) {
					$( this ).addClass( 'active' ).trigger( 'click' );
				} else if ( index === -1 ) {
					$( this ).addClass( 'is-hidden' );
				}

			} );
		},
		/**
		 * Reset lại filter khi xóa, delete.
		 */
		resetByKeyboard: function() {
			filterCondition.$tabItems.removeClass( 'is-hidden active' );
			filterCondition.$tabContentItems.removeClass( 'is-hidden is-visible' );
			filterCondition.$tabItems.first().trigger( 'click' );
		},
		/**
		 * Reset lại filter sau khi nhấn nút.
		 */
		resetByButton: function() {
			$( '.buttons__reset' ).on( 'click', function() {
				filterCondition.handleResetByButton();
			} );
		},
		handleResetByButton: function() {
			$( '.tab-content__item input' ).prop( 'checked', false );
			$( '.input__search input' ).val('');
			$( '.filter__dropdown .all' ).prop( 'checked', true ).trigger( 'change' );
			$( '.filter__name input' ).val('');
			filterCondition.resetByKeyboard();
			renderCondition.renderConditionItems();
		},
		getVisibleConditionTabItems: function() {
			var visibleItems = [];
			$( '.condition__tab-content' ).each( function() {
				if ( $( this ).has( '.is-visible' ).length ) {
					var id = $( this ).prop( 'id' );
					visibleItems.push( id );
				}
			} );
			return visibleItems;
		},
	};

	function xoaDau( text ) {
		return text
			.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a")
			.replace(/đ/g, "d")
			.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y")
			.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u")
			.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o")
			.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e")
			.replace(/ì|í|ị|ỉ|ĩ/g,"i");
	}

	var chiSo = {
		init: function() {
			chiSo.$popup     = $( '#chi-so-modal' );
			chiSo.$submitBtn = chiSo.$popup.find( '.btn--save-chi-so' );
			chiSo.$chart     = $( '.chart-area' );

			chiSo.litmitCheckedItem();
			chiSo.submit();
		},

		litmitCheckedItem: function() {
			chiSo.$popup.on( 'change', 'input[type="checkbox"]', function( e ) {
				e.preventDefault();
				var $this = $( this );
				var $checked = chiSo.getCheckedItems();
				if ( $checked.length > 8 ) {
					alert(  "Quý khách vui lòng chỉ chọn tối đa 8 chỉ tiêu" );
					setTimeout(function() {
						$this.prop('checked', false);
				    }, 0);
					e.preventDefault();
					e.stopPropagation();
					return;
				}
			} );
		},

		getCheckedItems: function() {
			return chiSo.$popup.find( 'input[type="checkbox"]:checked' );
		},

		getIDsFromCheckedItems: function() {
			var IDs = [];
			var $checked = chiSo.getCheckedItems();
			$checked.each( function() {
				IDs.push( $( this ).attr( 'id' ) );
			} )
			return IDs;
		},

		getOutputFromCheckItems: function( IDs ) {
			var output = '';
			IDs.forEach( function( id ) {
				if ( 'GDTT' === id ) {
					output += '<div class="chart__item chart__item--table">\
						<table class="chart__table">\
							<tr>\
								<th></th>\
								<th>GD</th>\
								<th>TT</th>\
								<th>Tổng GT</th>\
							</tr>\
							<tr>\
								<td>VN-Index</td>\
								<td class="txt-green">12.2</td>\
								<td class="txt-green">130</td>\
								<td class="txt-green">52.1</td>\
							</tr>\
							<tr class="half-hidden">\
								<td>VN30-Index</td>\
								<td class="txt-red">12.2</td>\
								<td class="txt-red">130</td>\
								<td class="txt-red">52.1</td>\
							</tr>\
							<tr>\
								<td>HNX-Index</td>\
								<td class="txt-red">12.2</td>\
								<td class="txt-red">130</td>\
								<td class="txt-red">52.1</td>\
							</tr>\
							<tr class="half-hidden">\
								<td>UPCOM-Index</td>\
								<td class="txt-green">12.2</td>\
								<td class="txt-green">130</td>\
								<td class="txt-green">52.1</td>\
							</tr>\
						</table>\
					</div>';
				} else {
					output += '<div id="chi-so-' + id + '" class="chart__item">\
						<div class="chart__image">\
							<img src="images/chart.svg" alt="chart">\
						</div>\
						<div class="chart__info">\
							<div>\
								<span class="info__ten">' + id + '</span>\
								<span class="info__point info__point--down"><i class="fas fa-caret-down"></i> 912.32 (1.23  1.32%)</span>\
							</div>\
							<div>\
								<span class="info__cp">312.235 <span class="txt-yellow">CP</span></span>\
								<span class="info__ty">951.231 <span class="txt-yellow">Tỷ</span></span>\
							</div>\
							<div>\
								<span class="txt-green"><i class="fas fa-caret-up"></i> 11 <span>(<span class="txt-pink">0</span>)</span></span>\
								<span class="txt-yellow"><i class="fas fa-square"></i> 4</span>\
								<span class="txt-red"><i class="fas fa-caret-down"></i> 15 <span>(<span class="txt-cyan">0</span>)</span></span>\
								<span>Liên tục</span>\
							</div>\
						</div>\
					</div>'
				}
			} );
			return output;
		},

		submit: function() {
			chiSo.$submitBtn.on( 'click', function() {
				var IDs = chiSo.getIDsFromCheckedItems();
				var slidesHTML = chiSo.getOutputFromCheckItems( IDs );
				chiSo.$chart.slick( 'unslick' );
				chiSo.$chart.empty();
				chiSo.$chart.append( slidesHTML );
				chiSo.$chart.slick( chartSliderArgs );
			} )
		},

		/**
		 * Reset lại filter sau khi nhấn nút.
		 */
		reset: function() {
			var $defaultCheck = $( '.chi-so-item' ).find( 'input[data-default]' );
			$( '#set-chi-so-default' ).on( 'click', function() {
				$( '.chi-so-item input' ).prop( 'checked', false );
				$defaultCheck.prop( 'checked', true );
			} );
		}
	}


	/**
	 * Toggle giá trị td, th
	 */
	function toggleTableData() {
		$( '.has-toggle a' ).on( 'click', function( e ) {
			e.stopPropagation();
			var dataToggle = $( this ).attr( 'data-toggle' );
			if ( 'gia' === dataToggle ) {
				var current = $( '.has-toggle.toggle--gia' ).attr( 'colspan' );
				var $table = $( this ).closest( '.stock-table' );
				$table.toggleClass( 'is-hidden-gia' ); // Fix lỗi khi thêm toggle từ giá sang dư và thêm mã thì bị lệch
				$( '.has-toggle.toggle--gia' ).attr( 'colspan', current == 3 ? 2 : 3 );
				$( '.phai-sinh .item-info > td' ).attr( 'colspan', current == 3 ? 27: 28 );
				$( '.chung-quyen .item-info > td' ).attr( 'colspan', current == 3 ? 28: 29 );
			}
			$( '.toggle--' + dataToggle ).toggleClass( 'toggling' );
		} );
	}

	var danhMucMBS = {
		init: function() {
			danhMucMBS.navTab             = $( '.dropdown-menu--danh-muc' ).prev( '.nav-tab' );
			danhMucMBS.$tableWrapper      = $( '#danh-muc' );
			danhMucMBS.$tableCoBanWrapper = $( '#danh-muc-co-ban' );
			danhMucMBS.bodyIndex          = 1;

			danhMucMBS.addCode();
			danhMucMBS.editDanhMuc();
			danhMucMBS.createDanhMuc();
			danhMucMBS.deleteDanhMuc();
			danhMucMBS.handleToggleTableBody();
		},

		bodyMockup: function( index ) {
			return '\
				<table class="table__body" data-bang-gia-body-for="' + index + '">\
					<colgroup>\
						<col class="col-code">\
						<col class="col-price">\
						<col class="col-price">\
						<col class="col-price">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-diff">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-price">\
						<col class="col-vol">\
						<col class="col-vol col-vol--lg">\
						<col class="col-price">\
						<col class="col-price">\
						<col class="col-price toggle--gia toggle--hidden">\
						<col class="col-vol">\
						<col class="col-vol">\
					</colgroup>\
					<thead class="is-hidden">\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
						<th></th>\
					</thead>\
					<tbody>\
				</tbody>\
			</table>\
			';
		},

		bodyCoBanMockup: function( index ) {
			return '\
				<table class="table__body" data-co-ban-body-for="' + index + '">\
					<colgroup>\
						<col>\
						<col>\
						<col class="col-nganh">\
						<col class="col-kl">\
						<col class="col-10-phien">\
						<col class="col-gt">\
						<col>\
						<col>\
						<col>\
						<col>\
						<col>\
						<col>\
						<col>\
						<col>\
						<col class="col-eps">\
						<col>\
						<col>\
						<col class="col-nn">\
						<col class="col-nn">\
					</colgroup>\
					<thead class="is-hidden">\
						<tr>\
							<th rowspan="2">Mã CK</th>\
							<th rowspan="2">Sàn</th>\
							<th rowspan="2" class="sortable">Ngành</th>\
							<th rowspan="2" class="td-col-2 sortable">Tổng KL <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2" class="sortable">%KL / KLTB<br>10 phiên <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2" class="td-col-2 sortable">Tổng GT <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2" class="sortable cell-highlight">TC</th>\
							<th rowspan="2" class="sortable cell-highlight">Khớp lệnh</th>\
							<th rowspan="2" class="sortable cell-highlight">+/-</th>\
							<th rowspan="2" class="sortable cell-highlight">%</th>\
							<th rowspan="2" class="td-col-2 sortable">Vốn hóa</th>\
							<th rowspan="2">PE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2">PB <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2">PS <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2">EPS<br>4 quý <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2">ROE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th rowspan="2">ROA <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
							<th colspan="2">NĐTNN <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>\
						</tr>\
						<tr>\
							<th>Mua</th>\
							<th>Bán</th>\
						</tr>\
					</thead>\
					<tbody>\
					</tbody>\
				</table>\
			';
		},

		/**
		 * Edit list item
		 */
		editDanhMuc: function() {
			$( '.dropdown-menu--danh-muc' ).on( 'click', '.btn--edit', function( e ) {
				e.preventDefault();
				var $this         = $( this );
				var $listItem     = $this.closest( '.list__item' );
				var $listName     = $listItem.find( '.list__name' ).toggleClass( 'is-hidden' );
				var $input        = $listItem.find( 'input' ).toggleClass( 'is-hidden' );
				var $textWrapper  = $listName.find( 'span' );
				var $dropDownMenu = $listItem.closest( '.dropdown-menu' );

				$input.val( $textWrapper.text() );

				$input.on( 'change', function( e ) {
					$textWrapper.text( e.target.value );
					danhMucMBS.navTab.text( e.target.value );
				} );

				$input.on( 'keypress', function( e ) {
					if ( e.which == '13' ) {
						$listName.removeClass( 'is-hidden' );
						$( this ).addClass( 'is-hidden' );
					}
				} )

			} );
		},

		/**
		 * Edit list item
		 */
		deleteDanhMuc: function() {
			$( '.dropdown-menu--danh-muc' ).on( 'click', '.btn--delete', function( e ) {
				e.preventDefault();
				e.stopPropagation(); // khi click sẽ không trigger click vào thẻ a phía trên.
				var $listItem     = $( this ).closest( '.list__item' );
				var $prevItem     = $listItem.prev( '.list__item' );
				var $prevItemText = $prevItem.find( '.list__name span' ).text();
				$listItem.remove();

				// Update tên navtab bằng tên của danh mục phía trước
				danhMucMBS.updateNavTab( $prevItemText );

				// Chuyển sang bảng tương ứng với danh mục trước
				$prevItem.children( 'a' ).trigger( 'click' );

				// Xóa danh mục thì xóa luôn bảng tương ứng với danh mục đó.
				var tableBodyID = $listItem.children( 'a' ).attr( 'data-bang-gia-body' );
				danhMucMBS.deleteTable( tableBodyID );
			} );
		},

		deleteTable: function( tableBodyID ) {
			$( 'table[data-bang-gia-body-for="' + tableBodyID + '"]' ).remove();
			$( 'table[data-co-ban-body-for="' + tableBodyID + '"]' ).remove();
		},

		createDanhMuc: function() {
			$( '.danh-muc__tao-moi' ).on( 'submit', function( e ) {
				e.preventDefault();
				if ( chuaDangNhap( 'Quý khách vui lòng đăng nhập để sử dụng tính năng này' ) ) {
					return;
				}

				danhMucMBS.bodyIndex++;

				var $input = $( this ).find( 'input[type="text"' );
				var value = $input.val();
				if ( value === '' ) {
					return;
				}
				var newItem = '\
					<li class="list__item">\
						<a href="#danh-muc" data-view-mode="danh-muc" data-bang-gia-body="' + danhMucMBS.bodyIndex + '" class="has-table">\
							<span class="list__name">\
								<i class="fas fa-check-circle"></i><span>' + value + '</span>\
							</span>\
							<input class="is-hidden" type="text">\
							<span class="list__buttons">\
								<span class="btn--edit" href="#" title="Sửa"><i class="fas fa-edit"></i></span>\
								<span class="btn--delete" href="#" title="Xóa"><i class="fas fa-times"></i></span>\
							</span>\
						</a>\
					</li>\
				';
				$( '.dropdown-menu--danh-muc .dropdown-menu__list' ).append( newItem );
				danhMucMoi.addAvailableDanhMuc();
				$input.val( '' );

				danhMucMBS.updateNavTab( value );
				danhMucMBS.appendBody();
				danhMucMBS.appendCoBanBody();
				danhMucMBS.toggleTableBody( danhMucMBS.bodyIndex );
			} )
		},

		appendBody: function() {
			var $table = danhMucMBS.bodyMockup( danhMucMBS.bodyIndex );
			$table = $( $.parseHTML( $table ) );
			danhMucMBS.$tableWrapper.append( $table );
		},

		appendCoBanBody: function() {
			var $table = danhMucMBS.bodyCoBanMockup( danhMucMBS.bodyIndex );
			$table = $( $.parseHTML( $table ) );
			danhMucMBS.$tableCoBanWrapper.append( $table );
		},

		updateNavTab: function( val ) {
			danhMucMBS.navTab.text( val );
		},

		handleToggleTableBody: function() {
			$body.on( 'click', '.list__item a', function() {
				var $this         = $( this );
				var value         = $this.find( '.list__name span' ).text();
				var dataTableBody = $this.attr( 'data-bang-gia-body' );

				// Update navtab text.
				danhMucMBS.updateNavTab( value );

				// toggle table body.
				danhMucMBS.toggleTableBody( dataTableBody );
			} );
		},

		toggleTableBody: function( dataTableBody ) {
			var $tableBody      = danhMucMBS.$tableWrapper.find( '.table__body' );
			var $tableCoBanBody = danhMucMBS.$tableCoBanWrapper.find( '.table__body' );
			var viewModes       = $( '.stock-header__view-modes.is-active' ).children( 'a' );

			$tableBody.addClass( 'is-hidden' );
			$tableCoBanBody.addClass( 'is-hidden' );

			$tableBody.filter( '[data-bang-gia-body-for="' + dataTableBody + '"]' ).removeClass( 'is-hidden' );
			$tableCoBanBody.filter( '[data-co-ban-body-for="' + dataTableBody + '"]' ).removeClass( 'is-hidden' );

			// Active bảng giá.
			viewModes.removeClass( 'active' );
			viewModes.eq( 0 ).addClass( 'active' );

			$( '#add-code' ).attr( 'data-bang-gia-body', dataTableBody );
		},

		addCode: function() {
			addCode.add();
			addCode.manualAdd();
			addCode.clickAdd();
		}
	}

	function toggleViewMode() {
		$body.on( 'click', '.nav-tab, .dropdown-menu a', function( e ) {
			var $this = $( this );
			var viewMode = $this.attr( 'data-view-mode' );
			if ( typeof viewMode === typeof undefined && viewMode === false ) {
				return;
			}
			$viewMode.removeClass( 'is-active' );
			$( '.view-modes--' + viewMode ).addClass( 'is-active' );
			$( '.view-mode' ).removeClass( 'active' );
			$( '.view-modes--' + viewMode ).find( '.view-mode' ).eq(0).addClass( 'active' );
		} )
	}

	function toggleChart() {
		$( '.stock-header__chart-toggle' ).on( 'click', function() {
			if ( $chartArea.hasClass( 'half-hidden' ) ) {
				$chartArea.removeClass( 'half-hidden' ).addClass( 'is-hidden' );
				$( this ).addClass( 'rotate-arrow' );
				stickyHeader();
				return;
			} else if ( $chartArea.hasClass( 'is-hidden' ) ) {
				$chartArea.removeClass( 'is-hidden' );
				$( this ).removeClass( 'rotate-arrow' );
				stickyHeader();
				return;
			}
			$chartArea.addClass( 'half-hidden' );
			stickyHeader();
		} );
	}

	function initChartSlider() {
		$( '.chart-area' ).removeClass( 'is-hidden' ).slick( chartSliderArgs );
	}

	function removeCode() {
		$body.on( 'click', '.txt-ma .remove', function( e ) {
			e.preventDefault();
			$( this ).closest( 'tr' ).remove();
		} );
	}

	var addCode = {
		codes: [
			{
				value: 'kdc',
				label: 'KDC - Công ty Cổ phần thực phẩm Kido - HOSE',
				type: 'up',
				data: {
					ma: 'KDC',
					ten: 'Công ty Cổ phần thực phẩm Kido - HOSE',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 96.61,
					ban_1: 97.5,
					tong_kl_mua: 131,
					tong_kl_ban: 372,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_fcf: 212,
					ps: 212,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					roa: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},
			{
				value: 'kdf',
				label: 'KDF - Công ty Cổ phần thực phẩm Kido - UPCOM',
				type: 'down',
				data: {
					ma: 'KDF',
					ten: 'Công ty Cổ phần thực phẩm Kido - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 93.61,
					ban_1: 94.50,
					tong_kl_mua: 125,
					tong_kl_ban: 365,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},
			{
				value: 'kdg',
				label: 'KDG - Công ty Cổ phần thực phẩm Kido - HOSE',
				type: 'up',
				data: {
					ma: 'KDG',
					ten: 'Công ty Cổ phần thực phẩm Kido - HOSE',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.50,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},
			{
				value: 'kdh',
				label: 'KDH - Công ty Cổ phần thực phẩm Kido - UPCOM',
				type: 'down',
				data: {
					ma: 'KDH',
					ten: 'Công ty Cổ phần thực phẩm Kido - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.5,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},

			{
				value: 'kah',
				label: 'KAH - Công ty Cổ phần thực phẩm Kado - UPCOM',
				type: 'down',
				data: {
					ma: 'KAH',
					ten: 'Công ty Cổ phần thực phẩm Kado - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.5,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},

			{
				value: 'kbh',
				label: 'KBH - Công ty Cổ phần thực phẩm Kibo - UPCOM',
				type: 'down',
				data: {
					ma: 'KBH',
					ten: 'Công ty Cổ phần thực phẩm Kibo - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.5,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},

			{
				value: 'kch',
				label: 'KcH - Công ty Cổ phần thực phẩm Kico - UPCOM',
				type: 'down',
				data: {
					ma: 'KCH',
					ten: 'Công ty Cổ phần thực phẩm Kico - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.5,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},

			{
				value: 'kih',
				label: 'KIH - Công ty Cổ phần thực phẩm Kio - UPCOM',
				type: 'down',
				data: {
					ma: 'KIH',
					ten: 'Công ty Cổ phần thực phẩm Kio - UPCOM',
					nganh: 'Hàng và dịch vụ công nghiệp',
					tran: 60.30,
					san: 52.50,
					tc: 56.40,
					mua_g3: 56.60,
					mua_kl3: 2.15,
					mua_g2: 56.70,
					mua_kl2: 3.71,
					mua_g1: 56.80,
					mua_kl1: 7.21,
					khop_lech: 0.50,
					khop_lech_phan_tram: '10%',
					khop_gia: 56.90,
					khop_kl: 52,
					ban_g1: 57.00,
					ban_kl1: 3.62,
					ban_g2: 57.10,
					ban_kl2: 1.65,
					ban_g3: 3.95,
					ban_kl3: 1.65,
					tong_kl: 130.12,
					tong_gt: 140,
					gia_cao: 57.20,
					gia_tb: 56.90,
					gia_thap: 56.70,
					du_mua: 7.20,
					du_ban: 6.90,
					du_chenh: 6.70,
					nn_mua: 1.60,
					nn_ban: 15.60,
					nn_du: 126.10,
					nn_room: 2.5,
					nn_phan_tram: '3%',
					mua_1: 94.61,
					ban_1: 95.5,
					tong_kl_mua: 126,
					tong_kl_ban: 366,

					von_hoa: 152,
					doanh_thu: 1234,
					loi_nhuan_rong: 123,
					cfo: 1234,
					fcf: 6885,
					tong_tai_san: 11111,
					tong_no_phai_tra: 234,
					von_chu_so_huu: 3536,
					mbs_ranking: 214,
					magic_formula_score: 32334,
					f_score: 435,
					z_score: 243,
					m_score: 575,
					c_score: 565,
					so_huu_nuoc_ngoai: 214,
					room_con_lai: 2352,
					khoi_luong_giao_dich: 749,
					gia_tri_giao_dich: 357,
					khoi_ngoai_mua_dong: 324,
					tu_doanh_mua_dong: 214,
					gtgd_5phien: 314,
					gtgd_10phien: 714,
					gtgd_20phien: 914,
					bien_dong_gia_5phien: 200,
					bien_dong_gia_10phien: 200,
					bien_dong_gia_20phien: 200,
					eps: 166,
					pe: 736,
					pb: 425,
					p_tangible_bv: 212,
					ps: 212,
					p_fcf:324,
					tev_ebitda: 213,
					tang_truong_eps: 13,
					loi_tuc_co_tuc: 32,
					tang_truong_doanh_thu_svnt: 426,
					tang_truong_eps_svnt: 999,
					tang_truong_doanh_thu_kep: 426,
					tang_truong_eps_kep: 999,
					tang_truong_dps_kep: 5,
					c: 234,
					roe: 534,
					roce: 317,
					bien_loi_nhuan_hd: 32,
					suc_manh_tuong_duong_1thang: 3,
					suc_manh_tuong_duong_3thang: 6,
					suc_manh_tuong_duong_1nam: 3,
					thay_doi_kl_1_5ngay: 12,
					thay_doi_kl_10_3thang: 12,
					thay_doi_kl_10_6thang: 12,
					gia_cao52: 3,
					ma50: 3,
					ma200: 4,
					vq_tong_tai_san: 42,
					vq_khoan_phai_thu: 23,
					vq_khoan_phai_tra: 43,
					vq_hang_ton_kho: 12,
					cstt_ngan_han: 67,
					cstt_nhanh: 2,
					cstt_tien_mat: 7,
					don_bay_tai_chinh: 78,
					tldbcp_vay_lai: 5,
				}
			},
		],

		$input   : null,
		alertText: 'Quý khách vui lòng đăng nhập để sử dụng tính năng này',

		init: function() {
			addCode.$input = $( '#add-code' );
			/*addCode.add( $table );
			addCode.manualAdd( $table );
			addCode.clickAdd( $table );*/
		},

		getTablesToInsert: function() {
			var $tableBodies = [];
			$tableBodies[0] = $( '.table__body[data-co-ban-body-for="' + addCode.$input.attr( 'data-bang-gia-body' ) + '"' ).find( 'tbody' );
			$tableBodies[1] = $( '.table__body[data-bang-gia-body-for="' + addCode.$input.attr( 'data-bang-gia-body' ) + '"' ).find( 'tbody' );
			return $tableBodies;
		},

		add: function() {
			addCode.$input.autocomplete( {
				source: addCode.codes,
				select: function( event, ui ) {
					if ( chuaDangNhap( addCode.alertText ) ) {
						return;
					}
					var $tableBodies = addCode.getTablesToInsert();
					$tableBodies.forEach( function( $tableBody ) {
						addCode.addToTable( $tableBody, ui.item );
					} )
				}
			} );
		},

		manualAdd: function() {
			addCode.$input.on( 'keypress', function( e ) {
				if ( e.which != 13 ) {
					return;
				}
				e.preventDefault();
				if ( chuaDangNhap( addCode.alertText ) ) {
					return;
				}

				var code = addCode.$input.val().toLowerCase();

				var filtered = addCode.codes.filter( function( item ) {
					return item.value.toLowerCase().indexOf( code ) === 0;
				} );

				if ( filtered.length ) {
					var $tableBodies = addCode.getTablesToInsert();
					$tableBodies.forEach( function( $tableBody ) {
						addCode.addToTable( $tableBody, filtered[0] );
					} )
				}

				addCode.$input.val( '' );
				addCode.$input.autocomplete( 'close' );
			} );
		},

		clickAdd: function() {
			$( '#submit-code' ).on( 'click', function( e ) {
				e.preventDefault();
				if ( chuaDangNhap( addCode.alertText ) ) {
					return;
				}
				var code = addCode.$input.val().toLowerCase();

				var filtered = addCode.codes.filter( function( item ) {
					return item.value.toLowerCase() === code;
				} );

				if ( filtered.length ) {
					var $tableBodies = addCode.getTablesToInsert();
					$tableBodies.forEach( function( $tableBody ) {
						addCode.addToTable( $tableBody, filtered[0] );
					} )
				}

				addCode.$input.val( '' );
				addCode.$input.autocomplete( 'close' );
			} );
		},

		getRowHTML: function( $tableBody, color, data ) {
			var html = '';
			var $table = $tableBody.closest( '.table__body' );
			if ( $table.attr( 'data-co-ban-body-for' ) ) {
				html = '\
				<tr>\
					<td class="tooltip txt-ma txt-yellow txt-left" data-tippy-content="' + data.ten + '"><span class="code-wrap"><a href="./chi-tiet-ma.php" target="_blank">' + data.ma + '</a><i class="fas fa-times remove"></i></span></td>\
					<td class="txt-white txt-left">HOSE</td>\
					<td class="txt-white txt-left">Xây dựng và vật liệu</td>\
					<td class="txt-white">1.352.62</td>\
					<td class="txt-white">32.15%</td>\
					<td class="txt-white">52.326</td>\
					<td class="txt-yellow cell-highlight">56.90</td>\
					<td class="txt-green cell-highlight">57.90</td>\
					<td class="txt-green cell-highlight">2.10</td>\
					<td class="txt-green cell-highlight">3.23%</td>\
					<td class="txt-white td-col-2">1.202.326</td>\
					<td class="txt-white">12.1</td>\
					<td class="txt-white">1.3</td>\
					<td class="txt-white">2.1</td>\
					<td class="txt-white">1.355</td>\
					<td class="txt-white">12.23%</td>\
					<td class="txt-white">2.01%</td>\
					<td class="txt-white">15.60</td>\
					<td class="txt-white">26.21</td>\
				</tr>\
				';
			} else {
				html = '\
				<tr>\
					<td class="tooltip txt-ma cell-highlight ' + color + '" data-tippy-content="' + data.ten + '"><span class="code-wrap"><a class="chi-tiet-ma-url" href="./chi-tiet-ma.php" target="_blank">' + data.ma + '</a><i class="fas fa-times remove"></i></span></td>\
					<td class="txt-pink cell-highlight">' + data.tran + '</td>\
					<td class="txt-cyan cell-highlight">' + data.san + '</td>\
					<td class="txt-yellow cell-highlight">' + data.tc + '</td>\
					<td class="' + color + '">' + data.mua_g3 + '</td>\
					<td class="' + color + '">' + data.mua_kl3 + '</td>\
					<td class="' + color + '">' + data.mua_g2 + '</td>\
					<td class="' + color + '">' + data.mua_kl2 + '</td>\
					<td class="' + color + '">' + data.mua_g1 + '</td>\
					<td class="' + color + '">' + data.mua_kl1 + '</td>\
					<td class="' + color + ' cell-highlight toggle--percent">\
						<span class="hidden-first">' + data.khop_lech_phan_tram + '</span>\
						<span>' + data.khop_lech + '</span>\
					</td>\
					<td class="' + color + ' cell-highlight">' + data.khop_gia + '</td>\
					<td class="' + color + ' cell-highlight">' + data.khop_kl + '</td>\
					<td class="' + color + '">' + data.mua_g1 + '</td>\
					<td class="' + color + '">' + data.mua_kl1 + '</td>\
					<td class="' + color + '">' + data.mua_g2 + '</td>\
					<td class="' + color + '">' + data.mua_kl2 + '</td>\
					<td class="' + color + '">' + data.mua_g3 + '</td>\
					<td class="' + color + '">' + data.mua_kl3 + '</td>\
					<td class="txt-white toggle--tong">\
						<span class="hidden-first">' + data.tong_kl + '</span>\
						<span>' + data.tong_gt + '</span>\
					</td>\
					<td class="' + color + ' cell-highlight toggle--gia">\
						<span class="hidden-first">' + data.du_mua + '</span>\
						<span>' + data.gia_cao + '</span>\
					</td>\
					<td class="' + color + ' cell-highlight toggle--gia">\
						<span class="hidden-first">' + data.du_ban + '</span>\
						<span>' + data.gia_tb + '</span>\
					</td>\
					<td class="' + color + ' cell-highlight toggle--gia toggle--hidden">\
						<span>' + data.gia_thap + '</span>\
					</td>\
					<td class="' + color + ' toggle--dtnn">\
						<span class="hidden-first">' + data.nn_room + '</span>\
						<span>' + data.nn_mua + '</span>\
					</td>\
					<td class="' + color + ' toggle--dtnn">\
						<span class="hidden-first">' + data.nn_phan_tram + '</span>\
						<span>' + data.nn_ban + '</span>\
					</td>\
				</tr>\
				';
			}
			return html;
		},

		addToTable: function( $tableBody, item ) {
			var data = item.data,
				color = 'txt-green';

			switch ( item.type ) {
				case 'down':
					color = 'txt-red';
					break;
				case 'normal':
					color = 'txt-yellow';
					break;
			}

			var html = addCode.getRowHTML( $tableBody, color, data );

			$tableBody.append( html );

			var tableSort = new SortCodes( $tableBody, 'orderBangGia' );
			tableSort.init();
		}
	};

	var viewCode = {
		baseUrl: './chi-tiet-ma.php',

		codes: [
			{
				value: 'kdc',
				label: 'KDC - Công ty Cổ phần thực phẩm Kido - HOSE',
			},
			{
				value: 'kdf',
				label: 'KDF - Công ty Cổ phần thực phẩm Kido - UPCOM',
			},
			{
				value: 'kdg',
				label: 'KDG - Công ty Cổ phần thực phẩm Kido - HOSE',
			},
			{
				value: 'kdh',
				label: 'KDH - Công ty Cổ phần thực phẩm Kido - UPCOM',
			},
		],

		$input         : null,
		searchCodeAlert: 'Quý khách vui lòng đăng nhập để xem chi tiết mã cổ phiếu',
		viewCodeAlert  : 'Quý khách vui lòng đăng nhập để xem chi tiết mã cổ phiếu',

		init: function() {
			viewCode.$input = $( '#view-code' );
			viewCode.search();
			viewCode.manualSearch();
			viewCode.xemChiTiet();
		},

		search: function() {
			viewCode.$input.autocomplete( {
				source: viewCode.codes,
				select: function( event, ui ) {
					if ( chuaDangNhap( viewCode.searchCodeAlert ) ) {
						return;
					}
					window.location = viewCode.baseUrl + '?code=' + ui.item.value;
				}
			} );
		},

		manualSearch: function() {
			viewCode.$input.on( 'keypress', function( e ) {
				if ( e.which != 13 ) {
					return;
				}

				e.preventDefault();

				if ( chuaDangNhap( viewCode.searchCodeAlert ) ) {
					return;
				}

				var code = viewCode.$input.val().toLowerCase();

				var filtered = viewCode.codes.filter( function( item ) {
					return item.value.toLowerCase().indexOf( code ) === 0;
				} );

				if ( filtered.length ) {
					window.location = viewCode.baseUrl + '?code=' + filtered[0].value;
				}

				viewCode.$input.val( '' );
				viewCode.$input.autocomplete( 'close' );
			} );
		},

		clickSearch: function() {
			$( '#view-code-submit' ).on( 'click', function( e ) {
				e.preventDefault();

				if ( chuaDangNhap( viewCode.searchCodeAlert ) ) {
					return;
				}

				var code = viewCode.$input.val().toLowerCase();

				var filtered = viewCode.codes.filter( function( item ) {
					return item.value.toLowerCase() === code;
				} );

				if ( filtered.length ) {
					window.location = viewCode.baseUrl + '?code=' + filtered[0].value;
				}

				viewCode.$input.val( '' );
				viewCode.$input.autocomplete( 'close' );
			} );
		},

		xemChiTiet: function() {
			$body.on( 'click', '.chi-tiet-ma-url', function() {
				if ( chuaDangNhap( viewCode.viewCodeAlert ) ) {
					return false;
				}
				return true;
			} )
		}
	};


	var SortCodes = function ( selector, key ) {
		this.$el = $( selector );
		this.key = key;
	}
	SortCodes.prototype.sort = function() {
		var that = this;
		this.$el.sortable( {
			update: function( event, ui ) {
				var index = that.$el.find( 'tr' ).index( ui.item );
				ui.item.data( 'index', index );

				var order = JSON.parse( localStorage.getItem( that.key ) );
				order = order || {};

				// Push other codes down.
				Object.keys( order ).forEach( function( id ) {
					if ( order[id] >= index ) {
						order[id] = order[id] + 1;
					}
				} );

				// Save index for current item.
				order[ui.item.data( 'id' )] = index;

				localStorage.setItem( that.key, JSON.stringify( order ) );
			}
		} );
	};
	SortCodes.prototype.refresh = function() {
		var that = this,
			order = JSON.parse( localStorage.getItem( that.key ) );

		order = order || {};

		Object.keys( order ).forEach( function( id ) {
			var $item = that.$el.find( '[data-id="' + id + '"]' );
			if ( ! $item.length ) {
				return;
			}
			$item.insertBefore( $item.siblings( ':eq(' + order[id] + ')' ) );
		} );
	};
	SortCodes.prototype.init = function() {
		this.sort();
		this.refresh();
	}

	var phaiSinh = {
		alertText: null,

		init: function() {
			phaiSinh.toggleItemInfo();
			phaiSinh.toggleTab();
			phaiSinh.toggleTabTable();
			phaiSinh.alertText = 'Quý khách vui lòng đăng nhập để xem chi tiết mã phái sinh';
		},

		// Ẩn hiện thông tin chi tiết của mã trong màn hình bảng giá Phái sinh.
		toggleItemInfo: function() {
			var $info = $( '.phai-sinh .item-info' );
			$body.on( 'click', '.phai-sinh .item', function() {
				if ( chuaDangNhap( phaiSinh.alertText ) ) {
					return;
				}
				var $next = $( this ).next();
				$info.not( $next ).removeClass( 'is-active' );
				$next.toggleClass( 'is-active');
			} );
		},

		toggleTab: function() {
			$( '.phai-sinh .item-info__tabs' ).on( 'click', 'a', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.item-info' ).find( '.item-info__tab' );
				$tabs.removeClass( 'is-active' );

				var href = $this.attr( 'href' );

				if ( -1 !== href.indexOf( '#' ) ) {
					e.preventDefault();
					$tabs.filter( '[data-tab="' + href + '"]').addClass( 'is-active' );
				}
			} );
		},

		toggleTabTable: function() {
			$( '.phai-sinh .item-info__sidebar__nav' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );
				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.item-info' ).find( '.item-info__sidebar__tab' );
				$tabs.removeClass( 'is-active' );
				$tabs.filter( '[data-tab="' + $this.attr( 'href' ) + '"]').addClass( 'is-active' );
			} );
		}
	};

	var chungQuyen = {
		alertText: null,

		init: function() {
			chungQuyen.toggleItemInfo();
			chungQuyen.toggleTab();
			chungQuyen.toggleTabTable();
			phaiSinh.alertText = 'Quý khách vui lòng đăng nhập để xem chi tiết mã chứng quyền';
		},

		// Ẩn hiện thông tin chi tiết của mã trong màn hình bảng giá Phái sinh.
		toggleItemInfo: function() {
			var $info = $( '.chung-quyen .item-info' );
			$body.on( 'click', '.chung-quyen .item', function() {
				if ( chuaDangNhap( phaiSinh.alertText ) ) {
					return;
				}
				var $next = $( this ).next();
				$info.not( $next ).removeClass( 'is-active' );
				$next.toggleClass( 'is-active');
			} );
		},

		toggleTab: function() {
			$( '.chung-quyen .item-info__tabs' ).on( 'click', 'a', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.item-info' ).find( '.item-info__tab' );
				$tabs.removeClass( 'is-active' );

				var href = $this.attr( 'href' );

				if ( -1 !== href.indexOf( '#' ) ) {
					e.preventDefault();
					$tabs.filter( '[data-tab="' + href + '"]').addClass( 'is-active' );
				}
			} );
		},

		toggleTabTable: function() {
			$( '.chung-quyen .item-info__sidebar__nav' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );
				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.item-info' ).find( '.item-info__sidebar__tab' );
				$tabs.removeClass( 'is-active' );
				$tabs.filter( '[data-tab="' + $this.attr( 'href' ) + '"]').addClass( 'is-active' );
			} );
		}
	};

	var itemDetails = {
		init: function() {
			itemDetails.toggleMenu();
			itemDetails.selectMenuItem();
			itemDetails.toggleGiaoDichTabs();
			itemDetails.toggleSuKienTabs();
			itemDetails.initGiaoDichDatePicker();
			itemDetails.toggleSideTables();
			itemDetails.switchFinanceTabs();
			itemDetails.switchFinancePeriod();
			itemDetails.toggleDinhGiaOptions();
			itemDetails.toggleTinHieuTable();
			itemDetails.toggleKhuyenNghiTable();
			itemDetails.toggleBackTestOptions();
		},

		toggleMenu: function() {
			$( '.code-details__close' ).on( 'click', function() {
				$( this )
				.toggleClass( 'is-toggle' )
				.closest( '.code-details' )
				.toggleClass( 'is-sm' );
			} );
		},

		selectMenuItem: function() {
			var $menu = $( '.code-details__menu ul' ),
				$sections = $( '.code-details__section' );
			$menu.on( 'click', 'a', function( e ) {
				var $this = $( this );

				$menu.find( 'a' ).removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				$sections.removeClass( 'is-active' );

				var href = $this.attr( 'href' );

				if ( -1 !== href.indexOf( '#' ) ) {
					e.preventDefault();
					$sections.filter( '[data-tab="' + href + '"]').addClass( 'is-active' );
				}

				$( '.code-details__content' ).attr( 'data-tab', href );
			} );
		},

		initGiaoDichDatePicker: function() {
			$( '.gd__history__filter .date-picker' ).datepicker( {
				dateFormat: 'dd-mm-yy',
			} );
		},

		toggleGiaoDichTabs: function() {
			$( '.gd__tabs' ).on( 'click', 'a', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.code-details__section' ).find( '.gd__tab' );
				$tabs.removeClass( 'is-active' );

				var href = $this.attr( 'href' );

				if ( -1 !== href.indexOf( '#' ) ) {
					e.preventDefault();
					$tabs.filter( '[data-tab="' + href + '"]').addClass( 'is-active' );
				}
			} );
		},

		toggleSuKienTabs: function() {
			$( '.sukien__tabs' ).on( 'click', 'a', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.code-details__section' ).find( '.sukien__tab' );
				$tabs.removeClass( 'is-active' );

				var href = $this.attr( 'href' );

				if ( -1 !== href.indexOf( '#' ) ) {
					e.preventDefault();
					$tabs.filter( '[data-tab="' + href + '"]').addClass( 'is-active' );
				}
			} );
		},

		toggleDinhGiaOptions: function() {
			$( '.dg__options .js-my-select' ).on( 'change', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.code-details__section' ).find( '.dg__option ' );
				$tabs.removeClass( 'is-active' );
				var val = $this.val();

				$tabs.filter( '[data-tab="' + val + '"]').addClass( 'is-active' );

			} );
		},

		toggleTinHieuTable: function() {
			var $table_date = $( '.is-active > .table-date' ).html();
			var $header_date = $( '.header-date' ).html( $table_date );

			var $table_tenma_full = $( '.is-active > .table-tenma-full' ).html();
			var $header_tenma_full = $( '.header-tenma-full' ).html( $table_tenma_full );

			var $table_lth = $( '.is-active > .table-loai-th' ).html();
			var $header_lth = $( '.header-loai-th' ).html( $table_lth );
		},

		toggleKhuyenNghiTable: function() {
			var $table_date = $( '.is-active > .table-date-kn' ).html();
			var $header_date = $( '.header-date-kn' ).html( $table_date );

			var $table_tenma_full = $( '.is-active > .table-noi-dung' ).html();
			var $header_tenma_full = $( '.header-noi-dung' ).html( $table_tenma_full );

			var $table_link = $( '.is-active > .table-link' ).html();
			var $header_link = $( '.header-link' ).html( $table_link );
		},

		toggleBackTestOptions: function() {
			$( '.backtest-options .js-my-select' ).on( 'change', function( e ) {
				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.closest( '.sub-navigation__item' ).find( '.bk__option ' );
				$tabs.removeClass( 'is-active' );
				var val = $this.val();

				$tabs.filter( '[data-tab="' + val + '"]').addClass( 'is-active' );

			} );
		},

		toggleSideTables: function() {
			$( '.code-details__tables__nav' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );
				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var $tabs = $this.parent().siblings();
				$tabs.removeClass( 'is-active' );
				$tabs.filter( '[data-tab="' + $this.attr( 'href' ) + '"]').addClass( 'is-active' );
			} );
		},

		switchFinanceTabs: function() {
			var $sections = $( '.tc__section' );
			$( '.tc__dropdown' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );

				$( '.tc__dropdown__text' ).text( $this.text() );

				var href = $this.attr( 'href' ),
					$section = $sections.filter( '[data-tab="' + href + '"]' ),
					headerOffset = 90;

				if ( window.innerWidth < 1280 ) {
					headerOffset = 60;
				}

				$( [document.documentElement, document.body] ).animate( {
			        scrollTop: $section.offset().top - Math.floor( $fixedComponents.outerHeight() ) - headerOffset
			    }, 500 );
			} );
		},

		switchFinancePeriod: function() {
			$( '.tc__period' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );

				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var href = $this.attr( 'href' ),
					others = '#quarter' === href ? '#year' : '#quarter';
				$( '[data-tab="' + others + '"]' ).removeClass( 'is-active' );
				$( '[data-tab="' + href + '"]' ).addClass( 'is-active' );
			} );
		}
	};

	var market = {
		$sections: $( '.market__section' ),
		$descriptions: $( '.market__description__item' ),
		init: function() {
			market.toggleIndex();
			market.toggleFilter();
			market.toggleSubMenu();
		},

		toggleSubMenu: function() {
			var $subMenu = $( '.sub-navigation__item' );
			$( '.market__main-navigation' ).on( 'click', '.nav-tab', function( e ) {
				e.preventDefault();
				var $this = $( this );
				var href = $this.attr( 'href' );

				if ( href === '#tin-hieu' ) {
					market.$sections.removeClass( 'is-active' );
					market.$descriptions.removeClass( 'is-active' );
				}

				$subMenu.removeClass( 'is-active' );
				var $activeSubMenu = $subMenu.filter( '[data-tab="' + href + '"]' ).addClass( 'is-active' );
				$activeSubMenu.find( 'li:first-child a' ).addClass( 'active' ).trigger( 'click' );
			} );
		},

		toggleIndex: function() {
			$( '.market__sub-navigation' ).on( 'click', '.market__sub__link', function( e ) {
				e.preventDefault();

				var $this = $( this );

				$( '.index__select__text' ).text( $this.text() );

				var href = $this.attr( 'href' );

				market.$sections.removeClass( 'is-active' );
				market.$descriptions.removeClass( 'is-active' );

				market.$sections.filter( '[data-tab="' + href + '"]' ).addClass( 'is-active' );
				market.$descriptions.filter( '[data-tab="' + href + '"]' ).addClass( 'is-active' );
			} );
		},
		toggleFilter: function() {
			$( '.market__filter' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );

				$this.closest( '.market__select' ).find( '.market__select__text' ).text( $this.text() );
			} );
		}
	}

	var accountManagement = {
		init: function() {
			accountManagement.toggleFilter();
			accountManagement.toggleTypes();
		},
		toggleFilter: function() {
			$( '.qltk__select' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );

				$this.closest( '.qltk__select' ).find( '.qltk__select__text' ).text( $this.text() );
			} );
		},
		toggleTypes: function() {
			var $sections = $( '.qltk__section' );
			$( '.qltk__types' ).on( 'click', 'a', function( e ) {
				e.preventDefault();

				var $this = $( this );
				$this.siblings().removeClass( 'is-active' );
				$this.addClass( 'is-active' );

				var href = $this.attr( 'href' );
				$sections.removeClass( 'is-active' );
				$sections.filter('[data-tab="' + href + '"]').addClass( 'is-active' );
			} );
		},
	}

	function sortTables( table ) {
		var $body = $( table ).find( '.table__body' );
		$body.tablesorter( {
			cssChildRow: "tablesorter-childRow"
		} );
		$( table ).on( 'click', '.sortable', function() {
			var index = parseInt( $( this ).attr( 'data-index' ) );
			$body.trigger( 'sorton', [ [ [index, "n"] ] ] );
		} );
	}
	[ '#danh-muc', '#danh-muc-co-ban', '#bang-gia', '#bang-gia-co-ban', '#khuyen-nghi', '#chung-quyen' ].forEach( function( table ) {
		sortTables( table );
	} )

	var danhMucMoi = {
		$wrapper: $( '.danh-muc-moi-wrapper' ),
		init: function() {
			danhMucMoi.addAvailableDanhMuc();
			danhMucMoi.hideOnClickOutside();
			$body.on( 'click', '.init-danh-muc-moi', function() {
				danhMucMoi.$wrapper.removeClass( 'is-hidden' );
			} );
			$body.on( 'click', '.add-danh-muc', function() {
				danhMucMoi.$wrapper.addClass( 'is-hidden' );
			} );
		},
		addAvailableDanhMuc: function() {
			var $list = $( '.danh-muc__da-co' ).empty();
			var $availableDanhMuc = $( '.dropdown-menu--danh-muc' ).find( '.list__item' );
			var items = [];
			$availableDanhMuc.each( function() {
				var name = $( this ).text();
				items.push( '\
					<div class="danh-muc__item">'
						+ name + '\
						<span class="add-danh-muc"><i class="fas fa-plus"></i></span>\
					</div>\
				' );
			} );
			$list.append( items );
		},
		hideOnClickOutside: function() {
			$( document ).on( 'click', function( e ) {
				if ( ! danhMucMoi.$wrapper.is( e.target ) && ! $( '.init-danh-muc-moi' ).is( e.target ) && danhMucMoi.$wrapper.has( e.target ).length === 0 ) {
					danhMucMoi.$wrapper.addClass( 'is-hidden' );
				}
			} )
		}
	}

	var datLenhPopup = {
		loaiGia            : ['ATC', 'ATO', 'MP', 'MOK', 'MAK', 'MTL', 'PLO'],
		lenhDatas          : [],
		$wrapper           : null,
		$muaBanSelect      : null,
		$datLenhBanDropdown: null,
		$submitButton      : null,
		$inputCode         : null,
		$infoKiQuy         : null,
		//$infoCode          : null,
		//$codeWrapper       : null,
		$tkInfo            : null,
		$codeName          : null,
		$codeCompany       : null,
		$inputGia          : null,
		$inputKL           : null,
		$selectTK          : null,
		init: function() {
			datLenhPopup.togglePopup();
			datLenhPopup.toggleEditPopup();
			datLenhPopup.handleMuaBanSelect();
			datLenhPopup.handleSelectTK();
			datLenhPopup.handleSelectMaCK();
			datLenhPopup.handleEnterGia();
			datLenhPopup.submit();
			datLenhPopup.enterAsTab();
			datLenhPopup.toggleAllCheckBox();
			datLenhPopup.handleFooterPopupSelect();
			datLenhPopup.handleLenhSelect();
			datLenhPopup.editLenh();
			datLenhPopup.copyLenh();
			datLenhPopup.huyDatLenh();
			datLenhPopup.initXacNhanOTP();
			datLenhPopup.addClassToSoLenhTableRow();
			datLenhPopup.handleDanhMucMuaBan();
			datLenhPopup.setSavedInput();
			datLenhPopup.shortcutToDatLenh();
			datLenhPopup.handleScroll();
			datLenhPopup.arrowDownToSelectTk();
		},
		initElements: function() {
			datLenhPopup.$wrapper            = $( '.dat-lenh-popup' );
			datLenhPopup.$muaBanSelect       = $( '.form__select-mua-ban' );
			datLenhPopup.$datLenhBanDropdown = $( '.dat-lenh__ban' );
			datLenhPopup.$datLenhTabindex    = $( '.dat-lenh__form__tabindex' );
			datLenhPopup.$submitButton       = $( '.dat-lenh-popup__btn--submit' );
			datLenhPopup.$inputCode          = $( '.form__input--code input[type="text"]' );
			datLenhPopup.$infoKiQuy          = $( '.info--ki-quy' );
			datLenhPopup.$inputGia           = $( '.form__input--gia input[type="text"]' );
			datLenhPopup.$inputKL            = $( '.form__input--KL input[type="text"]' );
			datLenhPopup.$selectTK           = $( '.form__select-tai-khoan');
			datLenhPopup.$soLenhPopup        = $( '.so-lenh-popup');
			datLenhPopup.$danhMucPopup       = $( '.danh-muc-popup');
			//datLenhPopup.$infoCode           = $( '.dat-lenh__info--code' );
			datLenhPopup.$tkInfo             = $( '.dat-lenh-popup__tk-info' );
			//datLenhPopup.$codeWrapper        = $( '.dat-lenh-popup__code' );
			//datLenhPopup.$codeName           = $( '.dat-lenh-popup__code__name' );
			//datLenhPopup.$codeCompany        = $( '.dat-lenh-popup__code__company' );
            datLenhPopup.addDisabledAttr();

		},
		togglePopup: function() {
			$( '.init-dat-lenh-popup, .form__close' ).on( 'click', function( e ) {
				e.preventDefault();
				$body.toggleClass( 'enable-dat-lenh-popup' );
				datLenhPopup.refreshDatLenhPopup();
			} );
			datLenhPopup.initElements();
		},

		toggleEditPopup: function() {
			$( '.popup-edit__close' ).on( 'click', function( e ) {
				e.preventDefault();
				$body.toggleClass( 'enable-dat-lenh-edit' );
			} );
			datLenhPopup.initElements();
		},

		arrowDownToSelectTk: function() {
			$( '.form__select-tai-khoan .dropdown-toggle' ).on( 'focus', function( e ) {
				e.preventDefault();
				var $this = $( this );
				$(window).on( "keyup", function(e) {
			        if ( e.keyCode == 40 ) {
			        	$this.next().show().on('focus', 'li', function() {
						    $this = $(this);
						    $this.addClass('active').siblings().removeClass();
						    $this.closest('div.container').scrollTop($this.index() * $this.outerHeight());
						}).on('keydown', 'li', function(e) {
						    var $li = $( this );
						    if (e.keyCode == 40) {
						        $li.next().focus();
						        return false;
						    } else if (e.keyCode == 38) {
						        $li.prev().focus();
						        return false;
						    } else if (e.keyCode == 13) {
						        $li.find('a').trigger('click');
						        $('.dropdown-menu--tai-khoan' ).hide();
						        $('.form__input--code input[type="text"]').focus();
						        return false;
						    }
						}).find('li').first().focus();

						$( '.form__select-tai-khoan .dropdown' ).hover( function() {
							$( '.dropdown-menu--tai-khoan' ).show();
						}, function() {
							$( '.dropdown-menu--tai-khoan' ).hide();
						} )
			        }
				});
			});
		},

		// Kiểm tra xem input nào có checkbox được check
		// thì thêm class is-saved để khi đặt lệnh vẫn lưu giá trị
		setSavedInput: function() {
			datLenhPopup.$wrapper.on( 'change', '.form__body input[type="checkbox"]', function() {
				var $this  = $( this );
				var $input = $this.closest( 'label' ).prev( 'input' );
				$input.toggleClass( 'is-saved', $this.prop( 'checked' ) );
			} );
		},
		// Set dữ liệu dropdown.
		setDropdownData: function( $dropdown, value ) {
			$dropdown.find( '.dropdown-toggle span' ).text( value );
		},
		// Lấy dữ liệu dropdown.
		getDropdownData: function( $dropdown ) {
			return $dropdown.find( '.dropdown-toggle span' ).text();
		},
		handleSelectTK: function() {
			$( '.form__select-tai-khoan' ).on( 'click', '.dropdown-menu--tai-khoan a', function( e ) {
				e.preventDefault();
				var $this  = $( this );
				var dataTK = $this.attr( 'data-tk' );

				// Ẩn hiện box thông tin tk ở bên trái.
				datLenhPopup.$tkInfo.toggleClass( 'is-visible', dataTK ? true : false );

				// Thay đổi giá trị của select khi lựa chọn tk.
				$this.closest( '.form__select-tai-khoan' ).find( '.dropdown-toggle span' ).text( $this.text() );

				// Ẩn hiện sức mua tài khoản.
				$( '.info--tk' ).toggleClass( 'is-hidden', dataTK ? false : true );

				// Nếu đang bán, chọn tài khoản thì hiện tỉ lệ kí quỹ
				if( datLenhPopup.$wrapper.hasClass( 'is-selling' ) ) {
					datLenhPopup.$infoKiQuy.toggleClass( 'is-hidden', dataTK ? false : true );
				}
			} )
		},
		handleSelectMaCK: function() {
			datLenhPopup.selectCode();
			datLenhPopup.manualSelectCode();
		},
		handleEnterGia: function() {
			var $table    = $( '.visible-on-price' );
			datLenhPopup.$inputGia.autocomplete( {
				source: datLenhPopup.loaiGia
			} ).on( 'change', function( e ) {
				var value = $( this ).val();
				$table.toggleClass( 'is-hidden', value ? false : true );
			} );
		},
		handleMuaBanSelect: function() {
			$( '.form__select-mua-ban' ).on( 'click', '.dropdown-menu a, .dropdown-toggle', function( e ) {
				e.preventDefault();
				var value         = $( this ).text();
				var isSelling     = null;
				var $dropDownMenu = $( this ).closest( '.has-submenu' );
				isSelling         = $dropDownMenu.hasClass( 'dat-lenh__ban' ) ? true : false;
				if ( isSelling ) {
					datLenhPopup.$wrapper.removeClass().addClass( 'is-selling dat-lenh-popup' );
				} else {
					datLenhPopup.$wrapper.removeClass().addClass( 'is-buying dat-lenh-popup' );
				}
				datLenhPopup.toggleMuaBanSelectText( value, isSelling );
				datLenhPopup.toggleSubmitButton( value, isSelling );
			} )
		},
		toggleSubmitButton: function( value, isSelling ) {
			datLenhPopup.$submitButton.text( value );
			datLenhPopup.$submitButton.toggleClass( 'btn--red-2', isSelling );
		},
		toggleMuaBanSelectText: function( value, isSelling ) {
			// datLenhPopup.$muaBanSelect.closest( '.dat-lenh-popup__actions' ).toggleClass( 'is-selling', isSelling );
			// datLenhPopup.$muaBanSelect.toggleClass( 'is-selling', isSelling );
			var $mua = $( '.dat-lenh__mua .dropdown-toggle span' );
			var $ban = $( '.dat-lenh__ban .dropdown-toggle span' );
			if ( isSelling ) {
				$ban.text( value );
				$mua.text( 'Mua' );
			} else {
				$mua.text( value );
				$ban.text( 'Bán' );
			}
		},
		selectCode: function() {
			datLenhPopup.$inputCode.autocomplete( {
				source: addCode.codes,
				select: function( event, ui ) {
					event.preventDefault();
					//datLenhPopup.insertDataFromCode( ui.item )
					//datLenhPopup.$infoCode.removeClass( 'is-hidden' );
					$( this ).val( ui.item.value.toUpperCase() );
					if( datLenhPopup.$wrapper.hasClass( 'is-buying' ) ) {
						datLenhPopup.$infoKiQuy.removeClass( 'is-hidden' );
					}
					if( datLenhPopup.$wrapper.hasClass( 'is-selling' ) ) {
						$( '.visible-on-price' ).removeClass( 'is-hidden' );
					}
				}
			} ).on( 'input', function( e ) {
				var value = e.target.value;
				if ( value === '' ) {
					//datLenhPopup.$infoCode.addClass( 'is-hidden' );
					//datLenhPopup.$codeWrapper.removeClass( 'is-visible' );

					// Nếu rỗng thì ẩn kí quỹ đi.
					if( datLenhPopup.$wrapper.hasClass( 'is-buying' ) ) {
						datLenhPopup.$infoKiQuy.addClass( 'is-hidden' );
					}
					if( datLenhPopup.$wrapper.hasClass( 'is-selling' ) ) {
						$( '.visible-on-price' ).addClass( 'is-hidden' );
					}
					return;
				}
				$( this ).val( value.toUpperCase() );
			} );
		},
		// Khi nhấn Enter hoặc nhấn tab.
		manualSelectCode: function() {
			datLenhPopup.$inputCode.on( 'keydown', function( e ) {
				if ( e.which != 13 && e.which != 9 ) {
					return;
				}
				var $this = $( this );
				var code = $this.val().toLowerCase();

				var filtered = addCode.codes.filter( function( item ) {
					return item.value.toLowerCase().indexOf( code ) === 0;
				} );

			/*	if ( filtered.length ) {
					datLenhPopup.insertDataFromCode( filtered[0] );
				}*/

				$this.val( code.toUpperCase() );
				$this.autocomplete( 'close' );
				if( datLenhPopup.$wrapper.hasClass( 'is-buying' ) ) {
					datLenhPopup.$infoKiQuy.removeClass( 'is-hidden' );
				}
				if( datLenhPopup.$wrapper.hasClass( 'is-selling' ) ) {
					$( '.visible-on-price' ).removeClass( 'is-hidden' );
				}
			} );
		},
		getLenhData: function() {
			var data = {
				state: datLenhPopup.$wrapper.hasClass( 'is-selling' ) ? 'selling' : 'buying',
				tk   : datLenhPopup.getDropdownData( datLenhPopup.$selectTK ),
				lenh : datLenhPopup.$wrapper.hasClass( 'is-selling' ) ? 'Bán' : 'Mua',
				code : datLenhPopup.$inputCode.val(),
				kl   : $( '.form__input--KL input' ).val(),
				gia  : $( '.form__input--gia input' ).val(),
			}
			return data;
		},
		insertDataToSoLenh: function( data ) {
			if ( ! data.kl || ! data.code || ! data.gia || ! data.tk ) {
				return false;
			}
			datLenhPopup.setDropdownData( $( '.so-lenh__tk' ), data.tk );
			var loaiLenh = $( '.so-lenh__lenh' ).attr( 'data-value' );
			var tr = datLenhPopup.getRowFormat( data, loaiLenh );
			$( '.so-lenh__table[data-lenh="' + loaiLenh + '"]' ).find( 'tbody' ).append( tr );
			return true;
		},
		getRowFormat: function( data, loaiLenh ) {
			var tr = '';
			if ( loaiLenh === 'lenh-trong-ngay' ) {
				// Thêm span bao lấy phần code rồi cho float right để khi dài ra chữ sẽ chạy sang phải.
				tr = '\
					<tr>\
						<td><input class="so-lenh__table__checkbox" type="checkbox" name="" value=""></td>\
						<td>' + data.lenh + '</td>\
						<td class="so-lenh__table__code span"><span>' + data.code + '<span></td>\
						<td class="txt-right so-lenh__table__KL">' + data.kl + '</td>\
						<td class="txt-right">90.000</td>\
						<td class="txt-right so-lenh__table__gia">' + data.gia + '</td>\
						<td>KMP</td>\
						<td class="so-lenh__table__edit" data-state="' + data.state + '"><i class="fas fa-pencil-alt"></i></td>\
						<td class="so-lenh__table__copy" data-state="' + data.state + '"><i class="far fa-copy"></i></td>\
					</tr>\
				';
			} else if ( loaiLenh === 'lenh-phien-ke-tiep' ) {
				tr = '\
					<tr>\
						<td><input class="so-lenh__table__checkbox" type="checkbox" name="" value=""></td>\
						<td>' + data.lenh + '</td>\
						<td class="so-lenh__table__code span"><span>' + data.code + '<span></td>\
						<td class="txt-right so-lenh__table__KL">' + data.kl + '</td>\
						<td class="txt-right so-lenh__table__gia">' + data.gia + '</td>\
						<td>KMP</td>\
					</tr>\
				';
			} else if ( loaiLenh === 'gio-lenh' ) {
				tr = '\
					<tr>\
						<td><input class="so-lenh__table__checkbox" type="checkbox" name="" value=""></td>\
						<td>' + data.lenh + '</td>\
						<td class="so-lenh__table__code span"><span>' + data.code + '<span></td>\
						<td class="txt-right so-lenh__table__KL">' + data.kl + '</td>\
						<td class="txt-right so-lenh__table__gia">' + data.gia + '</td>\
						<td></td>\
					</tr>\
				';
			}
			return tr;
		},
		// Nhấn enter để chuyển qua lại giữa các input
        enterAsTab: function() {
        	var $inputs = datLenhPopup.$datLenhTabindex;
            $inputs.on( "keypress", function(e) {
            	var key = undefined;
				// Check trên các trình duyệt và hệ điều hành khác nhau.
				var possible = [ e.key, e.keyIdentifier, e.keyCode, e.which ];
				// Loop qua tất cả khả năng xem có event nào chạy không.
				while ( key === undefined && possible.length > 0 )
				{
					key = possible.pop();
				}
                /* ENTER PRESSED*/
                if ( key == 13 ) {
                    /* FOCUS ELEMENT */
                    var index = $inputs.index( this );
                    if ( index == $inputs.length - 1 ) {
                       	return;
                    } else {
                        $inputs.eq(index + 1).focus();
                    	$inputs.eq(index + 1).select();
                    }
                }
              });
        },
		submit: function() {
			datLenhPopup.$submitButton.on( 'click', function() {
				datLenhPopup.addDatLenhNotice();
				datLenhPopup.resetDatLenh();
			} );
		},
		activeFooterNavigation: function() {
			// Check nếu sổ lệnh popup đã hiện ( không có class is hidden )
			// thì không click nữa.
			if ( ! datLenhPopup.$soLenhPopup.hasClass( 'is-hidden' ) ) {
				return;
			}
			// Làm click để tránh trường hợp các bảng danh mục hoặc tài sản đang bật thì sẽ bị ẩn đi
			$( '.footer-navigation__link[href="#so-lenh-popup"]' ).trigger( 'click' );
		},
		addDatLenhNotice: function() {
			var random         = Math.floor( Math.random() * 2 );
			var $datLenhNotice = $( '.dat-lenh-notice' );
			var text           = '';
			if ( random ) {
				// Nếu có thể đặt thành công.
				var data = datLenhPopup.getLenhData();
				var hasData = datLenhPopup.insertDataToSoLenh( data ); // Check xem có data truyền vào input không.
				if ( hasData ) {
					$datLenhNotice.removeClass( 'failure' ).addClass( 'success' );
					datLenhPopup.activeFooterNavigation();
					text = 'Đặt lệnh thành công';
				} else {
					text = 'Chưa điền đầy đủ thông tin';
				}
			} else {
				$datLenhNotice.removeClass( 'success' ).addClass( 'failure' );
				text = 'Tài khoản không đủ số dư. Mã lỗi: ABCXYZ';
			}
			$datLenhNotice.text( text );
		},
		toggleAllCheckBox: function() {
			var $tableBody = $( '.so-lenh__table tbody' );
			$( '.dropdown__checkall' ).on( 'change', function() {
				$tableBody.find( 'tr' ).addClass( 'is-checked' );
				$tableBody.find( 'input[type="checkbox"]' ).prop( 'checked', $( this ).prop( 'checked' ) );
			} );
		},
		handleFooterPopupSelect: function() {
			$( '.footer-nav-popup__select' ).on( 'click', '.dropdown-menu a', function( e ) {
				e.preventDefault();
				var $this  = $( this );
				var $dropdown = $this.closest( '.footer-nav-popup__select' );
				$dropdown.attr( 'data-value', $this.attr( 'data-value' ) );
				datLenhPopup.setDropdownData( $dropdown, $this.text() );
			} )
		},
		handleLenhSelect: function() {
			$( '.so-lenh__lenh' ).on( 'click', '.dropdown-menu a', function( e ) {
				e.preventDefault();
				var $this  = $( this );
				var value = $this.attr( 'data-value' );
				datLenhPopup.$soLenhPopup.removeClass().addClass( 'footer-nav-popup so-lenh-popup ' + value );
				datLenhPopup.handleScroll()
			} )
		},
		addClassToSoLenhTableRow: function() {
			datLenhPopup.$soLenhPopup.on( 'click', '.so-lenh__table__checkbox', function() {
				var $row = $( this ).closest( 'tr' );
				$row.toggleClass( 'is-checked' );
			} )
		},
		xacNhanOTP: function( action ) {
			// Thêm số lượng item đã check vào xác nhận otp popup.
			var checkedItemNumber = $( '.so-lenh__table .is-checked' ).length;
			$( '.xac-nhan-otp__ds-lenh span' ).text( checkedItemNumber );
			$( '.xac-nhan-otp' ).unbind('submit').on( 'submit', function( e ) {
				e.preventDefault();
				console.log( 'dasdas' );
				if ( 'huy' === action ) {
					datLenhPopup.$soLenhPopup.find( 'input[type="checkbox"]' ).prop( 'checked', false );
					datLenhPopup.$soLenhPopup.find( 'tr.is-checked' ).remove();
				} else {

				}

				$body.removeClass( 'modal-xac-nhan-otp-enable' );
			} );
		},
		initXacNhanOTP: function() {
			var $huyBtn = $( '.so-lenh__huy' );
			var $kichHoatBtn = $( '.so-lenh__kich-hoat' );

			$huyBtn.on( 'click', function() {
				datLenhPopup.xacNhanOTP( 'huy' );
			} )

			$kichHoatBtn.on( 'click', function() {
				datLenhPopup.xacNhanOTP( 'kich-hoat' );
			} );
		},
		/*huySoLenh: function() {
			var $huyBtn = $( '.so-lenh__huy' );
			$huyBtn.on( 'click', function() {
				if ( ! datLenhPopup.xacNhanOTP() ) {
					return;
				}
				datLenhPopup.$soLenhPopup.find( 'input[type="checkbox"]' ).prop( 'checked', false );
				datLenhPopup.$soLenhPopup.find( 'tr.is-checked' ).remove();
			} )
		},
		kichHoatSoLenh: function() {
			$( '.so-lenh__kich-hoat' ).on( 'click', function() {
				// Thêm số lượng item đã check vào xác nhận otp popup.
				var checkedItemNumber = $( '.so-lenh__table .is-checked' ).length;
				$( '.xac-nhan-otp__ds-lenh span' ).text( checkedItemNumber );
			} );
		},*/
		setTkFromPopupToDatLenh: function( tk ) {
			datLenhPopup.setDropdownData( datLenhPopup.$selectTK, tk );
			$( '.info--tk' ).toggleClass( 'is-hidden', tk ? false : true );
		},
		copyLenh: function() {
			datLenhPopup.$soLenhPopup.on( 'click', '.so-lenh__table__copy', function() {
				var $this     = $( this );
				var $row      = $this.closest( 'tr' );
				var state     = $this.attr( 'data-state' );
				var value     = state === 'selling' ? 'Bán' : 'Mua';
				var isSelling = state === 'selling' ? true : false;
				var tk        = datLenhPopup.getDropdownData( datLenhPopup.$soLenhPopup.find( '.so-lenh__tk' ) );

				// Nếu từ edit sang copy thì remove disabled tài khoản và code
				datLenhPopup.$selectTK.removeClass( 'is-disabled' );
				datLenhPopup.$inputCode.removeClass( 'is-disabled' );

				datLenhPopup.$wrapper.removeClass().addClass( 'is-' + state + ' dat-lenh-popup' );
				datLenhPopup.setDataFromLenh( $row );

				// Khi copy thì phải thay đổi cả select mua bán và nút submit.
				datLenhPopup.toggleMuaBanSelectText( value, isSelling );
				datLenhPopup.toggleSubmitButton( value, isSelling );

				// Disable tài khoản
				//datLenhPopup.$selectTK.addClass( 'is-disabled' );

				// Truyền số tk lại phần đặt lệnh.
				datLenhPopup.setTkFromPopupToDatLenh( tk )

				$body.addClass( 'enable-dat-lenh-popup' );
			} );
		},
		editLenh: function() {
			datLenhPopup.$soLenhPopup.on( 'click', '.so-lenh__table__edit', function() {
				var $this = $( this );
				var $row  = $this.closest( 'tr' );

				var $soLenhGia = $row.find( '.so-lenh__table__gia' );
				var $soLenhKL  = $row.find( '.so-lenh__table__KL' );
				var tk         = datLenhPopup.getDropdownData( datLenhPopup.$soLenhPopup.find( '.so-lenh__tk' ) );

				$( '.dat-lenh-popup__edit' ).removeClass().addClass( 'is-editing dat-lenh-popup__edit is-' + $this.attr( 'data-state' ) );
				/*datLenhPopup.$inputGia.on( 'change', function() {
					$soLenhGia.text( $( this ).val() );
				} )
				datLenhPopup.$inputKL.on( 'change', function() {
					$soLenhKL.text( $( this ).val() );
				} )*/

				datLenhPopup.setDataFromLenh( $row );

				// Disable mã và tài khoản
				datLenhPopup.$selectTK.addClass( 'is-disabled' );
				datLenhPopup.$inputCode.addClass( 'is-disabled' );

				// Truyền số tk lại phần đặt lệnh.
				datLenhPopup.setTkFromPopupToDatLenh( tk )

				$body.addClass( 'enable-dat-lenh-edit' );

				// Truyền giá trị KL và Giá khi ấn xác nhận vào bảng sổ lệnh.
				var dataEdit = $this.attr( 'data-edit' );
				$( '.popup-edit__btn--submit' ).attr( 'data-edit', dataEdit );
				datLenhPopup.luuEditLenh( dataEdit );
			} )
		},
		// Khi nhấn edit hoặc copy lệnh trong sổ lệnh
		// Lấy data từ lệnh đã đặt và đưa vào ô input trong phần đặt lệnh
		setDataFromLenh: function( $row ) {
			var $soLenhCode = $row.find( '.so-lenh__table__code span' );
			var $soLenhGia = $row.find( '.so-lenh__table__gia' );
			var $soLenhKL = $row.find( '.so-lenh__table__KL' );

			var code  = $soLenhCode.text();
			var gia   = $soLenhGia.text();
			var kl    = $soLenhKL.text();

			// Điền giá trị của lệnh vào input trong ô đặt lệnh
			datLenhPopup.$inputCode.val( code );
			datLenhPopup.$inputGia.val( gia ).focus();
			datLenhPopup.$inputKL.val( kl );
		},
		luuEditLenh: function( $dataEdit ) {
			$( '.popup-edit__btn--submit' ).on( 'click', function() {
				var $this = $( this ),
					$row  = $this.closest( '.popup-edit__form' ),
					$gia  = $row.find( '.form__input--gia input' ).val(),
					$kl   = $row.find( '.form__input--KL input' ).val(),
					$lenh = $( '[data-edit="' + $dataEdit + '"]' ).closest( 'tr' );

				$lenh.find( '.so-lenh__table__gia' ).text( $gia );
				$lenh.find( '.so-lenh__table__KL' ).text( $kl );
			} );
		},
		huyDatLenh: function() {
			$( '.gio-lenh__huy' ).on( 'click', function() {
				datLenhPopup.resetDatLenh();
			} );
		},
		resetDatLenh: function() {
			console.log("datLenhPopup.$wrapper.find( 'input' )", datLenhPopup.$wrapper.find( 'input' ));
			datLenhPopup.$wrapper.find( 'input' ).not( '.is-saved' ).val( '' );
			if ( ! datLenhPopup.$inputCode.hasClass( 'is-saved' ) ) {
				datLenhPopup.$infoKiQuy.addClass( 'is-hidden' );
			}
			if ( ! datLenhPopup.$inputGia.hasClass( 'is-saved' ) ) {
				$( '.visible-on-price' ).addClass( 'is-hidden' );
			}
			$( '.info--tk' ).addClass( 'is-hidden' );
			datLenhPopup.setDropdownData( $( '.form__select-tai-khoan' ), 'Tài khoản' );
		},
		toggleSoLenh: function() {
			if ( ! datLenhPopup.$soLenhPopup.hasClass( 'is-hidden' ) ) {
				return;
			}
			datLenhPopup.$soLenhPopup.removeClass( 'is-hidden' );
		},
		handleDanhMucMuaBan: function() {
			datLenhPopup.$danhMucPopup.on( 'click', '.danh-muc__table__action', function( e ) {
				e.preventDefault();
				var $this  = $( this );
				var action = $this.attr( 'data-action' );
				var $row   = $this.closest( 'tr' );
				var code   = $row.find( '.danh-muc__table__code' ).text();

				var tk     = datLenhPopup.$danhMucPopup.find( '.dropdown-toggle span' ).text();

				var value     = action === 'selling' ? 'Bán' : 'Mua';
				var isSelling = action === 'selling' ? true : false;

				datLenhPopup.$inputCode.val( code );
				datLenhPopup.$selectTK.find( '.dropdown-toggle span' ).text( tk );

				$body.addClass( 'enable-dat-lenh-popup' );
				datLenhPopup.$wrapper.removeClass().addClass( 'dat-lenh-popup is-' + action );

				// Khi copy thì phải thay đổi cả select mua bán và nút submit.
				datLenhPopup.toggleMuaBanSelectText( value, isSelling );
				datLenhPopup.toggleSubmitButton( value, isSelling );

				datLenhPopup.setTkFromPopupToDatLenh( tk );
				$( '.info--ki-quy' ).removeClass( 'is-hidden' );
			} );
		},
		datLenhByShortcut: function( action ) {
			var text = 'selling' === action ? 'Bán' : 'Mua';
			var isSelling = 'selling' === action ? true : false;
			datLenhPopup.$wrapper.removeClass().addClass( 'is-' + action + ' dat-lenh-popup' );

			datLenhPopup.toggleMuaBanSelectText( text, isSelling );
			datLenhPopup.toggleSubmitButton( text, isSelling );
			$body.addClass( 'enable-dat-lenh-popup' );

			jQuery('.form__select-tai-khoan .dropdown-toggle' ).trigger( 'focus' );
		},
		shortcutToDatLenh: function() {
			if( ! isLoggedIn() ) {
				return;
			}
			$( document ).keydown( function( e ) {

				var key = undefined;
				// Check trên các trình duyệt và hệ điều hành khác nhau.
				var possible = [ e.key, e.keyIdentifier, e.keyCode, e.which ];
				// Loop qua tất cả khả năng xem có event nào chạy không.
				while ( key === undefined && possible.length > 0 )
				{
					key = possible.pop();
				}

				var sellingShortcut = key
				&& ( e.ctrlKey || e.metaKey ) // ctrl key
				&& ( key == '115' || key == '83' ) // s key
				&& !( e.altKey );

				var buyingShortcut = key
				&& ( e.ctrlKey || e.metaKey ) // ctrl key
				&& ( key == '66' ) // b key
				&& !( e.altKey );

				if ( sellingShortcut ) {
					datLenhPopup.datLenhByShortcut( 'selling' );
					return false;
				} else if ( buyingShortcut ) {
					datLenhPopup.datLenhByShortcut( 'buying');
					return false;
				}
				return true;
			} );
		},
		handleScroll: function() {
			var $popUpBody   = $( '.footer-nav-popup__body' )[0];
			var classList    = $( '.footer-nav-popup' ).attr('class').split(/\s+/);
			var $activeTable = $( 'table[data-lenh="' + classList.pop() + '"');
			// Dữ liệu demo.
			var tr = '\
				<tr>\
					<td><input class="so-lenh__table__checkbox" type="checkbox" name="" value=""></td>\
					<td>Mua</td>\
					<td class="so-lenh__table__code span"><span>KDC<span></td>\
					<td class="txt-right so-lenh__table__KL">500</td>\
					<td class="txt-right">90.000</td>\
					<td class="txt-right so-lenh__table__gia">100</td>\
					<td>KMP</td>\
					<td class="so-lenh__table__edit"><i class="fas fa-pencil-alt"></i></td>\
					<td class="so-lenh__table__copy"><i class="far fa-copy"></i></td>\
				</tr>\
			';
			$( '.footer-nav-popup__body' ).on( 'scroll', function() {
				var scrollHeight = $popUpBody.scrollHeight;
				var height       = $popUpBody.offsetHeight;
				var scrollTop    = $( this ).scrollTop();

				if ( scrollTop == scrollHeight - height ) {
					// Thêm dữ liệu demo.
					$activeTable.append( tr );

					// Ở đây sẽ gọi tới API để chèn thêm lệnh vào
				}
			} )
		},
		refreshDatLenhPopup: function() {
			datLenhPopup.$wrapper.removeClass().addClass( 'dat-lenh-popup' );
			$( '.dat-lenh__mua .dropdown-toggle' ).trigger( 'click' );
			$( '.is-disabled' ).removeClass( 'is-disabled' );
			datLenhPopup.resetDatLenh();
		},
        addDisabledAttr: function() {
            $('.form__input--code input[type="text"]').each( function() {
                if ( $( this ).hasClass( 'is-disabled' ) ) {
                    $( this ).attr( 'disable', true );
                }
            } )
        }
	}
	function toggleFooterNavPopup() {
		$( '.footer-navigation__link' ).on( 'click', function( e ) {
			e.preventDefault();

			if ( chuaDangNhap( 'Quý khách vui lòng đăng nhập để xem thông tin' ) ) {
				return false;
			}

			var $this = $( this );

			$this.siblings().removeClass( 'is-active' );
			$this.toggleClass( 'is-active' );

			$( '.footer-nav-popup' ).addClass( 'is-hidden' );

			$( '.footer-nav-popup' ).filter( $this.attr( 'href' ) ).toggleClass( 'is-hidden', ! $this.hasClass( 'is-active' ) );

		} )
	}

	function setMaxHeightFooterNavPopup() {
		$( '.footer-nav-popup' ).css( 'height', window.innerHeight - Math.floor( $fixedComponents.outerHeight() ) - 75 );
	}

	function closeFooterNavPopup() {
		var $close = $( '.footer-nav-popup__close' );
		var $popup = $close.closest( '.footer-nav-popup' );
		$close.on( 'click', function() {
			$popup.addClass( 'is-hidden' );
			$( '.footer-navigation__link' ).removeClass( 'is-active' );
		} )
	}

	function closeFooterNavFooterPopup() {
		var $close = $( '.footer-nav-popup__footer-close' );
		var $footer = $close.closest( '.footer-nav-popup__footer' );
		$close.on( 'click', function() {
			$footer.toggleClass( 'is-minimized' );
		} )
	}

	// Không làm kiểu object init cho 2 element được vì sẽ bị trùng object.
	var DsLenh = function( element ) {
		this.$el             = element;
		this.totalRow        = null;
		this.hiddenFromPos   = 5;
		this.hiddenBeforePos = 1;
		this.rowNumber       = 5;
		this.pageNumber      = 1;
	};
	DsLenh.prototype.init = function() {
		this.$checkAll        = this.$el.find( '.dsl__dropdown__checkall' );
		this.$checkBoxes      = this.$el.find( '.dsl__dropdown__checkbox' );
		this.$tableBody       = this.$el.find( 'tbody' );
		this.$tableRow        = this.$tableBody.find( 'tr' );
		this.$dropdownFooter  = this.$el.next( '.dsl__dropdown__footer' );
		this.$pageNumberInput = this.$dropdownFooter.find( '.dropdown-pagination__page-number' );
		this.$rowNumberInput  = this.$dropdownFooter.find( '.dropdown-pagination__row-number' );

		this.initPagination();
		this.hideRow();
		this.goNext();
		this.goPrev();
		this.goFirst();
		this.goLast();
		this.handlePageNumberInput();
		this.handleRowNumberInput();
		this.toggleDropdownTableCheckbox();
	};
	DsLenh.prototype.initPagination = function() {
		this.totalRow = this.$tableBody.find( 'tr' ).length;
		this.setTotalPageNumber();
	};
	DsLenh.prototype.hideRow = function() {
		this.$tableRow.removeClass( 'is-hidden' )
						.lt( this.hiddenBeforePos - 1 ).addClass( 'is-hidden' );
		this.$tableRow.removeClass( 'hidden-from-now' )
						.eq( this.hiddenFromPos - 1  ).addClass( 'hidden-from-now' );
	};
	DsLenh.prototype.setTotalPageNumber = function() {
		this.totalPageNumber = Math.ceil( this.totalRow / this.rowNumber ) ;
		this.$pageNumberInput.attr( 'max', this.totalPageNumber );
	}
	DsLenh.prototype.goNext = function() {
		var that = this;
		this.$dropdownFooter.on( 'click', '.next', function() {
			if ( that.hiddenBeforePos + that.rowNumber > that.totalRow ) {
				return;
			}
			that.hiddenBeforePos += that.rowNumber;
			that.hiddenFromPos = that.hiddenBeforePos + that.rowNumber - 1;
			if ( that.hiddenFromPos > that.totalRow ) {
				that.hiddenFromPos = that.totalRow;
				that.hiddenBeforePos = that.totalRow - that.totalRow % that.rowNumber + 1;
			}
			that.handeNextPrev( 'next' );
			that.hideRow();
		} )
	};
	DsLenh.prototype.goPrev = function() {
		var that = this;
		this.$dropdownFooter.on( 'click', '.prev', function() {
			that.hiddenBeforePos -= that.rowNumber;
			that.hiddenFromPos = that.hiddenBeforePos + that.rowNumber - 1;
			if ( that.hiddenFromPos < that.rowNumber ) {
				that.hiddenFromPos = that.rowNumber;
				that.hiddenBeforePos = 1;
			}
			that.handeNextPrev( 'prev' );
			that.hideRow();
		} )
	};
	DsLenh.prototype.handeNextPrev = function() {
		var direction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
		if ( 'next' === direction ) {
			this.pageNumber++;
		} else if ( 'prev' === direction ) {
			this.pageNumber--;
		}
		this.limitPageNumber();
		this.$pageNumberInput.val( this.pageNumber );
	};
	DsLenh.prototype.limitPageNumber = function() {
		if ( this.pageNumber > this.totalPageNumber ) {
			this.pageNumber = this.totalPageNumber;
			this.$pageNumberInput.val( this.totalPageNumber );
		} else if ( this.pageNumber < 1 ) {
			this.pageNumber = 1;
			this.$pageNumberInput.val( 1 );
		}
	}
	DsLenh.prototype.goFirst = function() {
		var that = this;
		this.$dropdownFooter.on( 'click', '.first', function() {
			that.hiddenFromPos   = that.rowNumber;
			that.hiddenBeforePos = 1;
			that.pageNumber      = 1;

			that.$pageNumberInput.val( that.pageNumber );
			that.hideRow();
		} )
	};
	DsLenh.prototype.goLast = function() {
		var that = this;
		this.$dropdownFooter.on( 'click', '.last', function() {
			that.hiddenFromPos   = that.totalRow;
			if ( that.totalRow % that.rowNumber !== 0 ) {
				that.hiddenBeforePos = that.totalRow - that.totalRow % that.rowNumber + 1;
			} else {
				that.hiddenBeforePos = that.totalRow - that.rowNumber + 1;
			}
			that.pageNumber      = that.totalPageNumber;

			that.$pageNumberInput.val( that.pageNumber );
			that.hideRow();
		} )
	};
	DsLenh.prototype.handlePageNumberInput = function() {
		var that = this;
		this.$pageNumberInput.on( 'input', function( e ) {
			that.pageNumber = parseInt( e.target.value );
			that.limitPageNumber();
			that.hiddenFromPos = that.rowNumber * that.pageNumber;
			if ( that.hiddenFromPos > that.totalRow ) {
				that.hiddenFromPos = that.totalRow;
			}
			that.hiddenBeforePos = that.hiddenFromPos - that.rowNumber + 1;
			if ( ! that.pageNumber || that.rowNumber > that.totalRow ) {
				// Nếu input rỗng hoặc rowNumber > totalRow.
				that.hiddenFromPos   = that.rowNumber;
				that.hiddenBeforePos = 1;
			}
			that.hideRow();
		} )
	};
	DsLenh.prototype.handleRowNumberInput = function() {
		var that = this;
		this.$rowNumberInput.on( 'input', function( e ) {
			that.rowNumber = parseInt( e.target.value );

			// Reset page number
			that.pageNumber = 1;
			that.$pageNumberInput.val( 1 );
			that.setTotalPageNumber();
			that.hiddenFromPos = 1;

			if ( ! that.rowNumber ) {
				that.hiddenFromPos   = 5;
				that.hiddenBeforePos = 1;
			} else {
				that.hiddenFromPos = that.rowNumber;
			}

			if ( that.hiddenFromPos > that.totalRow ) {
				that.hiddenFromPos = that.totalRow;
			}

			that.hideRow();
		} )
	};
	DsLenh.prototype.toggleDropdownTableCheckbox = function() {
		var that = this;
		$( '.dsl__dropdown__reset-checkbox' ).on( 'click', function( e ) {
			e.preventDefault();
			that.$checkAll.prop( 'checked', false );
			that.$checkBoxes.prop( 'checked', false );
		} )
		this.$checkAll.on( 'change', function( e ) {
			that.$checkBoxes.prop( 'checked', $( this ).prop('checked') );
		} )
	}
	$.fn.lt = function(n) {return this.slice(0,n);};

	var toggleDropdownTable = function() {
		$body.on( 'click', '.dsl__dropdown__toggle', function( e ) {
			e.preventDefault();
			$( this ).toggleClass( 'is-visible-dropdown' );
		} )
	}
	toggleDropdownTable();

	var tuongTacSoSanh = {
		codes: null,
		$input: null,
		inserted: [ 'KDF','KDC','KDG' ],
		init: function() {
			tuongTacSoSanh.$input = $( '#so-sanh-view-code' );
			tuongTacSoSanh.codes = addCode.codes;
			tuongTacSoSanh.add();
			tuongTacSoSanh.manualAdd();
			tuongTacSoSanh.clickAdd();
			tuongTacSoSanh.showColumn( addCode.codes );
		},
		add: function() {
			tuongTacSoSanh.$input.autocomplete({
				source: addCode.codes,
				select: function( event, ui ) {
					tuongTacSoSanh.insertColumn(ui.item);
				}
			})
		},
		manualAdd: function() {
			tuongTacSoSanh.$input.on( 'keypress', function( e ) {
				if ( e.which != 13 ) {
					return;
				}

				e.preventDefault();

				var code = tuongTacSoSanh.$input.val().toLowerCase();

				var filtered = tuongTacSoSanh.codes.filter( function( item ) {
					return item.value.toLowerCase().indexOf( code ) === 0;
				} );

				if ( filtered.length ) {
					tuongTacSoSanh.insertColumn( filtered[0] );
				}

				tuongTacSoSanh.$input.val( '' );
				tuongTacSoSanh.$input.autocomplete( 'close' );
			} );
		},
		clickAdd: function() {
			$( '#submit-view-code' ).on( 'click', function( e ) {
				e.preventDefault();

				var code = tuongTacSoSanh.$input.val().toLowerCase();

				var filtered = tuongTacSoSanh.codes.filter( function( item ) {
					return item.value.toLowerCase() === code;
				} );

				if ( filtered.length ) {
					tuongTacSoSanh.insertColumn( filtered[0] );
				}

				tuongTacSoSanh.$input.val( '' );
				tuongTacSoSanh.$input.autocomplete( 'close' );
			} );
		},


		showColumn: function( codes ){

			for( var i = 3; i < codes.length ; i++){

				var data = codes[i].data;

				if ( i > 5 ) {
					break;
				}

				$('#bang-so-sanh').find('.h-table').append(
					'<tr class="ten-ma-nganh">\
						<td class="goi-y" colspan="2">' + data['ma'] + '-' + data['nganh'] +
							'<button class="btn-plush" id="' + data['ma'] + '">+</button>\
						</td>\
					</tr>'
				);

			}


			for( var i = 0; i < codes.length ; i++){

				if ( i > 2 ) {
					break;
				}

				var data = codes[i].data;
				var firstHeading = '<th rowspan="4" class="ten-ma-nganh">';
				if ( i > 0 ) {
					firstHeading += '<button id="' + data['ma'] + '" class="xoa">x</button>';
				}
				firstHeading +=  data['ma'] + '</br>' + data['nganh'] + '</th>';

				$('#bang-so-sanh').find('.th').append( firstHeading );


				$('#bang-so-sanh').find('.h-table-so-sanh td').each(function() {
					$(this).attr('colspan', Number($(this).attr('colspan')) + 1);
				});

				$('#bang-so-sanh tbody').find('tr').each(function(){

					var key = $(this).find('td').eq(0).attr('data-key');
					var tdValue =  data[key] ? data[key] : '';
					var td = '<td class="rm-' +  data['ma'] + ' txt-right txt-green">' + tdValue + '</td>';

					$(this).find('td').eq(1).after(td);

				});



			}
		},

		insertColumn: function(item) {

			var data = item.data;
			if ( tuongTacSoSanh.inserted.indexOf( data['ma'] ) !== -1 ) {
				alert("Mã đã tồn tại");
				return;
			}

			if ( tuongTacSoSanh.inserted.length === 5 ) {
				alert("chỉ so sánh được tối đa là 5 mã");
				return;
			}
			tuongTacSoSanh.inserted.push( data['ma'] );

			$('#bang-so-sanh').find('.th').append(
				'<th rowspan="4" class="ten-ma-nganh">\
					<button id="' + data['ma'] + '" class="xoa">x</button>' +
					data['ma'] + '</br>' + data['ten'] +
				'</th>'
			);

			$('#bang-so-sanh').find('.h-table-so-sanh td').each(function() {
				$(this).attr('colspan', Number($(this).attr('colspan')) + 1);
			});

			$('#bang-so-sanh tbody').find('tr').each(function(){

				var key = $(this).find('td').eq(0).attr('data-key');
				var tdValue =  data[key] ? data[key] : '';
				var td = '<td class="rm-' +  data['ma'] + ' txt-right txt-green">' + tdValue + '</td>';

				$(this).find('td').eq(1).after(td);
			});
		},
		removeColumn: function(item) {
			var th = item.closest('th');
			var code = item.attr( 'id' );
			tuongTacSoSanh.inserted = tuongTacSoSanh.inserted.filter( function ( elem ) {
				return elem !== code
			} );
			item.closest('th').remove();

			$('#bang-so-sanh').find('.h-table-so-sanh td').each(function() {
				$(this).attr('colspan', Number($(this).attr('colspan')) - 1);
			});

			$('#bang-so-sanh tbody').find('tr').each(function(){
				$(this).find('td.rm-' + item.attr('id') ).remove();
			});
		}
	}

	function handleDropDownNiemYet() {
		$( '.dropdown--niem-yet' ).on( 'click', '.dropdown--niem-yet__link', function( e ) {
			var $this = $( this );
			var $navTab = $( '.dropdown--niem-yet' ).children( 'a' );
			$navTab.text( $this.text() );
		} )
	}

	function clickNhanMaOTP() {
		$( '.init-lay-ma-otp-popup' ).on( 'click', function( e ) {
			$body.addClass( 'ma-otp-popup-enable' );
		} );
		$( '.nhan-otp-popup__background, .nhan-otp-popup__close' ).on( 'click', function( e ) {
			$body.removeClass( 'ma-otp-popup-enable' );
		} );
	}

	function loadLayMaOTP() {
		$( '.lay-ma-otp' ).show();
		setTimeout( function() {
			$( '.lay-ma-otp' ).hide();
		}, 180000 );
		$( '.lay-ma-otp__no' ).on( 'click', function( e ) {
			$( '.lay-ma-otp' ).hide();
		} );
		$( '.lay-ma-otp__yes' ).on( 'click', function( e ) {
			$( '.lay-ma-otp' ).hide();
			$body.addClass( 'ma-otp-popup-enable' );
		} );
	}

	/* Hàm xử lý gửi mã OTP */
	function guiMaOTP() {

	}
	$( '.nhan-otp-popup__body a' ).on( 'click', function( e ) {
		$( '.nhan-otp-popup__content' ).html( 'Gửi OTP thành công! Quý khách vui lòng kiểm tra SMS/Email <br><br>' );
		guiMaOTP();
	} );


	/* Hàm click xem chi tiết sổ lệnh */
	function chiTietSoLenh() {
		$( '.so-lenh-view' ).on( 'click', function() {
			$body.addClass( 'enable-so-lenh-edit' );
		} );
		$( '.so-lenh__close' ).on( 'click', function() {
			$body.removeClass( 'enable-so-lenh-edit' );
		} );
	}

	/* Hàm popup Nộp/Rút ký quỹ */
	function popupNopRut() {
		// Rút
		$( '.tab-rut' ).on( 'click', function() {
			$body.addClass( 'enable-yeu-cau-rut' );
		} );
		$( '.rut__close, .yeu-cau__background' ).on( 'click', function() {
			$body.removeClass( 'enable-yeu-cau-rut' );
		} );

		// Nộp
		$( '.tab-nop' ).on( 'click', function() {
			$body.addClass( 'enable-yeu-cau-nop' );
		} );
		$( '.nop__close, .yeu-cau__background' ).on( 'click', function() {
			$body.removeClass( 'enable-yeu-cau-nop' );
		} );
	}


	toggleTables();
	toggleTab();
	toggleClassActive();
	initModals();
	hideModals();
	toggleTableData();
	initChartSlider();
	removeCode();

	addCode.init(); // Phải đặt addCode init lên trước danhMucMBS để init autocomplete cho danh mục mặc định MBS.
	danhMucMBS.init();
	danhMucMoi.init();
	viewCode.init();
	datLenhPopup.init();
	tuongTacSoSanh.init();
	toggleFooterNavPopup();
	closeFooterNavPopup();
	closeFooterNavFooterPopup();
	//setMaxHeightFooterNavPopup();

	$window.on( 'load', function() {
		stickyHeader();
		toggleChart();
	} );
	$window.on( 'resize', stickyHeader );
	$window.on( 'orientchange', stickyHeader );

	var bangGia = new SortCodes( '#danh-muc .table__body tbody', 'orderBangGia' );
	var coBan = new SortCodes( '#danh-muc-co-ban .table__body tbody', 'orderCoban' );
	bangGia.init();
	coBan.init();

	var trongNgay = new DsLenh( $( '.dsl__dropdown__table--trong-ngay' ) );
	var gioLenh = new DsLenh( $( '.dsl__dropdown__table--gio-lenh' ) );
	var danhMucTaiSan = new DsLenh( $( '.dsl__dropdown__table--dmts' ) );
	trongNgay.init();
	gioLenh.init();
	danhMucTaiSan.init();

	runTooltip();
	$( document ).on( 'click', '.filter__buttons .buttons__save', filterTab.saveFilter );
	$stockHeaderNav.on( 'click', '.dropdown-menu a', highlightTab );

	phaiSinh.init();
	chungQuyen.init();
	itemDetails.init();
	market.init();
	accountManagement.init();
	filterTab.init();
	toggleViewMode();
	handleDropDownNiemYet();

	chiSo.init();

	$('#bang-so-sanh').on('click', 'button.xoa', function() {
		tuongTacSoSanh.removeColumn($(this));
	});

	$('#bang-so-sanh').on('click', 'button.btn-plush', function() {
		var code = $( this ).attr( 'id' ).toLowerCase();
		var filtered = tuongTacSoSanh.codes.filter( function( item ) {
			return item.value.toLowerCase().indexOf( code ) === 0;
		} );
		if ( filtered.length ) {
			tuongTacSoSanh.insertColumn( filtered[0] );
		}
	});


	clickNhanMaOTP();
	loadLayMaOTP();
	chiTietSoLenh();
	popupNopRut();
}( jQuery ) );
