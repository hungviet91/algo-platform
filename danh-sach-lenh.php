<?php include 'header.php'; ?>
<div class="fixed-components">
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
</div>

<div class="stock-tables__table dsl__tables">
	<?php include './danh-sach-lenh/trong-ngay.php'; ?>
	<?php include './danh-sach-lenh/gio-lenh.php'; ?>
	<?php include './danh-sach-lenh/phien-ke-tiep.php'; ?>
</div>

<?php include 'footer.php'; ?>
