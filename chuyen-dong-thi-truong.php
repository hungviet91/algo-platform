<?php include 'header-logged-in.php'; ?>
<div class="fixed-components">
	<?php include 'site-header-login.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-market.php'; ?>
</div>

<div class="market stock-tables__table">

	<header class="market__sub-navigation">
		<ul class="sub-navigation__item is-active" data-tab="#chuyen-dong-thi-truong">
			<li><a class="market__sub__link active" data-tippy-content="Biểu đồ thể hiện thông tin điểm ảnh hưởng và % ảnh hưởng của 30 cổ phiếu tác động nhất tới tăng giảm điểm của Bộ chỉ số, trong đó 15 mã ảnh hưởng chiều tăng và 15 mã ảnh hưởng chiều giảm." href="#top-co-phieu">TOP cổ phiếu đóng góp vào INDEX</a></li>
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ cho thấy sự phân bố của các mã cổ phiếu và các mã khác biệt so với số đông còn lại
			Trục ngang thể hiện tỷ lệ KLGD với trung bình KLGD 5 phiên. Trục đứng cho thấy đóng góp của từng mã vào return của index tại phiên hiện tại. Độ rộng của hình tròn tỷ lệ với vốn hóa của các cổ phiếu. Màu sắc hình tròn thể hiện giá đang tăng/giảm hay giữ nguyên so với phiên trước" href="#do-rong-thi-truong">Độ rộng thị trường</a></li>
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ so sánh dòng tiền tài cùng thời điểm với phiên trước, trung bình 1 tuần, 2 tuần và trung bình 1 tháng" href="#thanh-khoan">Thanh khoản</a></li>
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ thể hiện giá trị giao dịch của sàn đang tập trung vào những mã nào. Đơn vị giá trị giao dịch là triệu VND" href="#gt-kl">Giá trị và khối lượng giao dịch</a></li>
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ cho thấy phân bổ vốn hóa của tất cả các mã trền sàn" href="#von-hoa">Vốn hóa</a></li>
		</ul>
		<ul class="sub-navigation__item" data-tab="#chuyen-dong-to-chuc">
			<li><a class="market__sub__link" data-tippy-content="Màu xanh trên biểu đồ thể hiện khối ngoại mua ròng. Màu đỏ thể hiện khối ngoại bán ròng. Đơn vị giá trị là triệu VND" href="#nn">Nước ngoài mua bán ròng</a></li>
			<li><a class="market__sub__link" data-tippy-content="Màu xanh trên biểu đồ thể hiện khối tự doanh mua ròng. Màu đỏ thể hiện khối tự doanh bán ròng. Đơn vị giá trị là triệu VND" href="#tu-doanh">Tự doanh mua bán ròng</a></li>
		</ul>
		<ul class="sub-navigation__item" data-tab="#chuyen-dong-nganh">
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ thể hiện đóng góp của ngành vào INDEX. Màu xanh trên biểu đồ thể hiện ngành đóng góp dương (kéo INDEX lên), màu đỏ thể hiện đóng góp âm (kéo INDEX xuống)" href="#top-nganh">TOP ngành đóng góp vào INDEX</a></li>
			<li><a class="market__sub__link" data-tippy-content="Màu xanh trên biểu đồ thể hiện khối ngoại mua ròng. Màu đỏ thể hiện khối ngoại bán ròng. Đơn vị giá trị là triệu VND" href="#nn-nganh">Nước ngoài mua bán ròng theo ngành</a></li>
			<li><a class="market__sub__link" data-tippy-content="Màu xanh trên biểu đồ thể hiện khối tự doanh mua ròng. Màu đỏ thể hiện khối tự doanh bán ròng. Đơn vị giá trị là triệu VND" href="#tu-doanh-nganh">Tự doanh mua bán ròng theo ngành</a></li>
			<li><a class="market__sub__link" data-tippy-content="Biểu đồ thể hiện giá trị giao dịch ngành (đơn vị Triệu VND). Màu xanh trên biểu đồ thể hiện ngành đóng góp dương (kéo INDEX lên), màu đỏ thể hiện đóng góp âm (kéo INDEX xuống)" href="#gt-kl-nganh">Giá trị và khối lượng giao dịch theo ngành</a></li>
		</ul>
		<div class="sub-navigation__item" data-tab="#tin-hieu">
			<div class="backtest-options">
				<select class="js-my-select">
					<option value="bk-first-option">Ease of Movement (Backtest Performance)</option>
					<option value="bk-second-option">Relative Strength (Backtest Performance)</option>
				</select>
			</div>
			<div class="content-tin-hieu bk__option is-active" data-tab="bk-first-option">
				<h5><a class="init-backtest-modal" href=""><b>Ease of Movement (Backtest Performance)</b></a></h5>

				<table>
					<colgroup>
						<col></col>
						<col></col>
						<col></col>
						<col width="20%"></col>
						<col width="20%"></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
					</colgroup>
					<tr>
						<th>Ngày</th>
						<th>Sàn</th>
						<th>Mã CK</th>
						<th>Tên CK</th>
						<th>Ngành cấp 4</th>
						<th>Vốn hóa</th>
						<th>Giá hiện tại</th>
						<th>Tăng/Giảm</th>
						<th>5% VAR</th>
						<th>Tín hiệu</th>
						<th>Backtest Performance</th>
					</tr>
					<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
					<tr>
						<td class="txt-center">08/07/2019</td>
						<td class="txt-center">HNX</td>
						<td class="txt-center txt-green">ACB</td>
						<td>Ngân hàng Á Châu</td>
						<td>Ít biến động</td>
						<td class="txt-right">36,18</td>
						<td class="txt-right">29,000</td>
						<td class="txt-right">0.88</td>
						<td class="txt-right txt-red">-2.44</td>
						<td class="txt-center">Bán</td>
						<td class="txt-center txt-green"><a class="init-chitiet-backtest-modal" href="" href="#">Xem</a></td>
					</tr>
					<?php endfor; ?>
				</table>
			</div>
			<div class="content-tin-hieu bk__option second-option" data-tab="bk-second-option">
				<h5><a class="init-backtest-modal" href="#"><b>Relative Strength (Backtest Performance)</b></a></h5>
				<table>
					<colgroup>
						<col></col>
						<col></col>
						<col></col>
						<col width="20%"></col>
						<col width="20%"></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
					</colgroup>
					<tr>
						<th>Ngày</th>
						<th>Sàn</th>
						<th>Mã CK</th>
						<th>Tên CK</th>
						<th>Ngành cấp 4</th>
						<th>Vốn hóa</th>
						<th>Giá hiện tại</th>
						<th>RS 1M</th>
						<th>RS 3M</th>
						<th>RS 1Y</th>
						<th>Tín hiệu</th>
					</tr>
					<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
					<tr>
						<td class="txt-center">08/07/2019</td>
						<td class="txt-center">HNX</td>
						<td class="txt-center txt-green">ACB</td>
						<td>Ngân hàng Á Châu</td>
						<td>Ngân hàng</td>
						<td class="txt-right">38,263</td>
						<td class="txt-right">29,000</td>
						<td class="txt-right">100</td>
						<td class="txt-right txt-red">101</td>
						<td class="txt-center">101</td>
						<td class="txt-center txt-green"><a href="#">Bán</a></td>
					</tr>
					<?php endfor; ?>
				</table>
			</div>
		</div>
	</header>

	<section class="market__section is-active" data-tab="#top-co-phieu">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>

	</section>

	<section class="market__section" data-tab="#do-rong-thi-truong">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#thanh-khoan">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#nn">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tu-doanh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#gt-kl">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng: <span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#von-hoa">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#top-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#nn-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tu-doanh-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#gt-kl-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng: <span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tin-hieu">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>
</div>


<?php
include './modals/popup-backtest.php';
include './modals/chitiet-backtest.php';
include 'footer.php';
?>
