<div class="stock-tables__table bao-cao-tai-san-ps">
	<div id="vi-the-mo" class="stock-table table--active">
		<div class="tai-san-ps__header">
			<input type="text" name="" value="" placeholder="Mã HD">
			<select>
				<option>Vị thế (Tất cả)</option>
				<option>123456</option>
				<option>345678</option>
				<option>567890</option>
			</select>
		</div>

		<table class="table__body">
			<thead>
				<th class="sortable cell-highlight">
					<input class="dropdown__checkall" type="checkbox" name="">
				</th>
				<th class="sortable cell-highlight headerSortDown">STT</th>
				<th class="sortable cell-highlight">Mã HD</th>
				<th class="sortable cell-highlight">Ngày đáo hạn</th>
				<th class="sortable cell-highlight">Số vị thế mua</th>
				<th class="sortable cell-highlight">Số vị thế bán</th>
				<th class="sortable cell-highlight">Net</th>
				<th class="sortable cell-highlight">Giá mua TB</th>
				<th class="sortable cell-highlight">Giá bán TB</th>
				<th class="sortable cell-highlight">Giá thị trường</th>
				<th class="sortable cell-highlight">Lãi lỗ trong ngày</th>
				<th class="sortable cell-highlight">Mua</th>
				<th class="sortable cell-highlight">Bán</th>
				<th class="sortable cell-highlight">Đóng</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">1</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green"></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">2</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green"></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">3</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green"></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">4</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green"></td>
					<td class=""></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id="vi-the-dong" class="stock-table">
		<div class="tai-san-ps__header">
			<input type="text" name="" value="" placeholder="Mã HD">
			<select>
				<option>Vị thế (Tất cả)</option>
				<option>123456</option>
				<option>345678</option>
				<option>567890</option>
			</select>
		</div>

		<table class="table__body">
			<thead>
				<th class="sortable cell-highlight">
					<input class="dropdown__checkall" type="checkbox" name="">
				</th>
				<th class="sortable cell-highlight headerSortDown">STT</th>
				<th class="sortable cell-highlight">Mã HD</th>
				<th class="sortable cell-highlight">Ngày đáo hạn</th>
				<th class="sortable cell-highlight">Số vị thế mua</th>
				<th class="sortable cell-highlight">Số vị thế bán</th>
				<th class="sortable cell-highlight">Net</th>
				<th class="sortable cell-highlight">Giá mua TB</th>
				<th class="sortable cell-highlight">Giá bán TB</th>
				<th class="sortable cell-highlight">Giá thị trường</th>
				<th class="sortable cell-highlight">Lãi lỗ</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">1</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">2</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">3</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">4</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">3000</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
