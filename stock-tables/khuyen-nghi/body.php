<table class="table__body">
	<colgroup>
		<col>
		<col>
		<col>
		<col>
		<col>
		<col class="col-data">
		<col class="col-data">
		<col class="col-data">
		<col class="col-data">
		<col class="col-data">
		<col class="col-icon">
		<col class="col-icon">
		<col class="col-icon">
	</colgroup>
	<thead class="is-hidden">
		<tr>
			<th class="sortable td-col-2">Ngày<br>khuyến nghị</th>
			<th class="sortable td-col-2">Phương pháp</th>
			<th class="sortable td-col-2">Loại<br>khuyến nghị</th>
			<th class="sortable td-col-2">Sàn</th>
			<th class="sortable td-col-2">Mã CK</th>
			<th class="sortable td-col-2">Vùng giá<br>khuyến nghị</th>
			<th class="sortable">Giá hiện tại</th>
			<th class="sortable td-col-2">% so với vùng giá khuyến nghị</th>
			<th class="sortable">Vùng giá kỳ vọng</th>
			<th class="sortable td-col-2">% so với giá hiện tại</th>
			<th colspan="3" class="sortable">Thông tin liên quan</th>
		</tr>
	</thead>
	<tbody>
		<?php for( $i = 0; $i < 20; $i++ ) : ?>
			<tr>
				<td class="txt-green txt-center td-col-2">17/04/19</td>
				<td class="txt-green txt-center td-col-2">Cơ bản</td>
				<td class="txt-green txt-center td-col-2 cell-highlight">TÍCH LŨY</td>
				<td class="txt-green txt-center td-col-2 cell-highlight">HSX</td>
				<td class="txt-green txt-center td-col-2 cell-highlight"><a href="./chi-tiet-ma.php" target="_blank">MSH</a></td>
				<td class="txt-green td-col-2">48.5</td>
				<td class="txt-green">59.2</td>
				<td class="txt-green td-col-2 cell-highlight">3.62%</td>
				<td class="txt-green">56.5</td>
				<td class="txt-green td-col-2 cell-highlight">-4.25%</td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Chi tiết cổ phiếu"><i class="fas fa-info-circle"></i></a></td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Phân tích kỹ thuật"><i class="fas fa-cogs"></i></a></td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Báo cáo phân tích"><i class="fas fa-file-alt"></i></a></td>
			</tr>
			<tr>
				<td class="txt-green txt-center td-col-2">17/04/19</td>
				<td class="txt-red txt-center td-col-2">Cơ bản</td>
				<td class="txt-red txt-center td-col-2 cell-highlight">GIẢM TỶ TRỌNG</td>
				<td class="txt-red txt-center td-col-2 cell-highlight">UPCOM</td>
				<td class="txt-red txt-center td-col-2 cell-highlight"><a href="./chi-tiet-ma.php" target="_blank">QNS</a></td>
				<td class="txt-red td-col-2">38.5</td>
				<td class="txt-red">35.2</td>
				<td class="txt-red td-col-2 cell-highlight">-4.75%</td>
				<td class="txt-red">41.5</td>
				<td class="txt-red td-col-2 cell-highlight">-4.25%</td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Chi tiết cổ phiếu"><i class="fas fa-info-circle"></i></a></td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Phân tích kỹ thuật"><i class="fas fa-cogs"></i></a></td>
				<td class="txt-white txt-center"><a href="#" class="tooltip" data-tippy-content="Báo cáo phân tích"><i class="fas fa-file-alt"></i></a></td>
			</tr>
		<?php endfor; ?>
	</tbody>
</table>