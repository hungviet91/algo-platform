<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<colgroup>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col class="col-data">
			<col class="col-data">
			<col class="col-data">
			<col class="col-data">
			<col class="col-data">
			<col class="col-icon">
			<col class="col-icon">
			<col class="col-icon">
		</colgroup>
		<thead>
			<tr>
				<th data-index="0" class="sortable td-col-2">Ngày<br>khuyến nghị</th>
				<th data-index="1" class="sortable td-col-2">Phương pháp</th>
				<th data-index="2" class="sortable td-col-2 cell-highlight">Loại<br>khuyến nghị</th>
				<th data-index="3" class="sortable td-col-2 cell-highlight">Sàn</th>
				<th data-index="4" class="sortable td-col-2 cell-highlight">Mã CK</th>
				<th data-index="5" class="sortable td-col-2">Vùng giá<br>khuyến nghị</th>
				<th data-index="6" class="sortable">Giá hiện tại</th>
				<th data-index="7" class="sortable td-col-2 cell-highlight">% so với vùng giá khuyến nghị</th>
				<th data-index="8" class="sortable">Vùng giá kỳ vọng</th>
				<th data-index="9" class="sortable td-col-2 cell-highlight">% so với giá hiện tại</th>
				<th colspan="3">Thông tin liên quan</th>
			</tr>
		</thead>
	</table>
</div>