<div class="stock-tables__table bao-cao-lai-lo">
	<div id="lai-lo-du-kien" class="stock-table table--active">
		<table class="lai-lo__header">
			<tr>
				<td>Tài khoản</td>
				<td>
					<select>
						<option>123456</option>
						<option>345678</option>
						<option>567890</option>
					</select>
				</td>
				<td>Tên tài khoản</td>
				<td>
					<input type="text" name="" value="" placeholder="">
				</td>
				<td>
					<button type="submit" class="btn btn--primary">Tìm kiếm</button>
				</td>
			</tr>
		</table>

		<div class="table__header" style="margin-right: 4px;">
			<table>
				<thead>
					<th data-index="4" class="sortable">STT</th>
					<th data-index="5" class="sortable headerSortDown">TK</th>
					<th data-index="6" class="sortable">Mã CK</th>
					<th data-index="7" class="sortable">Loại</th>
					<th data-index="8" class="sortable">KL</th>
					<th data-index="9" class="sortable">Chờ nhận</th>
					<th data-index="11" class="sortable">Quyền chờ về</th>
					<th data-index="12" class="sortable">Giá TB</th>
					<th data-index="13" class="sortable">Giá hiện tại</th>
					<th data-index="14" class="sortable">Giá trị mua</th>
					<th data-index="15" class="sortable">Giá trị hiện tại</th>
					<th data-index="16" class="sortable">Lãi / lỗ</th>
					<th data-index="10" class="sortable">
						<span class="">% lãi / lỗ</span>
					</th>
					<th data-index="17" class="sortable">Bán</th>
				</thead>
			</table>
		</div>
		<div class="custom-scrollbar">
			<table class="table__body">
				<tbody>
					<tr>
						<td class="txt-pink">1</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">ABC</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-green">100</td>
						<td class="txt-green">0</td>
						<td class="txt-green">0</td>
						<td class="txt-green">0</td>
						<td class="txt-green">9,700</td>
						<td class="txt-green">0</td>
						<td class="txt-green">970,000</td>
						<td class="txt-green">970,000</td>
						<td class="txt-green">
							0
						</td>
						<td class="txt-green">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">2</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">ACB</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">0</td>
						<td class="txt-red">660</td>
						<td class="txt-red">0</td>
						<td class="txt-red">10,721</td>
						<td class="txt-red">22,000</td>
						<td class="txt-red">70,756,860</td>
						<td class="txt-red">14,520,000</td>
						<td class="txt-red">7,444,140</td>
						<td class="txt-red">
							105.2
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">3</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">ASM</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-green">10</td>
						<td class="txt-green">0</td>
						<td class="txt-green">7</td>
						<td class="txt-green">4,890</td>
						<td class="txt-green">6,150</td>
						<td class="txt-green">83,130</td>
						<td class="txt-green">10,4550</td>
						<td class="txt-green">21,420</td>
						<td class="txt-green">
							25.77
						</td>
						<td class="txt-green">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">4</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">5</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">6</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">7</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">8</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">9</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">10</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>

					</tr>
					<tr>
						<td class="txt-pink">6</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">7</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">8</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">9</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
					<tr>
						<td class="txt-pink">10</td>
						<td class="txt-pink">0112851</td>
						<td class="txt-cyan">DVD</td>
						<td class="txt-yellow">Thường</td>
						<td class="txt-red">100</td>
						<td class="txt-red">0</td>
						<td class="txt-red">0</td>
						<td class="txt-red">20,741</td>
						<td class="txt-red">3,500</td>
						<td class="txt-red">2,074,100</td>
						<td class="txt-red">350,000</td>
						<td class="txt-red">-1,724,100</td>
						<td class="txt-red">
							-83.13
						</td>
						<td class="txt-red">Bán</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="margin-right: 4px;">
			<table>
				<tbody>
					<tr>
						<td colspan="4" class="txt-pink cell-highlight">TỔNG CỘNG</td>
						<td class="txt-pink cell-highlight">6,481,335</td>
						<td class="txt-cyan cell-highlight">12,660</td>
						<td class="txt-yellow cell-highlight">7</td>
						<td class="txt-red"></td>
						<td class="txt-red"></td>
						<td class="txt-red">65,075,314,840</td>
						<td class="txt-red">132,107,746,550</td>
						<td class="txt-red">67,032,431,710</td>
						<td class="txt-red">103</td>
						<td class="txt-red cell-highlight"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div id="lai-lo-da-thuc-hien" class="stock-table">
		<table class="lai-lo__header">
			<tr>
				<td>Tài khoản</td>
				<td>
					<select>
						<option>123456</option>
						<option>345678</option>
						<option>567890</option>
					</select>
				</td>
				<td>Mã CK</td>
				<td>
					<input type="text" name="" value="" placeholder="">
				</td>
				<td>Sàn GD</td>
				<td>
					<select>
						<option>123456</option>
						<option>345678</option>
						<option>567890</option>
					</select>
				</td>
				<td>
					<button type="submit" class="btn btn--primary">Tìm kiếm</button>
				</td>
			</tr>
			<tr>
				<td>Thời hạn</td>
				<td>
					<select class="lai-lo__select">
						<option>2 tháng</option>
						<option>3 tháng</option>
						<option>4 tháng</option>
					</select>
				</td>
				<td>Từ ngày</td>
				<td><input type="date" name="" value="" placeholder=""></td>
				<td>Đến ngày</td>
				<td><input type="date" name="" value="" placeholder=""></td>
			</tr>
		</table>

		<div style="margin-right: 4px;" class="table__header">
			<table>
				<thead>
					<th data-index="4" class="sortable">STT</th>
					<th data-index="5" class="sortable headerSortDown">Ngày GD</th>
					<th data-index="6" class="sortable">Tài khoản</th>
					<th data-index="7" class="sortable">Nội dung</th>
					<th data-index="8" class="sortable">Mã CK</th>
					<th data-index="9" class="sortable">Trạng thái</th>
					<th data-index="11" class="sortable">KL</th>
					<th data-index="12" class="sortable">Giá khớp</th>
					<th data-index="13" class="sortable">Phí bán</th>
					<th data-index="14" class="sortable">Thuế/phí khác</th>
					<th data-index="15" class="sortable">Giá TB của CK tại thời điểm bán</th>
					<th data-index="16" class="sortable">Lãi / lỗ</th>
					<th data-index="10" class="sortable">
						<span class="">% lãi / lỗ</span>
					</th>
				</thead>
			</table>
		</div>
		<div class="custom-scrollbar">
			<table class="table__body">
				<tbody>
					<tr>
						<td class="txt-pink cell-highlight">1</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">2</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">3</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">4</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">5</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">6</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">7</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
					<tr>
						<td class="txt-pink cell-highlight">8</td>
						<td class="txt-pink cell-highlight">28/08/2019</td>
						<td class="txt-cyan cell-highlight">2278888</td>
						<td class="txt-yellow cell-highlight">BAN</td>
						<td class="txt-green">CTR</td>
						<td class="txt-green">02</td>
						<td class="txt-green">400</td>
						<td class="txt-green">65,500</td>
						<td class="txt-green">78,600</td>
						<td class="txt-green">26,400</td>
						<td class="txt-green cell-highlight">63,590</td>
						<td class="txt-green cell-highlight">
							658,920
						</td>
						<td class="txt-green">3</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="margin-right: 4px;">
			<table>
				<tbody>
					<tr>
						<td colspan="4">TỔNG CỘNG</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>16,439,510</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

</div>