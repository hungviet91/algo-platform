<div class="stock-tables__table bao-cao-lai-lo qly-lai-lo-ps">
	<div class="stock-table">
		<div class="qly-lai-lo-ps__title">
			<span class="page-title">Quản lý tài sản</span>
			<span>Số tài khoản</span>
			<input type="text" name="" value="" placeholder="">
			<button type="submit" class="btn btn--primary">Xem</button>
		</div>
		<table class="lai-lo__header">
			<tr>
				<td>Mã HD</td>
				<td>
					<input type="text" name="" value="" placeholder="">
				</td>
				<td>Từ ngày</td>
				<td><input type="date" name="" value="" placeholder=""></td>
				<td>Đến ngày</td>
				<td><input type="date" name="" value="" placeholder=""></td>
			</tr>
		</table>

		<div style="margin-right: 4px;" class="table__header">
			<table>
				<thead>
					<tr>
						<th class="cell-highlight" rowspan="3">STT</th>
						<th class="cell-highlight" rowspan="3">Ngày giao dịch</th>
						<th class="cell-highlight" rowspan="3">Ngày thanh toán</th>
						<th class="cell-highlight" rowspan="3">Mã hợp đồng</th>
						<th class="cell-highlight" colspan="4">Vị thế đóng</th>
						<th class="cell-highlight" rowspan="2" colspan="2">Vị thế còn lại</th>
						<th class="cell-highlight" rowspan="2" colspan="2">Tổng Vị thế</th>
						<th class="cell-highlight" rowspan="3">Giá mua TB</th>
						<th class="cell-highlight" rowspan="3">Giá bán TB</th>
						<th class="cell-highlight" rowspan="3">Giá đóng cửa</th>
						<th class="cell-highlight" rowspan="3">Tổng lãi / lỗ</th>
					</tr>
					<tr>
						<th class="cell-highlight" colspan="2">Đóng do giao dịch</th>
						<th class="cell-highlight" colspan="2">Đóng do đáo hạn</th>
					</tr>
					<tr>
						<th class="cell-highlight">Mua(Long)</th>
						<th class="cell-highlight">Bán(Short)</th>
						<th class="cell-highlight">Mua(Long)</th>
						<th class="cell-highlight">Bán(Short)</th>
						<th class="cell-highlight">Mua(Long)</th>
						<th class="cell-highlight">Bán(Short)</th>
						<th class="cell-highlight">Mua(Long)</th>
						<th class="cell-highlight">Bán(Short)</th>
					</tr>
				</thead>
			</table>
		</div>
		<div class="custom-scrollbar">
			<table class="table__body">
				<tbody>
					<tr>
						<td class="txt-pink">1</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">2</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">3</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">4</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">5</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">6</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
					<tr>
						<td class="txt-pink">7</td>
						<td class="txt-pink">25/10/2018</td>
						<td class="txt-cyan">26/10/2018</td>
						<td class="txt-yellow">VN30F2022</td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td class="txt-green">424</td>
						<td class="txt-green"></td>
						<td>1,050</td>
						<td></td>
						<td>1,060</td>
						<td>424,000,000</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div style="margin-right: 4px;">
			<table>
				<tbody>
					<tr>
						<td class="cell-highlight" colspan="10" style="text-align: right;">TỔNG CỘNG</td>
						<td class="cell-highlight"></td>
						<td class="cell-highlight"></td>
						<td class="cell-highlight"></td>
						<td class="cell-highlight"></td>
						<td class="cell-highlight"></td>
						<td class="cell-highlight">5,000,000</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>