<div class="stock-tables__table thong-tin-tai-khoan">
	<div id="thong-tin-chung" class="stock-table table--active">
		<table>
			<tr>
				<td>Tài khoản</td>
				<td><input type="text" name="" value="" placeholder="4608121"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>CMND/DKKD</td>
				<td><input type="text" name="" value="" placeholder="00118302"></td>
				<td>Ngày cấp</td>
				<td><input type="text" name="" value="" placeholder="08/02/2017"></td>
				<td>Hết hạn</td>
				<td><input type="text" name="" value="" placeholder="29/03/2023"></td>
				<td>Nơi cấp</td>
				<td><input type="text" name="" value="" placeholder="Cục cảnh sát HN"></td>
			</tr>
			<tr>
				<td>Hộ chiếu/GPTL</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Ngày cấp</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Hết hạn</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Nơi cấp</td>
				<td><input type="text" name="" value="" placeholder=""></td>
			</tr>
			<tr>
				<td>Giấy tờ khác</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Ngày cấp</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Hết hạn</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Nơi cấp</td>
				<td><input type="text" name="" value="" placeholder=""></td>
			</tr>
			<tr>
				<td>Tên đầy đủ</td>
				<td colspan="5"><input type="text" name="" value="" placeholder="Nguyễn Hồng Hanh"></td>
				<td>Giới tính</td>
				<td class="table-radio">
					<input type="radio" id="male" name="gender" value="male">
					<label for="male">Nam</label><br>
					<input type="radio" id="female" name="gender" value="female" checked>
					<label for="female">Nữ</label><br>
				</td>
			</tr>
			<tr>
				<td>Ngày sinh</td>
				<td><input type="text" name="" value="" placeholder="29/07/1983"></td>
				<td>Nơi sinh</td>
				<td><input type="text" name="" value="" placeholder=""></td>
				<td>Quốc tịch</td>
				<td><input type="text" name="" value="" placeholder="Việt Nam"></td>
				<td>Cư trú tại VN</td>
				<td class="table-radio">
					<input type="radio" id="yes" name="cu-tru" value="yes" checked>
					<label for="male">Có</label><br>
					<input type="radio" id="no" name="cu-tru" value="no">
					<label for="female">Không</label><br>
				</td>
			</tr>
			<tr>
				<td>Địa chỉ thường chú</td>
				<td colspan="5"><input type="text" name="" value="" placeholder="Số 3 Liễu Giai, Ba Đình, Hà Nội"></td>
				<td>Điện thoại CQ</td>
				<td><input type="text" name="" value="" placeholder="0916033837"></td>
			</tr>
			<tr>
				<td>Địa chỉ hiện tại</td>
				<td colspan="5"><input type="text" name="" value="" placeholder="Số 3 Liễu Giai, Ba Đình, Hà Nội"></td>
				<td>Điện thoại DĐ</td>
				<td><input type="text" name="" value="" placeholder="0916033837"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td colspan="5"><input type="text" name="" value="" placeholder="hanh1.nguyenhong@mbs.com.vn"></td>
			</tr>
			<tr>
				<td>Khi cần liên hệ</td>
				<td>
					<select>
						<option selected>Ông</option>
						<option>Bà</option>
					</select>
				</td>
				<td colspan="4"><input type="text" name="" value="" placeholder="Bùi Sơn Tú"></td>
				<td>Quan hệ</td>
				<td><input type="text" name="" value="" placeholder="Chồng"></td>
			</tr>
			<tr>
				<td>Địa chỉ liên lạc</td>
				<td colspan="5"><input type="text" name="" value="" placeholder=""></td>
				<td>Điện thoại</td>
				<td><input type="text" name="" value="" placeholder=""></td>
			</tr>
		</table>
		<hr style="margin: 20px 0;">
		<b style="color: #fff; font-size: 13px;"><i>Nhân viên chăm sóc</i></b>
		<table>
			<tr>
				<td>Tên đầy đủ</td>
				<td colspan="2"><input type="text" name="" value="" placeholder="Vũ Tuấn Duy"></td>
				<td>Chi nhánh/PGD</td>
				<td colspan="2"><input type="text" name="" value="" placeholder="SỞ GIAO DỊCH"></td>
			</tr>
			<tr>
				<td>Email</td>
				<td colspan="2"><input type="email" name="" value="" placeholder="duyvutuan@mbs.com.vn"></td>
				<td>Điện thoại</td>
				<td colspan="2"><input type="text" name="" value="" placeholder="0914901111"></td>
			</tr>
		</table>
	</div>

	<div id="thiet-lap" class="stock-table">
		<div class="table__filter__item dsl__dropdown">
			<label>Mã TK</label>
			<input type="text" name="" value="" placeholder="">
			<button type="submit" class="btn btn--primary">Tìm kiếm</button>
		</div>

		<table class="dsl__dropdown__table">
			<thead>
				<tr>
					<th class="txt-center">Họ tên</th>
					<th>Mã TK</th>
					<th>Loại TK</th>
					<th>Mặc định</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="txt-center">Nguyễn Hồng Hạnh</td>
					<td class="txt-center">8803491</td>
					<td class="txt-center">Tài khoản thường</td>
					<td class="txt-center"><input id="thuong" class="dsl__dropdown__checkbox" type="radio" name="default" value="thuong" checked></td>
				</tr>
				<tr>
					<td class="txt-center">Nguyễn Hồng Hạnh</td>
					<td class="txt-center">8803491</td>
					<td class="txt-center">Tài khoản ký quỹ</td>
					<td class="txt-center"><input id="ky-quy" class="dsl__dropdown__checkbox" type="radio" name="default" value="ky-quy"></td>
				</tr>
			</tbody>
		</table>

		<div class="table__filter__item dsl__dropdown">
			<label>TK Mặc định</label>
			<input type="text" name="" value="" placeholder="">
			<button type="submit" class="btn btn--primary">Xác nhận</button>
			<button type="submit" class="btn btn--primary">Đóng</button>
		</div>
	</div>

</div>
