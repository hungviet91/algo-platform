<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<tr>
			<th data-index="0" rowspan="2" class="sortable ma-cq">Mã CQ</th>
			<th data-index="1" rowspan="2" class="sortable ngay-dh  has-toggle toggle--ngay">
				<a class="toggle__left" data-toggle="ngay"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">TCPH</span>
				<span class="">Ngày GDCC</span>
				<a class="toggle__right" data-toggle="ngay"><i class="fa fa-caret-right"></i></a>
			</th>
			<th data-index="2" rowspan="2" class="sortable cell-highlight">Trần</th>
			<th data-index="3" rowspan="2" class="sortable cell-highlight">Sàn</th>
			<th data-index="4" rowspan="2" class="sortable cell-highlight">TC</th>
			<th colspan="6">Dư mua</th>
			<th colspan="3" class="cell-highlight">Khớp lệnh</th>
			<th colspan="6">Dư bán</th>
			<th data-index="5" rowspan="2" class="sortable tong-kl">Tổng KL</th>
			<th colspan="2" class="has-toggle cell-highlight toggle--gia-cq">
				<a class="toggle__left" data-toggle="gia-cq"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">Dư</span>
				<span>Giá</span>
				<a class="toggle__right" data-toggle="gia-cq"><i class="fa fa-caret-right"></i></a>
			</th>
			<th colspan="2" class="has-toggle toggle--gia-nn">
				<a class="toggle__left" data-toggle="gia-nn"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">NN</span>
				<span>Cơ sở</span>
				<a class="toggle__right" data-toggle="gia-nn"><i class="fa fa-caret-right"></i></a>
			</th>
			<th data-index="6" class="sortable" rowspan="2">TH</th>
			<th data-index="7" class="sortable" rowspan="2">Độ<br>lệch</th>
			<th data-index="8" class="sortable" rowspan="2"><span>TL<br>CĐ</span></th>
			<th data-index="9" class="sortable" rowspan="2">ĐHV</th>
		</tr>
		<tr>
			<th data-index="10" class="sortable">Giá 3</th>
			<th data-index="11" class="sortable">KL 3</th>
			<th data-index="12" class="sortable">Giá 2</th>
			<th data-index="13" class="sortable">KL 2</th>
			<th data-index="14" class="sortable">Giá 1</th>
			<th data-index="15" class="sortable">KL 1</th>
			<th data-index="16" class="cell-highlight sortable">Giá</th>
			<th data-index="17" class="cell-highlight sortable has-toggle toggle--percent">
				<a class="toggle__left" data-toggle="percent"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">%</span>
				<span class="">+/-</span>
				<a class="toggle__right" data-toggle="percent"><i class="fa fa-caret-right"></i></a>
			</th>
			<th data-index="18" class="cell-highlight sortable">KL</th>
			<th data-index="19" class="sortable">Giá 1</th>
			<th data-index="20" class="sortable">KL 1</th>
			<th data-index="21" class="sortable">Giá 2</th>
			<th data-index="22" class="sortable">KL 2</th>
			<th data-index="23" class="sortable">Giá 3</th>
			<th data-index="24" class="sortable">KL 3</th>
			<th data-index="25" class="cell-highlight sortable toggle--gia-cq">
				<span class="hidden-first">Mua</span>
				<span>Cao</span>
			</th>
			<th data-index="26" class="cell-highlight sortable toggle--gia-cq">
				<span class="hidden-first">Bán</span>
				<span>Thấp</span>
			</th>
			<th data-index="27" class="sortable toggle--gia-nn">
				<span class="hidden-first">Mua</span>
				<span>Mã</span>
			</th>
			<th data-index="28" class="sortable toggle--gia-nn">
				<span class="hidden-first">Bán</span>
				<span>giá</span>
			</th>
		</tr>
	</table>
</div>