<table class="table__body">
	<thead class="is-hidden">
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
	</thead>
	<tbody>
		<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
			<tr class="item">
				<td class="txt-ma txt-green ma-cq">CMWG1904</td>
				<td class="ngay-dh toggle--ngayt">
					<span class="hidden-first">VND</span>
					<span>19/05/19</span>
				</td>
				<td class="txt-pink cell-highlight"><?php echo rand(10,100); ?></td>
				<td class="txt-cyan cell-highlight"><?php echo rand(10,100); ?></td>
				<td class="txt-yellow cell-highlight"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green cell-highlight"><?php echo rand(10,100); ?></td>
				<td class="txt-green cell-highlight toggle--percent">
					<span class="hidden-first">10%</span>
					<span>0.50</span>
				</td>
				<td class="txt-green cell-highlight"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-green"><?php echo rand(10,100); ?></td>
				<td class="txt-white tong-kl"><?php echo rand(10,100); ?></td>

				<td class="txt-green cell-highlight toggle--gia-cq">
					<span class="hidden-first"><?php echo rand(10,100); ?></span>
					<span><?php echo rand(10,100); ?></span>
				</td>
				<td class="txt-green cell-highlight toggle--gia-cq">
					<span class="hidden-first"><?php echo rand(10,100); ?></span>
					<span><?php echo rand(10,100); ?></span>
				</td>

				<td class="txt-green toggle--gia-nn">
					<span class="hidden-first"><?php echo rand(10,100); ?></span>
					<span><?php echo rand(10,100); ?></span>
				</td>
				<td class="txt-green toggle--gia-nn">
					<span class="hidden-first"><?php echo rand(10,100); ?></span>
					<span><?php echo rand(10,100); ?></span>
				</td>
				<td><?php echo rand(10,100); ?></td>
				<td><?php echo rand(10,100); ?></td>
				<td><?php echo rand(10,100); ?></td>
				<td><?php echo rand(10,100); ?></td>
			</tr>
			<tr class="item-info tablesorter-childRow">
				<td colspan="29">
					<div class="item-info__wrap">
						<div class="item-info__content">
							<div class="item-info__tabs">
								<a href="#giao-dich-trong-ngay">Giao dịch trong ngày</a>
								<a href="#dinh-gia-chung-quyen" class="is-active">Định giá chứng quyền</a>
								<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
								<a href="./chung-quyen.php" target="_blank">Phân tích kỹ thuật</a>
							</div>
							<div class="item-info__tab" data-tab="#giao-dich-trong-ngay">
								<div class="item-info__section">
									<img src="./images/phai-sinh/1.svg">
								</div>
								<div class="item-info__section">
									<img src="./images/phai-sinh/1.svg">
								</div>
								<div class="item-info__section">
									<img src="./images/phai-sinh/2.svg">
								</div>
								<div class="item-info__section">
									<img src="./images/phai-sinh/3.svg">
								</div>
							</div>
							<div class="item-info__tab" data-tab="#lich-su-giao-dich">
								<div class="item-info__section">
									<img src="./images/phai-sinh/4.svg">
								</div>
								<div class="item-info__section">
									<img src="./images/phai-sinh/1.svg">
								</div>
								<div class="item-info__section item-info__section--no-padding">
									<table>
										<tr>
											<th rowspan="2">Ngày</th>
											<th rowspan="2">Đóng cửa</th>
											<th rowspan="2">Thay đổi giá</th>
											<th rowspan="2">Giá tham chiếu</th>
											<th rowspan="2">NN<br>mua bán ròng</th>
											<th colspan="2">Khớp lệnh</th>
										</tr>
										<tr>
											<th>KLGD</th>
											<th>GTGD</th>
										</tr>
										<?php for ( $i = 1; $i <= 5; $i++ ) : ?>
											<tr>
												<td class="txt-green">26/4/2019</td>
												<td class="txt-green">877</td>
												<td class="txt-green">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
												<td class="txt-green">856</td>
												<td class="txt-green">125</td>
												<td class="txt-green">101,200</td>
												<td class="txt-green">1,500,350,000</td>
											</tr>
											<tr>
												<td class="txt-red">26/4/2019</td>
												<td class="txt-red">877</td>
												<td class="txt-red">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
												<td class="txt-red">856</td>
												<td class="txt-red">125</td>
												<td class="txt-red">101,200</td>
												<td class="txt-red">1,500,350,000</td>
											</tr>
										<?php endfor; ?>
									</table>
								</div>
							</div>
							<div class="item-info__tab is-active" data-tab="#dinh-gia-chung-quyen">
								<div class="item-info__section item-info__section--no-padding">
									<table class="table-chung-quyen">
										<colgroup>
											<col width="20%"></col>
											<col></col>
											<col width="30%"></col>
											<col></col>
											<col></col>
											<col></col>
										</colgroup>
										<tr>
											<td>Giá thực hiện:</td>
											<td class="txt-green">2.99</td>
											<td>Giá thị trường:</td>
											<td class="txt-green">33</td>
											<td>Delta:</td>
											<td class="txt-green">856</td>
										</tr>
										<tr>
											<td>Giá CK cơ sở:</td>
											<td class="txt-green">21.35</td>
											<td>Định giá:</td>
											<td class="txt-green">125</td>
											<td>Gamma:</td>
											<td class="txt-green">856</td>
										</tr>
										<tr>
											<td>Trạng thái:</td>
											<td class="txt-green">OTM</td>
											<td>Độ lệch chuẩn:</td>
											<td class="txt-green">125</td>
											<td>Vega:</td>
											<td class="txt-green">856</td>
										</tr>
										<tr>
											<td>Điểm hòa vốn:</td>
											<td class="txt-green">24.2</td>
											<td>Độ lệch chuẩn hàm ý:</td>
											<td class="txt-green">125</td>
											<td>Theta:</td>
											<td class="txt-green">856</td>
										</tr>
										<tr>
											<td>Giá IPO:</td>
											<td class="txt-green">30</td>
											<td>Gearing:</td>
											<td class="txt-green">1000</td>
											<td>Rho:</td>
											<td class="txt-green">856</td>
										</tr>
										<tr>
											<td>Khối lượng phát hành:</td>
											<td class="txt-green">30</td>
											<td>Effective gearing:</td>
											<td class="txt-green">1000</td>
											<td></td>
											<td class="txt-green"></td>
										</tr>
									</table>
								</div>
								<div class="item-info__section">
									<img src="./images/phai-sinh/1.svg">
								</div>
							</div>
							<div class="item-info__tab" data-tab="#phan-tich-do-lech">
								<div class="item-info__section">
									<img src="./images/chung-quyen/6.svg">
								</div>
								<div class="item-info__section">
									<img src="./images/chung-quyen/7.svg">
								</div>
							</div>
							<div class="item-info__tab" data-tab="#anh-huong-vn30">
								<div class="item-info__section">
									<img src="./images/chung-quyen/8.svg">
								</div>
							</div>
						</div>
						<div class="item-info__sidebar">
							<div class="item-info__sidebar__info">
								<div class="code-details__title">
									<span class="code-details__code">ABC123</span>
									<span class="txt-green"><i class="fas fa-caret-up"></i></span>
									<span class="code-details__price">29</span>
									<span class="code-details__diff txt-green">+0.5%<br>+0.50</span>
								</div>
								<table class="item-info__sidebar__details">
									<tr>
										<td>Tổ chức phát hành:</td>
										<td class="txt-right">ABC</td>
									</tr>
									<tr>
										<td>Loại chứng quyền:</td>
										<td class="txt-right">ABC</td>
									</tr>
									<tr>
										<td>Kiểu chứng quyền:</td>
										<td class="txt-right">ABC</td>
									</tr>
									<tr>
										<td>Ngày đáo hạn:</td>
										<td class="txt-right">12/09/19</td>
									</tr>
									<tr>
										<td>Ngày giao dịch cuối cùng:</td>
										<td class="txt-right">12/09/19</td>
									</tr>
									<tr>
										<td>Thời gian đáo hạn:</td>
										<td class="txt-right">56.15</td>
									</tr>
									<tr>
										<td>Tỷ lệ chuyển đổi:</td>
										<td class="txt-right">50%</td>
									</tr>
									<tr>
										<td>Giá thực hiện:</td>
										<td class="txt-right">50%</td>
									</tr>
									<tr>
										<td>Giá CK cơ sở:</td>
										<td class="txt-right">50%</td>
									</tr>
									<tr>
										<td>Trạng thái:</td>
										<td class="txt-right">50%</td>
									</tr>
									<tr>
										<td>Điểm hòa vốn:</td>
										<td class="txt-right">50%</td>
									</tr>
								</table>
							</div>
							<div class="item-info__sidebar__nav">
								<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
								<a href="#buoc-gia">Bước giá</a>
							</div>
							<div class="item-info__sidebar__tab is-active custom-scrollbar" data-tab="#khop-lenh">
								<table style="position: sticky;top: 0;">
									<tr>
										<th>Thời gian</th>
										<th>Giá</th>
										<th>KL</th>
										<th>Tổng KL</th>
									</tr>
								</table>
								<table>
									<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
										<tr>
											<td class="txt-green txt-right">15:05:20</td>
											<td class="txt-green txt-right">95.00</td>
											<td class="txt-green txt-right">39</td>
											<td class="txt-green txt-right">153</td>
										</tr>
									<?php endfor; ?>
								</table>
							</div>
							<div class="item-info__sidebar__tab custom-scrollbar" data-tab="#buoc-gia">
								<table style="position: sticky;top: 0;">
									<tr>
										<th colspan="2">Dư mua</th>
										<th colspan="2">Dư bán</th>
									</tr>
									<tr>
										<th>Giá</th>
										<th>KL</th>
										<th>KL</th>
										<th>Giá</th>
									</tr>
								</table>
								<table>
									<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
										<tr>
											<td class="txt-green txt-right">85.75</td>
											<td class="txt-green txt-right">35</td>
											<td class="txt-green txt-right">87.50</td>
											<td class="txt-green txt-right">36</td>
										</tr>
									<?php endfor; ?>
									<tr>
										<td class="txt-center code-details__tables__total" colspan="2">350</td>
										<td class="txt-center code-details__tables__total" colspan="2">360</td>
									</tr>
								</table>
								<div class="item-info__sidebar__stats">
									<div class="bg-green-2" style="width: 23%">23%</div>
									<div class="bg-red" style="width: 77%">77%</div>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>
		<?php endfor; ?>
	</tbody>

</table>

