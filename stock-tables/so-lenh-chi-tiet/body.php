<div class="stock-tables__table so-lenh-chi-tiet">
	<div id="danh-sach-lenh" class="stock-table table--active">
		<div class="so-lenh__header">
			<div class="so-lenh__header--top">
				<input type="text" name="" value="" placeholder="Mã HD">
				<select>
					<option>Loại lệnh</option>
					<option>123456</option>
					<option>345678</option>
					<option>567890</option>
				</select>
				<select>
					<option>Kênh đặt lệnh</option>
					<option>123456</option>
					<option>345678</option>
					<option>567890</option>
				</select>
			</div>
			<div class="so-lenh__header--bottom">
				<div>Lọc lệnh theo trạng thái</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Tất cả</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Đang chờ khớp</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Khớp</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Khớp 1 phần</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Đang hủy</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Hủy</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Không thành công</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Hết hiệu lực</div>
			</div>
		</div>

		<table class="table__body so-lenh__table">
			<thead>
				<th class="sortable cell-highlight">
					<input class="dropdown__checkall" type="checkbox" name="">
				</th>
				<th class="sortable cell-highlight headerSortDown">Số hiệu lệnh</th>
				<th class="sortable cell-highlight">Thời gian</th>
				<th class="sortable cell-highlight">Mã HD</th>
				<th class="sortable cell-highlight">Loại lệnh</th>
				<th class="sortable cell-highlight">KL đặt</th>
				<th class="sortable cell-highlight">Giá đặt</th>
				<th class="sortable cell-highlight">Giờ khớp</th>
				<th class="sortable cell-highlight">KL khớp</th>
				<th class="sortable cell-highlight">Giá khớp</th>
				<th class="sortable cell-highlight">Giờ hủy</th>
				<th class="sortable cell-highlight">KL hủy</th>
				<th class="sortable cell-highlight">Trạng thái</th>
				<th class="sortable cell-highlight">Kênh đặt lệnh</th>
				<th class="sortable cell-highlight">Loại lệnh</th>
				<th class="sortable cell-highlight">Chi tiết</th>
				<th colspan="2" class="sortable cell-highlight">Thao tác</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--green">Mua</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
				<tr>
					<td class="txt-pink">
						<input class="" type="checkbox" name="">
					</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Chờ kích hoạt</td>
					<td class="">Web trading</td>
					<td class="">Stop</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary btn--red">Hủy</button>
					</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary">Sửa</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id="lich-su-dat-lenh" class="stock-table">
		<div class="so-lenh__header">
			<div class="so-lenh__header--top">
				<input type="text" name="" value="" placeholder="Mã HD">
				<select>
					<option>Loại lệnh</option>
					<option>123456</option>
					<option>345678</option>
					<option>567890</option>
				</select>
				<select>
					<option>Kênh đặt lệnh</option>
					<option>123456</option>
					<option>345678</option>
					<option>567890</option>
				</select>
			</div>
			<div class="so-lenh__header--bottom">
				<div>Lọc lệnh theo trạng thái</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Tất cả</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Khớp</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Khớp 1 phần</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Hủy</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Không thành công</div>
				<div><input class="so-lenh__table__checkbox" type="checkbox" name="" value="">Hết hiệu lực</div>
			</div>
		</div>

		<table class="table__body">
			<thead>
				<th class="sortable cell-highlight">Thời gian</th>
				<th class="sortable cell-highlight headerSortDown">Số hiệu lệnh</th>
				<th class="sortable cell-highlight">Mã HD</th>
				<th class="sortable cell-highlight">Loại lệnh</th>
				<th class="sortable cell-highlight">KL đặt</th>
				<th class="sortable cell-highlight">Giá đặt</th>
				<th class="sortable cell-highlight">KL khớp</th>
				<th class="sortable cell-highlight">Giá khớp</th>
				<th class="sortable cell-highlight">KL hủy</th>
				<th class="sortable cell-highlight">Phí GD</th>
				<th class="sortable cell-highlight">Thuế</th>
				<th class="sortable cell-highlight">Trạng thái</th>
				<th class="sortable cell-highlight">Kênh đặt lệnh</th>
				<th class="sortable cell-highlight">Loại lệnh</th>
				<th class="sortable cell-highlight">Chi tiết</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
				<tr>
					<td class="txt-cyan">11/05/2019</td>
					<td class="txt-pink">0112851</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="btn--red">Bán</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class="">5</td>
					<td class="">920</td>
					<td class=""></td>
					<td class="">80,000</td>
					<td class="">23,000</td>
					<td class="">Đã khớp</td>
					<td class="">Môi giới / Giao dịch</td>
					<td class="">Thường</td>
					<td class="txt-green">
						<button type="submit" class="btn btn--primary so-lenh-view">Xem</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="so-lenh__edit">
	<div class="so-lenh__background"></div>
	<div class="so-lenh__body">
		<table class="table__body so-lenh__table">
			<thead>
				<th class="sortable cell-highlight headerSortDown">STT</th>
				<th class="sortable cell-highlight">Thời gian</th>
				<th class="sortable cell-highlight">Loại lệnh</th>
				<th class="sortable cell-highlight">Mã HD</th>
				<th class="sortable cell-highlight">KL đặt</th>
				<th class="sortable cell-highlight">Giá đặt</th>
				<th class="sortable cell-highlight">Thời gian khớp</th>
				<th class="sortable cell-highlight">KL khớp</th>
				<th class="sortable cell-highlight">Giá khớp</th>
				<th class="sortable cell-highlight">KL hủy</th>
				<th class="sortable cell-highlight">Lệnh ĐK</th>
				<th class="sortable cell-highlight">Giá ĐK</th>
				<th class="sortable cell-highlight">Giá KH</th>
				<th class="sortable cell-highlight">Giờ KH</th>
				<th class="sortable cell-highlight">Kênh đặt lệnh</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-pink">1</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="btn--red">Đặt</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="">Stop</td>
					<td class="txt-green">708</td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Web trading</td>
				</tr>
				<tr>
					<td class="txt-pink">2</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="btn--red">Sửa</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="">Stop</td>
					<td class="txt-green">708</td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Web trading</td>
				</tr>
				<tr>
					<td class="txt-pink">3</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="btn--red">Sửa</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">5</td>
					<td class="">700</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class="">Stop</td>
					<td class="txt-green">708</td>
					<td class=""></td>
					<td class=""></td>
					<td class="txt-green">Web trading</td>
				</tr>
			</tbody>
		</table>

		<span class="so-lenh__close btn btn--primary">Đóng</i></span>
	</div>
</div>