<div class="stock-tables__table giao-dich-tien">
	<div class="stock-table table--active">
		<table class="table__body">
			<thead>
				<th class="sortable cell-highlight headerSortDown">STT</th>
				<th class="sortable cell-highlight">Thời gian yêu cầu</th>
				<th class="sortable cell-highlight">Tên người nhận</th>
				<th class="sortable cell-highlight">Số tài khoản bên nhận</th>
				<th class="sortable cell-highlight">Số tiên chuyển</th>
				<th class="sortable cell-highlight">Loại yêu cầu</th>
				<th class="sortable cell-highlight">Phí GD</th>
				<th class="sortable cell-highlight">Trạng thái</th>
				<th class="sortable cell-highlight">Thao tác</th>
			</thead>
			<tbody>
				<tr>
					<td class="txt-pink">1</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">Nguyễn Văn A</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">100,000,000</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">1</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">Nguyễn Văn A</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">100,000,000</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">1</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">Nguyễn Văn A</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">100,000,000</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
				<tr>
					<td class="txt-pink">1</td>
					<td class="txt-cyan">11/05/2019</td>
					<td class="">Nguyễn Văn A</td>
					<td class="txt-yellow">VN30F1908</td>
					<td class="">100,000,000</td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
					<td class=""></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="stock-table">

	</div>
</div>

<div class="yeu-cau yeu-cau-rut">
	<div class="yeu-cau__background"></div>
	<div class="yeu-cau__wrapper">
		<div class="yeu-cau__header">
			Yêu cầu [Rút ký quỹ GD CKPS từ VSD]
		</div>
		<div class="yeu-cau__body">
			<span><i>Thời gian thực hiện từ 8h30 đến 15h45 các ngày làm việc. Quý khách hàng lưu ý khi thực hiện nộp/rút ký quỹ để hạn chế phát sinh phí Quản lý tài sản ký quỹ. Chi tiết biểu phí tại đây:</i></span>
			<br><br>
			<div><b>NGƯỜI YÊU CẦU</b></div>
			<hr>
			<table class="table__body">
				<tbody>
					<tr>
						<td>Họ tên</td>
						<td><b>Phan Thị Cẩm Thanh</b></td>
					</tr>
					<tr>
						<td>Số CMND / Hộ chiếu:</td>
						<td><b>012928668</b></td>
					</tr>
					<tr>
						<td>Số tài khoản tại MBS:</td>
						<td><b>005C006494</b></td>
					</tr>
					<tr>
						<td>Số tiền có thể chuyển:</td>
						<td><b>-</b></td>
					</tr>
					<tr>
						<td>Số tiền chuyển:</td>
						<td>
							<input type="number" name=""> <b><i>Đơn vị tiền:</i></b> VNĐ
						</td>
					</tr>
					<tr>
						<td>Nội dung chuyển tiền:</td>
						<td>
							<textarea type="textarea" rows="5" name=""></textarea>
						</td>
					</tr>
					<tr>
						<td>Mã OTP:</td>
						<td>
							<input type="text" name="">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<span class="yeu-cau__close rut__close btn btn--primary">Đóng</i></span>
		<span class="yeu-cau__xacnhan btn btn--primary">Xác nhận</i></span>
	</div>
</div>

<div class="yeu-cau yeu-cau-nop">
	<div class="yeu-cau__background"></div>
	<div class="yeu-cau__wrapper">
		<div class="yeu-cau__header">
			Yêu cầu [Nộp ký quỹ GD CKPS từ VSD]
		</div>
		<div class="yeu-cau__body">
			<span><i>Thời gian thực hiện từ 8h30 đến 15h45 các ngày làm việc. Quý khách hàng lưu ý khi thực hiện nộp/rút ký quỹ để hạn chế phát sinh phí Quản lý tài sản ký quỹ. Chi tiết biểu phí tại đây:</i></span>
			<br><br>
			<div><b>NGƯỜI YÊU CẦU</b></div>
			<hr>
			<table>
				<tbody>
					<tr>
						<td>Họ tên</td>
						<td><b>Phan Thị Cẩm Thanh</b></td>
					</tr>
					<tr>
						<td>Số CMND / Hộ chiếu:</td>
						<td><b>012928668</b></td>
					</tr>
					<tr>
						<td>Số tài khoản tại MBS:</td>
						<td><b>005C006494</b></td>
					</tr>
					<tr>
						<td>Số tiền có thể chuyển:</td>
						<td><b>-</b></td>
					</tr>
					<tr>
						<td>Số tiền chuyển:</td>
						<td>
							<input type="number" name=""> <b><i>Đơn vị tiền:</i></b> VNĐ
						</td>
					</tr>
					<tr>
						<td>Nội dung chuyển tiền:</td>
						<td>
							<textarea type="textarea" rows="5" name=""></textarea>
						</td>
					</tr>
					<tr>
						<td>Mã OTP:</td>
						<td>
							<input type="text" name="">
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<span class="yeu-cau__close nop__close btn btn--primary">Đóng</i></span>
		<span class="yeu-cau__xacnhan btn btn--primary">Xác nhận</i></span>
	</div>
</div>