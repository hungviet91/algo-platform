<div class="stock-tables__table">
	<div id="danh-muc" class="stock-table table--active">
		<?php
		include( 'stock-tables/danh-muc/bang-gia/header.php' );
		include( 'stock-tables/danh-muc/bang-gia/body.php' );
		?>
	</div>

	<div id="danh-muc-co-ban" class="stock-table stock-table--co-ban">
		<?php
		include( 'stock-tables/danh-muc/co-ban/header.php' );
		include( 'stock-tables/danh-muc/co-ban/body.php' );
		?>
	</div>

	<div id="niem-yet" class="stock-table">
		<?php
		include( 'stock-tables/niem-yet/bang-gia/header.php' );
		include( 'stock-tables/niem-yet/bang-gia/body.php' );
		?>
	</div>

	<div id="niem-yet-co-ban" class="stock-table stock-table--co-ban">
		<?php
		include( 'stock-tables/niem-yet/co-ban/header.php' );
		include( 'stock-tables/niem-yet/co-ban/body.php' );
		?>
	</div>

	<div id="khuyen-nghi" class="stock-table">
		<?php
		include( 'stock-tables/khuyen-nghi/header.php' );
		include( 'stock-tables/khuyen-nghi/body.php' );
		?>
	</div>

	<div id="thoa-thuan" class="stock-table" >
		<div class="table__header">
			<div class="header__top txt-white txt-center">Tổng khối lượng giao dịch thỏa thuận: 19,150,350 - Tổng giá trị giao dịch thỏa thuận: 229,351,500</div>
			<div class="header__content">
				<div class="col-3">
					<?php
					include( 'stock-tables/thoa-thuan/chao-mua/header.php' );
					?>
				</div>
				<div class="col-6">
					<?php
					include( 'stock-tables/thoa-thuan/khop-lenh/header.php' );
					?>
				</div>
				<div class="col-3">
					<?php
					include( 'stock-tables/thoa-thuan/chao-ban/header.php' );
					?>
				</div>
			</div>
		</div>
		<div class="table__body">
			<div class="col-3">
				<?php
				include( 'stock-tables/thoa-thuan/chao-mua/body.php' );
				?>
			</div>
			<div class="col-6">
				<?php
				include( 'stock-tables/thoa-thuan/khop-lenh/body.php' );
				?>
			</div>
			<div class="col-3">
				<?php
				include( 'stock-tables/thoa-thuan/chao-ban/body.php' );
				?>
			</div>
		</div>
	</div>

	<div id="ket-qua-loc" class="stock-table">
		<?php
		include( 'stock-tables/ket-qua-loc/header.php' );
		include( 'stock-tables/ket-qua-loc/body.php' );
		?>
	</div>
</div>
