<div class="stock-tables__header">
	<div class="stock-header__right">
		<div class="stock-header__buttons">
			<button type="button" class="init-chi-so-modal">
				<i class="fas fa-cog"></i>
			</button>
			<button type="button" class="stock-header__chart-toggle">
				<i class="fas fa-angle-up"></i>
			</button>
		</div>
	</div>
</div>