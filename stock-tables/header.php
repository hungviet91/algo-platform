<div class="stock-tables__header">
	<div class="stock-header__left">
		<form class="stock-header__search">
			<button id="submit-code"><i class="fas fa-plus"></i></button>
			<input class="input-txt--dark" type="text" placeholder="Thêm mã CK" id="add-code" data-bang-gia-body="1" data-co-ban-body="false">
		</form>
		<ul class="stock-header__navigation">
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle has-table active" data-view-mode="danh-muc" href="#danh-muc" title="Danh mục">Danh mục MBS</a>
				<div class="dropdown-menu--danh-muc dropdown-menu">
					<ul class="dropdown-menu__list">
						<li class="list__item">
							<a href="#danh-muc" data-bang-gia-body="1" data-view-mode="danh-muc" class="has-table">
								<span class="list__name">
									<i class="fas fa-check-circle"></i><span>Danh mục MBS</span>
								</span>
								<input class="is-hidden" type="text">
								<span class="list__buttons">
									<span class="txt-white btn--edit"><i class="fas fa-edit"></i></span>
									<span class="txt-white btn--delete"><i class="fas fa-times"></i></span>
								</span>
							</a>
						</li>
					</ul>
					<form class="danh-muc__tao-moi">
						<input class="input-txt--dark" type="text" placeholder="Tạo danh mục mới">
						<button type="submit"><i class="fas fa-plus"></i></button>
					</form>
				</div>
			</li>
			<li class="has-submenu dropdown dropdown--niem-yet">
				<a class="nav-tab dropdown-toggle has-table" href="#niem-yet" data-view-mode="niem-yet" title="">Niêm yết</a>
				<ul class="dropdown-menu">
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">HOSE</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">HNX</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">UPCOM</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VN30</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">HNX30</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VNMID</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VNSML</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VN DIAMOND</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VNFIN LEAD</a></li>
					<li><a href="#niem-yet" data-view-mode="niem-yet" class="dropdown--niem-yet__link has-table" title="">VNFIN SELECT</a></li>
				</ul>
			</li>
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle has-table" href="#thoa-thuan">Thỏa thuận</a>
				<ul class="dropdown-menu">
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT HOSE</a></li>
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT HNX</a></li>
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT UPCOM</a></li>
				</ul>
			</li>
			<li class="has-submenu dropdown dropdown--nganh">
				<a class="nav-tab dropdown-toggle has-table" href="#niem-yet" data-view-mode="nganh" title="Ngành">Ngành</a>
				<ul class="dropdown-menu dropdown-menu--nganh">
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Dầu khí</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Hóa chất</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Tài nguyên cơ bản</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Xây dựng và vật liệu</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Hàng và Dịch vụ công nghiệp</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Ô tô và phụ tùng</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Thực phẩm và đồ uống</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Hàng cá nhân và gia dụng</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Thiết bị và dịch vụ y tế</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Dược phẩm</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Bán lẻ</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Truyền thông</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Du lịch và giải trí</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Viễn thông</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Điện</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Nước và Khí đốt</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Ngân hàng</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Bảo hiểm</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Bất động sản</a></li>
					<li><a href="#niem-yet" data-view-mode="nganh" class="has-table" title="">Dịch vụ tài chính</a></li>
				</ul>
			</li>
			<li><a class="nav-tab has-table" href="#khuyen-nghi" title="Khuyến nghị">CP khuyến nghị</a></li>
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle" href="#loc" title="Lọc">Lọc</a>
				<div class="dropdown-menu--loc dropdown-menu">
					<ul class="dropdown-menu__list">
						<li class="list__item active">
							<span class="list__name txt-white"><span>CP tăng trưởng</span></span>
						</li>
						<li class="list__item">
							<span class="list__name txt-white"><span>CP lợi nhuận</span></span>
						</li>
						<li class="list__item list__item--filter">
							<span class="list__name txt-white"><span>CP giá trị</span></span>
							<input class="is-hidden" type="text">
							<span class="list__buttons">
								<span class="txt-white btn--edit"><i class="fas fa-edit"></i></span>
								<span class="txt-white btn--delete"><i class="fas fa-times"></i></span>
							</span>
						</li>
					</ul>
					<div class="loc__tao-moi">
						<a href="#" title="Tạo bộ lọc" class="txt-white init-filter-modal">Tạo bộ lọc</a>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="stock-header__right" data-table="#dm-mbs">
		<div class="stock-header__view-modes view-modes--danh-muc is-active">
			<a href="#danh-muc" data-bang-gia-body="1" class="active view-mode view-mode--bang-gia has-table"><i class="fas fa-chart-line"></i>Bảng giá</a></span>
			<a href="#danh-muc-co-ban" data-co-ban-body="1" class="view-mode view-mode--co-ban has-table"><i class="fas fa-chart-line"></i>Cơ bản</a>
			<a href="#" class="view-mode"><i class="fas fa-chart-line"></i>PTKT</a>
		</div>

		<div class="stock-header__view-modes view-modes--niem-yet">
			<a href="#niem-yet" class="active view-mode has-table"><i class="fas fa-chart-line"></i>Bảng giá</a></span>
			<a href="#niem-yet-co-ban" class="view-mode has-table"><i class="fas fa-chart-line"></i>Cơ bản</a>
			<a href="#" class="view-mode"><i class="fas fa-chart-line"></i>PTKT</a>
		</div>

		<div class="stock-header__view-modes view-modes--nganh">
			<a href="#niem-yet" class="active view-mode has-table"><i class="fas fa-chart-line"></i>Bảng giá</a></span>
			<a href="#niem-yet-co-ban" class="view-mode has-table"><i class="fas fa-chart-line"></i>Cơ bản</a>
			<a href="#" class="view-mode"><i class="fas fa-chart-line"></i>PTKT</a>
		</div>

		<div class="stock-header__buttons">
			<button type="button" class="init-chi-so-modal">
				<i class="fas fa-cog"></i>
			</button>
			<button type="button" class="stock-header__chart-toggle">
				<i class="fas fa-angle-up"></i>
			</button>
		</div>
	</div>

	<div class="stock-header__filter-result visble-on-filter is-hidden d-flex space-between flex-wrap">
		<div class="filter-result__number">
			Tên bộ lọc: <span class="filter-result__name"></span>
				(<span><strong class="txt-green">100</strong></span> mã thỏa mãn)
		</div>
		<div class="filter-result__edit visble-on-filter is-hidden txt-white">
			<a href="#">Điều khoản sử dụng</a>
			<a href="#" class="filter__create init-filter-modal">Tạo mới bộ lọc</a>
			<a href="#" class="filter__edit init-filter-modal" data-modal-edit="enable">Chỉnh sửa bộ lọc</a>
		</div>
	</div>
</div>