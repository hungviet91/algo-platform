<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<thead>
			<tr>
				<th rowspan="2" class="sortable">Mã CK</th>
				<th rowspan="2" class="sortable">Sàn</th>
				<th rowspan="2" class="td-col-5 sortable">Ngành</th>
				<th rowspan="2" class="td-col-2 sortable">Tổng KL <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th colspan="3" class="cell-highlight" class="filter-starts">Khớp lệnh</th>
			</tr>
			<tr>
				<th class="cell-highlight sortable has-toggle toggle--percent">
					<a class="toggle__left" data-toggle="percent"><i class="fa fa-caret-left"></i></a>
					<span class="hidden-first">%</span>
					<span class="">+/-</span>
					<a class="toggle__right" data-toggle="percent"><i class="fa fa-caret-right"></i></a>
				</th>
				<th class="cell-highlight sortable">Giá</th>
				<th class="cell-highlight sortable">KL</th>
			</tr>
		</thead>
	</table>
</div>