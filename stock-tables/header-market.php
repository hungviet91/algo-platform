<div class="stock-tables__header">
	<div class="stock-header__left">
		<ul class="market__main-navigation stock-header__navigation d-flex flex-wrap">
			<li class="dropdown">
				<a class="nav-tab active" href="#chuyen-dong-thi-truong" title="Danh mục">Chuyển động thị trường</a>
			</li>
			<li class="dropdown">
				<a class="nav-tab" href="#chuyen-dong-to-chuc" title="Danh mục">Chuyển động tổ chức</a>

			</li>
			<li class="dropdown">
				<a class="nav-tab" href="#chuyen-dong-nganh" title="Chuyển động ngành">Chuyển động ngành</a>
			</li>
			<li class="dropdown">
				<a class="nav-tab" href="#tin-hieu" title="Tín hiệu">Tín hiệu</a>
			</li>
		</ul>
	</div>
	<div class="stock-header__right">
		<div class="stock-header__buttons">
			<button type="button" class="init-chi-so-modal">
				<i class="fas fa-cog"></i>
			</button>
			<button type="button" class="stock-header__chart-toggle">
				<i class="fas fa-angle-up"></i>
			</button>
		</div>
	</div>
</div>