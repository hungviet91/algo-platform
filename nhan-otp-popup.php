<div class="lay-ma-otp">
	<div class="lay-ma-otp__body">
		<div class="lay-ma-otp__content">
			<strong>Bạn có muốn lấy mã OTP không?</strong><br>
		</div>
		<button type="submit" class="lay-ma-otp__yes btn btn--primary">Có</button>
		<button type="submit" class="lay-ma-otp__no btn btn--primary">Không</button>
	</div>
</div>

<div class="nhan-otp-popup">
	<div class="nhan-otp-popup__background"></div>
	<div class="nhan-otp-popup__body">
		<div class="nhan-otp-popup__content">
			<strong>Kính chúc quý khách đầu tư hiệu quả</strong><br>
			<p>Quý khách vui lòng thực hiện <a>Lấy mã OTP</a> để thuận tiện và đảm bảo an toàn khi giao tại MBS.</p><br>
		</div>
		<input type="text" name="" value="" placeholder="Nhập mã OTP">
		<button type="submit" class="btn btn--primary">Lưu</button>
		<span class="nhan-otp-popup__close"><i class="fas fa-times"></i></span>
	</div>
</div>
