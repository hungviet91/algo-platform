<div id="danh-muc-popup" class="footer-nav-popup danh-muc-popup is-hidden">
	<div class="footer-nav-popup__bar d-flex space-between align-center">
		<span class="footer-nav-popup__title">
			<i class="fas fa-book"></i>Danh mục
		</span>
		<span class="footer-nav-popup__close">
			<i class="fas fa-angle-down"></i>
		</span>
	</div>
	<div class="footer-nav-popup__header d-flex space-between align-center">
		<strong>Tài khoản</strong>
		<div class="danh-muc__tk footer-nav-popup__select has-submenu dropdown">
			<a class="dropdown-toggle" title="Danh mục"><span>4608121</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-tk="4608121" class="show-tk">4608121</a></li>
				<li><a href="#" data-tk="4608128" class="show-tk">4608128</a></li>
				<li><a href="#" data-tk="4608823" class="show-tk">4608823</a></li>
				<li><a href="#" data-tk="4608825" class="show-tk">4608825</a></li>
				<li><a href="#" data-tk="4608827" class="show-tk">4608827</a></li>
			</ul>
		</div>
	</div>
	<div class="footer-nav-popup__body custom-scrollbar">
		<table class="danh-muc__table">
			<colgroup>
				<col style="width: 59px">
				<col style="width: 23px">
				<col style="width: 52px">
				<col style="width: 52px">
				<col style="width: 57px">
				<col style="width: 33px">
				<col style="width: 33px">
			<thead>
				<th>Mã</th>
				<th>TT</th>
				<th>KL</th>
				<th>Giá TB</th>
				<th>% Lãi/Lỗ</th>
				<th>Mua</th>
				<th>Bán</th>
			</thead>
			<tbody>
				<?php for( $i = 0; $i < 3; $i++ ) : ?>
					<tr>
						<td class="danh-muc__table__code">ACB</td>
						<td></td>
						<td class="txt-right">100000</td>
						<td class="txt-right">25.5</td>
						<td class="txt-right">2</td>
						<td><a href="#" class="txt-center danh-muc__table__action txt-green" data-action="buying">Mua</a></td>
						<td><a href="#" class="txt-center danh-muc__table__action txt-red" data-action="selling">Bán</a></td>
					</tr>
				<?php endfor; ?>

				<?php for( $i = 0; $i < 3; $i++ ) : ?>
					<tr>
						<td class="danh-muc__table__code">KAC</td>
						<td></td>
						<td class="txt-right">100000</td>
						<td class="txt-right">25.5</td>
						<td class="txt-right">2</td>
						<td><a href="#" class="txt-center danh-muc__table__action txt-green" data-action="buying">Mua</a></td>
						<td><a href="#" class="txt-center danh-muc__table__action txt-red" data-action="selling">Bán</a></td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>
	</div>
</div>
