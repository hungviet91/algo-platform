<div class="danh-muc-moi-wrapper is-hidden">
	<h4>Thêm <span class="ten-danh-muc"></span> vào danh mục</h4>
	<ul class="danh-muc__da-co">
	</ul>
	<form class="danh-muc__tao-moi">
		<input class="input-txt--dark" type="text" placeholder="Tạo danh mục mới">
		<button type="submit"><i class="fas fa-plus"></i></button>
	</form>
</div>