<div id="so-lenh-popup" class="footer-nav-popup so-lenh-popup is-hidden lenh-trong-ngay">
	<div class="footer-nav-popup__bar d-flex space-between align-center">
		<span class="footer-nav-popup__title">
			<i class="fas fa-book"></i>Sổ lệnh
		</span>
		<span class="footer-nav-popup__close">
			<i class="fas fa-angle-down"></i>
		</span>
	</div>
	<div class="footer-nav-popup__header d-flex space-between">
		<div>
			<button class="btn btn--red init-xac-nhan-otp-modal so-lenh__huy" type="button">Hủy</button>
			<button class="btn btn--primary init-xac-nhan-otp-modal so-lenh__kich-hoat" type="button">Kích hoạt</button>
		</div>

		<div class="so-lenh__tk footer-nav-popup__select has-submenu dropdown">
			<a class="dropdown-toggle"><span>Tài khoản</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-tk="4608121" class="show-tk">4608121</a></li>
				<li><a href="#" data-tk="4608128" class="show-tk">4608128</a></li>
				<li><a href="#" data-tk="4608823" class="show-tk">4608823</a></li>
				<li><a href="#" data-tk="4608825" class="show-tk">4608825</a></li>
				<li><a href="#" data-tk="4608827" class="show-tk">4608827</a></li>
			</ul>
		</div>
	</div>
	<div class="footer-nav-popup__body custom-scrollbar">
		<div class="is-active so-lenh__trang-thai footer-nav-popup__select has-submenu dropdown" data-lenh="lenh-trong-ngay">
			<a class="dropdown-toggle"><span>Khớp một phần</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-value="tat-ca">Tất cả</a></li>
				<li><a href="#" data-value="cho-khop">Chờ khớp</a></li>
				<li><a href="#" data-value="khop">Khớp</a></li>
				<li><a href="#" data-value="khop-mot-phan">Khớp một phần</a></li>
				<li><a href="#" data-value="cho-duyet">Chờ duyệt</a></li>
				<li><a href="#" data-value="da-huy">Đã hủy</a></li>
				<li><a href="#" data-value="tu-choi">Từ chối</a></li>
				<li><a href="#" data-value="cho-xu-ly">Chờ xử lý</a></li>
			</ul>
		</div>

		<div class="so-lenh__trang-thai footer-nav-popup__select has-submenu dropdown" data-lenh="lenh-phien-ke-tiep">
			<a class="dropdown-toggle"><span>Tất cả</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-value="tat-ca">Tất cả</a></li>
				<li><a href="#" data-value="lenh-mua">Lệnh mua</a></li>
				<li><a href="#" data-value="lenh-ban">Lệnh bán</a></li>
			</ul>
		</div>

		<div class="so-lenh__trang-thai footer-nav-popup__select has-submenu dropdown" data-lenh="gio-lenh">
			<a class="dropdown-toggle"><span>Tất cả</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-value="tat-ca">Tất cả</a></li>
				<li><a href="#" data-value="lenh-mua">Lệnh mua</a></li>
				<li><a href="#" data-value="lenh-ban">Lệnh bán</a></li>
			</ul>
		</div>

		<div class="so-lenh__lenh footer-nav-popup__select has-submenu dropdown" data-value="lenh-trong-ngay">
			<a class="dropdown-toggle"><span>Lệnh trong ngày</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-value="lenh-trong-ngay">Lệnh trong ngày</a></li>
				<li><a href="#" data-value="lenh-phien-ke-tiep">Lệnh phiên kế tiếp</a></li>
				<!-- <li><a href="#" data-value="gio-lenh">Giỏ lệnh</a></li> -->
			</ul>
		</div>

		<table class="so-lenh__table" data-lenh="lenh-trong-ngay">
			<colgroup>
				<col style="width: 15px">
				<col style="width: 31px">
				<col style="width: 50px">
				<col style="width: 52px">
				<col style="width: 57px">
				<col style="width: 43px">
				<col style="width: 33px">
				<col class="so-lenh__table__edit">
				<col class="so-lenh__table__copy">
			</colgroup>

			<thead>
				<th><input class="dropdown__checkall" type="checkbox" name="" value=""></th>
				<th>Lệnh</th>
				<th>Mã</th>
				<th>KL</th>
				<th>KL Khớp</th>
				<th>Giá</th>
				<th>TT</th>
				<th class="so-lenh__table__edit"></th>
				<th class="so-lenh__table__copy"></th>
			</thead>
			<tbody>
				<?php for( $i = 0; $i < 10; $i++ ) : ?>
					<tr>
						<td><input class="so-lenh__table__checkbox" type="checkbox" name="" value=""></td>
						<td>Mua</td>
						<td class="so-lenh__table__code span"><span>KDC<span></td>
						<td class="txt-right so-lenh__table__KL">500</td>
						<td class="txt-right">90.000</td>
						<td class="txt-right so-lenh__table__gia">100</td>
						<td>KMP</td>
						<td class="so-lenh__table__edit" data-edit="<?php echo $i ?>"><i class="fas fa-pencil-alt"></i></td>
						<td class="so-lenh__table__copy" data-state="' + data.state + '"><i class="far fa-copy"></i></td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>

		<table class="so-lenh__table" data-lenh="lenh-phien-ke-tiep">
			<colgroup>
				<col style="width: 15px">
				<col style="width: 31px">
				<col style="width: 50px">
				<col style="width: 52px">
				<col style="width: 43px">
				<col style="width: 33px">
			</colgroup>

			<thead>
				<th><input class="dropdown__checkall" type="checkbox" name="" value=""></th>
				<th>Lệnh</th>
				<th>Mã</th>
				<th>KL</th>
				<th>Giá</th>
				<th>Kênh</th>
			</thead>
			<tbody></tbody>
		</table>

		<table class="so-lenh__table" data-lenh="gio-lenh">
			<colgroup>
				<col style="width: 15px">
				<col style="width: 31px">
				<col style="width: 50px">
				<col style="width: 52px">
				<col style="width: 43px">
				<col style="width: 33px">
			</colgroup>

			<thead>
				<th><input class="dropdown__checkall" type="checkbox" name="" value=""></th>
				<th>Lệnh</th>
				<th>Mã</th>
				<th>KL</th>
				<th>Giá</th>
				<th>TT</th>
			</thead>
			<tbody></tbody>
		</table>
	</div>

	<div class="footer-nav-popup__footer is-minimized">
		<span class="footer-nav-popup__footer-close">
			<i class="fas fa-angle-down"></i>
		</span>
		<table>
			<tr>
				<td>CK</td>
				<td>: <strong>Chờ khớp</strong></td>
			</tr>
			<tr>
				<td>K</td>
				<td>: <strong>Khớp</strong></td>
			</tr>
			<tr>
				<td>KMP</td>
				<td>: <strong>Khớp một phần</strong></td>
			</tr>
		</table>
		<table>
			<tr>
				<td>CD</td>
				<td>: <strong>Chờ duyệt</strong></td>
			</tr>
			<tr>
				<td>ĐH</td>
				<td>: <strong>Đã hủy</strong></td>
			</tr>
			<tr>
				<td>TC</td>
				<td>: <strong>Từ chối</strong></td>
			</tr>
			<tr>
				<td>CXL</td>
				<td>: <strong>Chờ xử lý</strong></td>
			</tr>
		</table>
	</div>
</div>
