<header class="site-header">
	<div class="header__left">
		<div class="site-icon">
			<img src="./images/logo.png" alt="logo">
		</div>
		<div class="header__search">
			<form role="search" method="get" class="search-form" action="">
				<label>
					<input class="search-field input-txt--dark" id="view-code" placeholder="Xem chi tiết mã..." name="s" type="search">
				</label>
				<button type="submit" class="search-submit" id="view-code-submit"><i class="fas fa-search"></i></button>
			</form>
		</div>
		<div class="header__time">
			<span class="hour">10:05:00</span>
		</div>
	</div>
	<div class="header__center">
		<div class="news">
			<a href="#">Tăng khả năng tiếp cận vốn cho Doanh nghiệp nhỏ và vừa</a>
			<a href="#">"Vấn đề đau đầu nhất" khi sửa Luật đầu tư</a>
		</div>
	</div>
	<div class="header__right">
		<ul class="header__navigation">
			<li class="dropdown">
				<a class="dropdown-toggle is-active" href="#"><?= $menu_title ?? 'Cơ sở' ?> <i class="fas fa-angle-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="./">Cơ sở</a></li>
					<li><a href="./phai-sinh.php">Phái sinh</a></li>
					<li><a href="./chung-quyen.php">Chứng quyền</a></li>
					<li><a href="#">Trái phiếu</a></li>
					<li><a href="#" target="_blank">Bảng giá cũ</a></li>
				</ul>
			</li>
		</ul>
		<div class="header__language dropdown">
			<a class="dropdown-toggle" href="#"><img src="./images/vn.svg" alt="vn"></a>
			<ul class="dropdown-menu">
				<li><a href="./"><img src="./images/vn.svg" alt="vn"> Tiếng Việt</a></li>
				<li><a href="./en.php"><img src="./images/gb.svg" alt="en"> English</a></li>
				<li><a href="./en.php"><img src="./images/cn.svg" alt="cn"> 中文</a></li>
				<li><a href="./en.php"><img src="./images/kr.svg" alt="cn"> 한국어</a></li>
				<li><a href="./en.php"><img src="./images/jp.svg" alt="jp"> 日本語</a></li>
			</ul>
		</div>
		<div class="header__actions">
			<a href="login.php" class="btn btn--login">Đăng nhập</a>
		</div>
	</div>
</header>