<div class="dat-lenh-popup">
	<div class="dat-lenh-popup__actions">
		<span class="dat-lenh-notice"></span>
		<div class="dat-lenh-popup__form d-flex flex-wrap">
			<div class="dat-lenh-popup__lenh-thuong">
				<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="" value="" checked=""><span>Lệnh thường</span></label>
			</div>
			<div class="form__body">
				<div class="form__body__inputs">
					<div class="form__select-mua-ban">
						<ul class="d-flex">
							<li class="has-submenu dat-lenh__mua dropdown">
								<a class="dropdown-toggle" href=""><span>Mua</span><i class="fas fa-angle-down"></i></a>
								<ul class="dropdown-menu--mua dropdown-menu">
									<li><a href="#">Mua</a></li>
									<li><a href="#">Mua OutR</a></li>
								</ul>
							</li>
							<li class="has-submenu dat-lenh__ban dropdown">
								<a class="dropdown-toggle" href=""><span>Bán</span><i class="fas fa-angle-down"></i></a>
								<ul class="dropdown-menu--ban dropdown-menu">
									<li><a href="#">Bán</a></li>
									<li><a href="#">Bán OutR</a></li>
									<li><a href="#">Bán TT68</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="form__select-tai-khoan">
						<ul>
							<li class="has-submenu dropdown">
								<a class="nav-tab dropdown-toggle" href="#"><span>Tài khoản</span><i class="fas fa-angle-down"></i></a>
								<div class="dropdown-menu--tai-khoan dropdown-menu">
									<ul class="dropdown-menu__list">
										<li tabindex="1"><a href="#" data-tk="">Tài khoản</a></li>
										<li tabindex="2"><a href="#" data-tk="4608121" class="show-tk">4608121</a></li>
										<li tabindex="3"><a href="#" data-tk="4608128" class="show-tk">4608128</a></li>
										<li tabindex="4"><a href="#" data-tk="4608823" class="show-tk">4608823</a></li>
										<li tabindex="5"><a href="#" data-tk="4608825" class="show-tk">4608825</a></li>
										<li tabindex="6"><a href="#" data-tk="4608827" class="show-tk">4608827</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
					<div class="dat-lenh__form__input form__input--code">
						<input class="dat-lenh__form__tabindex" type="text" name="" value="" placeholder="Mã">
						<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" tabindex="-1" name="" value=""><span></span></label>
					</div>
					<div class="dat-lenh__form__input form__input--gia">
						<input class="dat-lenh__form__tabindex" type="text" name="" value="" placeholder="Giá">
						<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" tabindex="-1" name="" value=""><span></span></label>
						<span class="txt-light-1 quantity">x1000</span>
						<!-- <div class="dat-lenh__info--code info--gia is-hidden">
							<span class="info--so-du">Số dư CK: <strong>28.500</strong></span><br>
							<span class="info--fal">FAL đã dùng: <strong>28.500</strong></span>
						</div>
						<div class="dat-lenh__info--code info--gia--ban is-hidden">
							<span class="info--so-du">Số dư CK: <strong>28.500</strong></span><br>
						</div> -->
					</div>
					<div class="dat-lenh__form__input form__input--KL">
						<input class="dat-lenh__form__tabindex" type="text" name="" value="" placeholder="KL">
						<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" tabindex="-1" name="" value=""><span></span></label>
					</div>
					<div class="dat-lenh__form__input form__input--OTP">
						<input class="dat-lenh__form__tabindex is-saved" type="password" name="" value="" placeholder="OTP">
						<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" tabindex="-1" name="" value="" checked=""><span></span></label>
						<div class="dat-lenh__info--otp">
							<button type="button" class="info--button-otp btn btn--red-2">Nhận mã OTP</button>
						</div>
					</div>
				</div>
				<div class="form__body__infos">
					<div class="form__body__info">
						<div class="info--tk is-hidden">
							<span>Sức mua tài khoản</span>
							<span>: <strong>23.028.500.000</strong></span>
						</div>
						<div class="info--ki-quy is-hidden">
							<span>Tỷ lệ ký quỹ</span>
							<span>: <strong>50%</strong></span>
						</div>
					</div>
					<div class="form__body__info visible-on-price is-hidden">
						<div class="info--kl-mua">
							<span>KL có thể mua</span>
							<span>: <strong>28.500.000</strong></span>
						</div>
						<div class="info--kl-ban">
							<span>KL có thể bán</span>
							<span>: <strong>28.500.000</strong></span>
						</div>
						<div class="info--kl-mua-fal">
							<span>KL mua gồm FAL</span>
							<span>: <strong>28.500</strong></span>
						</div>
					</div>
					<div class="form__body__info visible-on-price is-hidden">
						<div class="info--so-du">
							<span>Số dư CK</span>
							<span>: <strong>28.000.500</strong></span>
						</div>
						<div class="info--dung-fal">
							<span>FAL đã dùng</span>
							<span>: <strong>28.500</strong></span>
						</div>
					</div>
				</div>
			</div>
			<div class="form__submit">
				<button type="button" class="dat-lenh__form__tabindex dat-lenh-popup__btn--submit btn btn--green">Mua</button>
				<div class="dat-lenh__gio-lenh">
					<!-- <a href="#" class="gio-lenh__init"><i class="fas fa-cart-plus"></i>Giỏ lệnh</a> -->
					<a href="#" class="gio-lenh__huy"><i class="fas fa-times-circle"></i>Hủy</a>
				</div>
				<!-- <div class="dat-lenh-popup__btn--others">
					<a href="./danh-sach-lenh.php" type="button" class="btn btn--dark-3">Danh sách lệnh</a>
					<a href="./quan-ly-tai-khoan.php" type="button" class="btn btn--dark-3">Quản lý tài khoản</a>
				</div> -->
			</div>
			<span class="form__close"><i class="fas fa-times-circle"></i></span>
		</div>
	</div>
	<!-- <div class="dat-lenh-popup__code">
		<h4 class="dat-lenh-popup__code__name"></h4>
		<div class="dat-lenh-popup__code__company"></div>
		<div>Tỷ lệ ký quỹ: 12%</div>
		<table>
			<tbody>
				<tr>
					<td>
						<div class="d-flex space-between">
							Trần<span class="td-value--tran txt-pink"></span>
						</div>
					</td>
					<td>
						<div class="d-flex space-between">
							Sàn<span class="td-value--san txt-white"></span>
						</div>
					</td>
					<td class="dat-lenh-popup__code__col-sm">
						<div class="d-flex space-between">
							TC<span class="td-value--tc txt-yellow"></span>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="d-flex space-between">
							Mua 1<span class="td-value--mua_1 txt-green"></span>
						</div>
					</td>
					<td>
						<div class="d-flex space-between">
							Bán 1<span class="td-value--ban_1 txt-green"></span>
						</div>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<div class="d-flex space-between">
							KL Mua 1<span class="td-value--mua_1 txt-green"></span>
						</div>
					</td>
					<td>
						<div class="d-flex space-between">
							KL Bán 1<span class="td-value--ban_1 txt-green"></span>
						</div>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<div class="d-flex space-between">
							Tổng KL mua<span class="td-value--tong_kl_mua txt-white"></span>
						</div>
					</td>
					<td>
						<div class="d-flex space-between">
							Tổng KL bán<span class="td-value--tong_kl_ban txt-white"></span>
						</div>
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div> -->
	<div class="dat-lenh-popup__tk-info">
		<div class="d-flex space-between">
			<span>Tiền</span>
			<span>120.500.000</span>
		</div>
		<div class="d-flex space-between">
			<span>Giá trị CK</span>
			<span>8.900.200</span>
		</div>
		<div class="d-flex space-between">
			<span>Nợ</span>
			<span>1.500.00</span>
		</div>
		<div class="d-flex space-between">
			<span>Tài sản ròng</span>
			<span>6.000.000</span>
		</div>
		<div class="d-flex space-between">
			<span>Tỷ lệ K</span>
			<span>10%</span>
		</div>
		<div class="d-flex space-between">
			<span>Hạn mức Margin</span>
			<span>25%</span>
		</div>
	</div>
</div>

<div class="popup-edit">
	<div class="popup-edit__actions">
		<div class="popup-edit__form d-flex flex-wrap">
			<div class="form__body">
				<div class="form__body__inputs">
					<div class="dat-lenh__form__input form__input--gia">
						<input class="dat-lenh__form__tabindex" type="text" name="" value="" placeholder="Giá">
						<span class="txt-light-1 quantity">x1000</span>
					</div>
					<div class="dat-lenh__form__input form__input--KL">
						<input class="dat-lenh__form__tabindex" type="text" name="" value="" placeholder="KL">
					</div>
				</div>
			</div>
			<div class="form__submit">
				<button type="button" class="dat-lenh__form__tabindex popup-edit__btn--submit btn btn--green">Xác nhận</button>
			</div>
			<span class="popup-edit__close"><i class="fas fa-times-circle"></i></span>
		</div>
	</div>
</div>